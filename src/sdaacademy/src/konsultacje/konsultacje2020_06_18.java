package sdaacademy.src.konsultacje;

public class konsultacje2020_06_18 {
    /*
    || Zadanie 3
    ||| Klasa Shape
    Zaimplementuj klasę `Shape`. Klasa powinna zawierać:
    * pole odpowiedzialne za przechowywanie koloru
    * pole odpowiedzialne za przechowywanie informacji o tym czy kolor powinien wypełniać figurę czy nie
    * konstruktor bezparametrowy ustawiający wartość pola `color` na `unknown` i `isFilled` na `false`
    * konstruktor przyjmujący parametry `color` i `isFilled`
    * metody typu `getter` odpowiedzialne za zwracanie wartości pól klasy
    * metody typu `setter` odpowiedzialne za ustawianie wartości pól klasy
    * nadpisaną metodę `toString` odpowiedzialną za wyświetlanie następującej informacji: `Shape with color of ? and filled/NotFilled`, gdzie ? oznacza wartość kolor, a wartość `filled`/`not filled` powinna zostać zwracana w zależności od pola `isFilled`
     ||| Klasa Circle
    Zaimplementuj klasę `Circle`, która będzie rozszerzać klasę `Shape` o następujące cechy:
    * pole odpowiedzialne za przechowywanie wartości promienia
    * konstruktor bezparametrowy ustawiający wartość pola `color` na `unknown` i `isFilled` na `false` oraz pola `radius` na `1`
    * konstruktor przyjmujący parametry `color`, `isFilled`, `radius`
    * metodę typu `getter` odpowiedzialną za zwracanie wartości pola `radius`
    * metodę typu `setter` odpowiedzialną za ustawianie wartości pola `radius`
    * metodę `getArea` odpowiedzialną za obliczanie pola powierzchni
    * metodę `getPerimeter` odpowiedzialną za obliczanie obwodu
    * nadpisaną metodę `toString` odpowiedzialną za wyświetlanie następującej informacji: `Circle with radius=? which is a subclass off y`, gdzie ? oznacza wartość promienia, a wartość `y` powinna być rezultatem wywołania metody `toString` z klasu bazowej
    ||| Klasa Rectangle
    Zaimplementuj klasę `Rectangle`, która będzie rozszerzać klasę `Shape` o następujące cechy:
    * pole szerokość oraz długość będące typem `double`
    * konstruktor bezparametrowy ustawiający wartość pola `color` na `unknown` i `isFilled` na `false` oraz pola `width` i `length` na `1`
    * konstruktor przyjmujący parametry `color`, `isFilled`, `width` i `length`
    * metody typu `getter` do zwracania wartości pól `width`, `length`
    * metody typu `setter` do ustawiania wartości pól `width` i `length`
    * metodę `getArea` odpowiedzialną za obliczanie pola powierzchni
    * metodę `getPerimeter` odpowiedzialną za obliczanie obwodu
    * nadpisaną metodę `toString` odpowiedzialną za wyświetlanie następującej informacji: `Rectangle with width=? and length=? which is a subclass off y`, gdzie ? oznacza wartość odpowiednio szerokości i długości, a wartość `y` powinna być rezultatem wywołania metody `toString` z klasy bazowej
    ||| Klasa Square
    Zaimplementuj klasę `Square`, która będzie rozszerzać klasę `Rectangle`. Klasa ta nie powinna wprowadzać nowych pól oraz funkcjonalności, ale powinna wymuszać na klasie bazowej zachowanie kwadratu.
    Zaprezentuj zaimplementowane powyżej rozwiązanie na przykładzie.
     */

        public static void main(String[] args) {
            ShapeV1 shapeV1 = new ShapeV1("red", false);
            System.out.println(shapeV1);
            CircleV1 circle = new CircleV1("green", true, 10);
            System.out.println(circle.getArea());
            System.out.println(circle);
            RectangleV1 rectangle = new RectangleV1("yellow", false, 10, 23);
            System.out.println(rectangle);
            SquareV1 squareV1 = new SquareV1(20, "blue", true);
            System.out.println(squareV1);
        }
    }
    class ShapeV1 {
        //modyfikator_dostępu typ_pola nazwa_pola
        private String color;
        private boolean fill;
        //konstruktor - metoda: nazwa_klasy(parametry_do_przekazania)
        public ShapeV1(String color, boolean fill) {
            this.color = color;
            this.fill = fill;
        }
        public ShapeV1() { }
        //setter dla koloru
        public void setColor(String color) {
            this.color = color;
        }
        //setter dla wypełnienia
        public void setFill(boolean fill) {
            this.fill = fill;
        }
        //getter dla koloru
        public String getColor() {
            return color;
        }
        //getter dla wypełnienia
        public boolean isFill() {
            return fill;
        }
        @Override
        public String toString() {
            //%s - string
            //%d - integer
            //%f - float/double
            //ternarny operator
            String fillText = fill ? "filled" : "Not filled";
//        if (fill) {
//            fillText = "filled";
//        } else {
//            fillText = "Not filled";
//        }
            //super.toString();// -> odwołanie do klasy object
            return String.format("Shape with color of %s and %s", color, fillText);
        }
    }
    //Object->ShapeV1->CircleV1
    class CircleV1 extends ShapeV1 {
        private int radius;
        public CircleV1(){
            super("unknown", false);
            radius = 1;
        }
        public CircleV1(String color, boolean fill, int radius) {
            super(color, fill);
            this.radius = radius;
        }
        public CircleV1(String color, boolean fill, int radius, String additionalInfo) {
            this(color, fill, radius);//wywołanie konstruktora z tej samej klasy
            System.out.println(additionalInfo);
        }
        public void setRadius(int radius) {
            this.radius = radius;
        }
        public int getRadius() {
            return radius;
        }
        public double getArea() {
            return Math.PI*radius*radius;
        }
        public double getPerimeter() {
            return 2*Math.PI*radius;
        }
        @Override
        public String toString() {
            String shapeClassToString = super.toString();
            return String.format("Circle with radius=%d which is a subclass off %s", radius, shapeClassToString);
        }
    }
    class RectangleV1 extends ShapeV1 {
        private double width;
        private double height;
        public RectangleV1() {
            super("unknown", false);
            width = 1;
            height = 1;
        }
        public RectangleV1(String color, boolean fill, double width, double height) {
            super(color, fill);//wywołanie konstruktora klasy bazowej
            this.width = width;
            this.height = height;
        }
        public double getWidth() {
            return width;
        }
        public void setWidth(double width) {
            this.width = width;
        }
        public double getHeight() {
            return height;
        }
        public void setHeight(double height) {
            this.height = height;
        }
        public double getArea() {
            return width*height;
        }
        public double getPerimeter() {
            return 2*width + 2*height;
        }
        @Override
        public String toString() {
            String shapeToString = super.toString();
            return String.format("Rectangle with width=%f and height=%f which is a subclass off %s", width, height, shapeToString);
        }
    }
    class SquareV1 extends RectangleV1 {
        public SquareV1() {
            //odwołanie do konstruktora bezparametrowego klasy RectangleV1
            super();
        }
        public SquareV1(double size, String color, boolean fill) {
            //this - obiekt na rzecz którego wywołujemy metody
            //super -rodzic
            super(color, fill, size, size);
        }
        public void setWidth(int size) {
            super.setWidth(size);
            super.setHeight(size);
        }
        public void setHeight(int size) {
            super.setHeight(size);
            super.setWidth(size);
        }
        @Override
        public String toString() {
            String rectangleToString = super.toString();
            return String.format("Square with size=%f which is a subclass off %s", getWidth(), rectangleToString);
        }
    }