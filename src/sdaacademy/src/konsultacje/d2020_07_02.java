package sdaacademy.src.konsultacje;
import java.util.Random;
import java.util.Collection;
import java.util.HashSet;
/*
## Zadanie 3
Na podstawie `100` elementowej tablicy z losowo wybranymi wartościami z przedziału `0-50` zaimplementuj następujące funkcjonalności:
* zwróć listę unikalnych elementów
* zwróć listę elementów, które conajmniej raz powtórzyły się w wygenerowanej tablicy
 */
public class d2020_07_02 {
    public static void main(String[] args) {
        Random random = new Random();
        random.nextInt();
        String v1 = "Test";
        String v2 = "XXX";
        String v3 = v1 + v2;//=>"TestXXX"
        System.out.println(v3);
        for (int i=0;i<3;i++) {
            System.out.println("Random: " + random.nextInt(100) + 300);
            //"Random: + "losowa_wartość" + "300";
            System.out.println("Random: " + (random.nextInt(100) + 300));
            System.out.println("Math random(): " + Math.random());
        }
        //##########################################################
        int[] tab = generateTab(5, 30);
        //Sposób 1
        Collection<Integer> results1 = new HashSet<>();
        for (int valueFromTab : tab) {
            if (!results1.contains(valueFromTab)) {
                results1.add(valueFromTab);
            }
        }
        System.out.println("Result 1: " + results1);
        //Sposób 2
        HashSet<Integer> results2 = new HashSet<>();
        for (int valueFromTab : tab) {
            //wartość 100
            //liczony jest jednoznaczny identyfikator który wskazuje gdzie ma znajdować się wartość 100
            //jeśli coś w tym miejscu już jest to znaczy, że to wartość 100 zajeła już swoją pozycję
            //jeśli nie dodajemy
            results2.add(valueFromTab);
        }
        System.out.println("Result 2: " + results2);
    }
    private static int[] generateTab(int size, int bound) {
        Random random_tab = new Random();//tworzenie obiektu do generowania losowych wartości
        int[] tab = new int[size];
        for (int i=0;i<tab.length;i++) {
            int randomValue = random_tab.nextInt(bound);//losowanie liczby z przedziału od 0 do 30
            tab[i] = randomValue;
            System.out.println("Element: " + tab[i]);
        }
        return tab;
    }
}