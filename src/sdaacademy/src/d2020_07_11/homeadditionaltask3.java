package sdaacademy.src.d2020_07_11;

import java.util.ArrayList;
import java.util.List;

public class homeadditionaltask3 {
    private static List<Integer> joinSortedLists(List<Integer> list1, List<Integer> list2) {
        List<Integer> result = new ArrayList<>(list1.size() + list2.size());
        int i = 0;
        int j = 0;
        while (i < list1.size() || j < list2.size()) {
            int number1 = i < list1.size() ? list1.get(i) : Integer.MAX_VALUE;
            int number2 = j < list2.size() ? list2.get(j) : Integer.MAX_VALUE;
            if (number1 <= number2) {
                if (number1 < Integer.MAX_VALUE) {
                    result.add(number1);
                    i++;
                }
            } else {
                if (number2 < Integer.MAX_VALUE) {
                    result.add(number2);
                    j++;
                }
            }
        }
        return result;
    }
}
