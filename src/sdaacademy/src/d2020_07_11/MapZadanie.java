package sdaacademy.src.d2020_07_11;

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class MapZadanie {
    //    1. W metodzie main utwórz mapę <Long, String>, gdzie docelowo
//    klucz będzie reprezentował id, a wartość imię.
//    2. Dodaj rekordy(pary) do mapy, tak aby kilka z nich zawierało w
//    wartości imię na literę A.
//    3. Korzystając z mapy, wyświetl wszystkie imiona zaczynające się na
//    literę A.
//    4. Jeśli jest taka potrzeba, dodaj do mapy rekordy tak aby imię „Jan"
//    występowało kilkukrotnie.
//    5. Korzystając z mapy, wyświetl wszystkie id, które przechowują
//    wartość „Jan".
    public static void main(String[] args) {
        Map<Long, String> names = new TreeMap<>();
        names.put(1L, "Adam");
        names.put(2L, "Andrzej");
        names.put(3L, "Bartek");
        names.put(4L, "Brian");
        Map<Long, String> names2 = new TreeMap<>();
        names2.put(5L, "Jan");
        names2.put(6L, "Jan");
        names2.put(7L, "Jan");
        names.putAll(names2);
        for(String name: names.values()) {
            if (name.startsWith("A")) {
                System.out.println(name);
            }
        }
        for(Map.Entry<Long, String> entry: names.entrySet()) {
            if(Objects.equals(entry.getValue(), "Jan")) {
                System.out.println(entry.getKey());
            }
        }
    }
}
