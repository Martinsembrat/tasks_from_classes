package sdaacademy.src.d2020_07_11;

public class Homeadditionaltask3v2 {
    private static int[] joinSortedLists(int[] list1, int[] list2) {
        int[] result = new int[list1.length + list2.length];
        int i = 0;
        int j = 0;
        while (i < list1.length || j < list2.length) {
            int number1 = i < list1.length ? list1[i] : Integer.MAX_VALUE;
            int number2 = j < list2.length ? list2[j] : Integer.MAX_VALUE;
            if (number1 <= number2) {
                if (number1 < Integer.MAX_VALUE) {
                    result[i++ + j] = number1;
                }
            } else {
                if (number2 < Integer.MAX_VALUE) {
                    result[i + j++] = number2;
                }
            }
        }
        return result;
    }
}