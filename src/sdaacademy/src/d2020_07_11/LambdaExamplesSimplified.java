package sdaacademy.src.d2020_07_11;


public class LambdaExamplesSimplified {

    // @FunctionalInterface
    interface Calculation {

        double calculate(double a, double b);
    }

    private static double sum(double a, double b) {
        return a + b;
    }

    public static void main(String[] args) {
        // 1.
        LambdaExamplesSimplified.sum(1, 2);

        // 2. klasa anonimowa
        Calculation adder = new Calculation() {
            @Override
            public double calculate(double a, double b) {
                return a + b;
            }
        };

        adder.calculate(1, 2);

        // 3. lambda
        adder = (a, b) -> a + b;
        adder.calculate(1, 2);

        // 4. lambda + method reference
        adder = LambdaExamplesSimplified::sum;
        adder.calculate(1, 2);
    }


}
