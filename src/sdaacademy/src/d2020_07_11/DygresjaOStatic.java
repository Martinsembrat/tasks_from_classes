package sdaacademy.src.d2020_07_11;

public class DygresjaOStatic {

    private static class Student {

        public static Student withIndex(String index) {
            Student s = new Student();
            s.index = index;
            return s;
        }

        private String index;

        public String getIndex() {
            return index;
        }

        // tworzymy nowy obiekt ze zmienionymi danymi po to, aby
        public Student changeIndex(String index) {
            return Student.withIndex(index);
        }
    }

    public static void main(String[] args) {
        Student johnnyB = Student.withIndex("1234");
        System.out.println(johnnyB.getIndex());
        Student johnnyB2 = johnnyB.changeIndex("456"); // johnnyB.withIndex("1234");
        System.out.println(johnnyB2.getIndex());
    }
}
