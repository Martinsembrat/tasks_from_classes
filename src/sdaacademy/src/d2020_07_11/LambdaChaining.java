package sdaacademy.src.d2020_07_11;


import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaChaining {

    public static void main(String[] args) {
        functionsSample();
        predicatesSample();
    }

    private static void functionsSample() {
        Function<Double, Double> pow2 = x -> x * x; // pow2 - power of 2
        BiFunction<Double, Double, Double> sum = (a, b) -> a + b;

        Consumer<Double> printOnScreen = x -> System.out.println(x);

        printOnScreen.accept(sum.andThen(pow2).apply(3.0, 4.0)); // (3 + 4) ** 2
    }

    private static void predicatesSample() {
        Predicate<Double> isPositive = x -> x > 0;
        Predicate<Double> isEven = x -> x % 2 == 0;
        Predicate<Double> isPositiveEven = isPositive.and(isEven);
        Consumer<Boolean> printOnScreen = System.out::println; //to samo co w lini 20

        printOnScreen.accept(isPositiveEven.test(11.0));

        // System.out.println(x > 0 && x % 2 == 0);
    }

}
