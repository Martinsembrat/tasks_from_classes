package sdaacademy.src.d2020_07_11.d2020_07_11;

import java.util.Comparator;
import java.util.List;

public class Comparators {

    static class Room { // implements Comparable<Room> {

        private String city;
        private double area;
        private double price;
        private double distanceFromCityCenter;

        public Room(String city, double area, double price, double distanceFromCityCenter) {
            this.city = city;
            this.area = area;
            this.price = price;
            this.distanceFromCityCenter = distanceFromCityCenter;
        }

        public String getCity() {
            return city;
        }

        public double getArea() {
            return area;
        }

        public double getPrice() {
            return price;
        }

        public double getDistanceFromCityCenter() {
            return distanceFromCityCenter;
        }

        @Override
        public String toString() {
            return "Room{" +
                    "city='" + city + '\'' +
                    ", area=" + area +
                    ", price=" + price +
                    ", distanceFromCityCenter=" + distanceFromCityCenter +
                    '}';
        }

        // @Override
//        public int compareTo(Room other) {
//            // -1 - bieżący obiekt powinien być umieszczony przed other
//            // 0 - obiekty są tej samej rangi
//            // 1 - bieżący obiekt powinien być umieszczony za other
//            return (int) Math.signum(this.area - other.area);
//        }
    }

    private static final List<Room> roomsToRent = List.of(
            new Room("Szczecin", 20.0, 1000, 4.0),
            new Room("Szczecin", 8.0, 600, 2.4),
            new Room("Szczecin", 14.5, 900, 1.5),
            new Room("Szczecin", 14.5, 500, 8.9),
            new Room("Kraków", 20.0, 1400, 1),
            new Room("Gdańsk", 15.0, 1200, 3.0),
            new Room("Warszawa", 12, 1000, 3.5)
    );

    /**
     * .sorted() nie zadziała gdy Room nie implementuje Comparable !
     * .sorted(comparator) działa zawsze
     */
    public static void main(String[] args) {
        // nie zadziałą bez class Room implements Comparable<Room>
//        System.out.println("Posortowane domyślnie: ");
//        roomsToRent.stream().sorted().forEachOrdered(room -> System.out.println(room));

        System.out.println("Posortowane z użyciem Comparator'a: ");
        Comparator<Room> roomComparator = (room1, room2) -> (int) Math.signum(room1.price - room2.price);
        roomsToRent.stream().sorted(roomComparator).forEachOrdered(room -> System.out.println(room));

        System.out.println("Posortowane z użyciem Comparator'a 2: ");
        Comparator<Room> roomComparator2 = Comparator.comparing(room -> room.getPrice());
        roomsToRent.stream().sorted(roomComparator2).forEachOrdered(room -> System.out.println(room));
    }
}
