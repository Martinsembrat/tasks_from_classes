package sdaacademy.src.d2020_07_11.d2020_07_11;


import java.util.Optional;

public class Optionals {

    public static Optional<String> toString(Object object) {
        return object != null ?
                Optional.of(object.toString()):
                Optional.empty();
    }

    public static Optional<String> toString2(Object object) {
        return Optional.ofNullable(object).map(o -> o.toString());
    }

    public static String toStringUnsafe(Object object) {
        return object.toString();
    }

    public static String toStringUnsafe2(Object object) {
        return object != null ? object.toString(): null;
    }

    // przykład złego postępowania 1
    static class Something {
        private Optional<String> name; // tak nie robimy
    }

    // przykład złego postępowania 2
    // złe bo np. toStringSafe(null)
    public static String toStringSafe(Optional<Object> object) {
        return object.map(o -> o.toString()).orElse("");
    }

    public static void main(String[] args) {
        System.out.println("toStringUnsafe2");
        System.out.println(toStringUnsafe2(1));
        System.out.println(toStringUnsafe2("jakiś tekst"));
        System.out.println(toStringUnsafe2(null));

        String string = toStringUnsafe2(null);
        if(string != null) {
            System.out.println(string);
        }

        System.out.println();
        System.out.println("toString");
        toString(1).ifPresent(object -> System.out.println(object));
        toString("jakiś tekst").ifPresent(object -> System.out.println(object));
        toString(null).ifPresent(object -> System.out.println(object));

        System.out.println();
        String jakisTekst = null; //"jakiś tekst";
        {
            Optional<String> txt1Maybe = toString(jakisTekst).map(s -> s.toUpperCase());
            String txt1;

            if (txt1Maybe.isPresent()) {
                txt1 = txt1Maybe.get();
                System.out.println("txt1: " + txt1);
            }
        }

        String txt2 = toString(jakisTekst).map(s -> s.toUpperCase()).orElse(""); // orElse wymaga aby był gotowy od razy
        System.out.println("txt2: " + txt2);

        String txt3 = toString(jakisTekst)
                .map(s -> s.toUpperCase())
                .orElseGet(() -> String.valueOf(Math.random())); // orElseGet wymaga aby był gotowy w momencie, kiedy trzeba dostarczyć wartość domyślną
        System.out.println("txt3: " + txt3);

        String txt4 = toString(jakisTekst).map(s -> s.toUpperCase()).orElseThrow(() -> new IllegalStateException());
        System.out.println("txt4: " + txt4);
    }
}
