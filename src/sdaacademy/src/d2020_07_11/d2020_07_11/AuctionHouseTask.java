package sdaacademy.src.d2020_07_11.d2020_07_11;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AuctionHouseTask {

//    1. Stwórz klasę Room(miasto, metraż, cena, odległość od centrum),
//    reprezentującą pokój na wynajem.
//            2. Stwórz klasę AuctionHouse zawierającą listę pokoi do wynajęcia.
//3. Klasa AuctionHouse powinna zawierać metody:
//            1) Metoda, która zwróci listę pokoi z podanego jako argument miasta.
//            2) Metoda, która zwróci listę pokoi na podstawie metrażu (od, do).
//            3) Metoda, która zwróci listę wszystkich cen pokoi.
//            4) Metoda, która zwróci listę 3 pokoi do wynajęcia najbliżej centrum.
//    Metoda, która zwróci listę pokoi z podanego jako argument miasta.
//            Metoda, która zwróci listę pokoi na podstawie metrażu (od, do).
//    Metoda, która zwróci listę wszystkich cen pokoi.
//    Metoda, która zwróci listę 3 pokoi do wynajęcia najbliżej centrum.
//4. Zaprezentuj działanie metod klasy ActionHouse za pomocą main lub/i
//    przetestuj za pomocą testów jednostkowych.
    static class Room {

        private String city;
        private double area;
        private double price;
        private double distanceFromCityCenter;

        public Room(String city, double area, double price, double distanceFromCityCenter) {
            this.city = city;
            this.area = area;
            this.price = price;
            this.distanceFromCityCenter = distanceFromCityCenter;
        }

        public String getCity() {
            return city;
        }

        public double getArea() {
            return area;
        }

        public double getPrice() {
            return price;
        }

        public double getDistanceFromCityCenter() {
            return distanceFromCityCenter;
        }

        @Override
        public String toString() {
            return "Room{" +
                    "city='" + city + '\'' +
                    ", area=" + area +
                    ", price=" + price +
                    ", distanceFromCityCenter=" + distanceFromCityCenter +
                    '}';
        }
}

    static class AuctionHouse {

        private final List<Room> rooms = new ArrayList<>();

        public void add(Room room) {
            rooms.add(room);
        }

//        1) Metoda, która zwróci listę pokoi z podanego jako argument miasta.
        public List<Room> findByCity(String cityName) {
            Stream<Room> roomsInTheCity = rooms.stream().filter(room -> Objects.equals(room.getCity(), cityName));
            List<Room> result = roomsInTheCity.collect(Collectors.toUnmodifiableList());
            return result;
        }

        public double totalRoomAreaByCity(String cityName) {
//            return rooms.stream()
//                    .filter(room -> Objects.equals(room.getCity(), cityName))
//                    .collect(Collectors.summingDouble(Room::getArea));
            return rooms.stream()
                    .filter(room -> Objects.equals(room.getCity(), cityName))
                    .mapToDouble(room -> room.getArea())
                    .sum();
        }

//            2) Metoda, która zwróci listę pokoi na podstawie metrażu (od, do).
        public List<Room> findByAreaBetween(double minimumArea, double maximumArea) {
            return rooms.stream()
                    .filter(room -> room.getArea() >= minimumArea && room.getArea() <= maximumArea)
                    .collect(Collectors.toUnmodifiableList());
        }

//            3) Metoda, która zwróci listę wszystkich cen pokoi.
        public List<Double> getRoomPrices() {
            return rooms.stream()
                    .map(room -> room.getPrice())
                    .collect(Collectors.toUnmodifiableList());
        }
//            4) Metoda, która zwróci listę 3 pokoi do wynajęcia najbliżej centrum.
        public List<Room> findNearestToCityCenter(String cityName) {
            return rooms.stream()
                    .filter(room -> Objects.equals(room.getCity(), cityName))
                    .sorted(Comparator.comparingDouble(Room::getDistanceFromCityCenter))
                    .limit(3)
                    .collect(Collectors.toUnmodifiableList());
        }
    }

    private static final List<Room> roomsToRent = List.of(
            new Room("Szczecin", 20.0, 1000, 4.0),
            new Room("Szczecin", 8.0, 600, 2.4),
            new Room("Szczecin", 14.5, 900, 1.5),
            new Room("Szczecin", 14.5, 500, 8.9),
            new Room("Kraków", 20.0, 1400, 1),
            new Room("Gdańsk", 15.0, 1200, 3.0),
            new Room("Warszawa", 12, 1000, 3.5)
    );

    public static void main(String[] args) {
        AuctionHouse auctionHouse = new AuctionHouse();
        roomsToRent.forEach(room -> auctionHouse.add(room));

        System.out.println("W Szczecinie jest " + auctionHouse.findByCity("Szczecin").size() + " pokoi");
        System.out.println("W Krakowie jest " + auctionHouse.findByCity("Kraków").size() + " pokoi");
        System.out.println("W Gdańsku jest " + auctionHouse.findByCity("Gdańsk").size() + " pokoi");
        System.out.println("W Warszawie jest " + auctionHouse.findByCity("Warszawa").size() + " pokoi");

        List<Room> quiteBigRooms = auctionHouse.findByAreaBetween(13, 25);
        System.out.println("Pokoje mające od 13 do 25 m2:");
        quiteBigRooms.forEach(room -> System.out.println(room));

        List<Double> roomPrices = auctionHouse.getRoomPrices();
        System.out.println("Ceny pokoi: " + roomPrices);

        System.out.println("W Szczecinie najbliżej centrum są: ");
        List<Room> szczecinRooms = auctionHouse.findNearestToCityCenter("Szczecin");
        szczecinRooms.forEach(room -> System.out.println(room));
    }
}
