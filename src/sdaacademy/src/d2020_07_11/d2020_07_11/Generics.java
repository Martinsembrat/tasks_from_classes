package sdaacademy.src.d2020_07_11.d2020_07_11;

import java.util.Collection;
import java.util.List;


public class Generics {

    // typ T jest związany z tworzeniem obiektu Option; klasa nie zawiera informacji o tym, czym jest T
    static class Option<T> {

        private T value;

        // typ E jest związany z wywołąniem metody of; kompilator ustawia go na podstawie kontekstu wywołania
        public static <E> Option<E> of(E value) {
            return new Option<E>(value);
        }

//        public static <E> Option<E> empty() {
//            return new Option<E>(null);
//        }

        private Option(T value) {
            this.value = value;
        }

        public T orElse(T defaultValue) {
            return value != null ? value : defaultValue;
        }
    }

    // @SuppressWarnings("unchecked") powoduje ukrycie potenchalnych błędów rzutowania z użyciem wymazywania typów w typach generycznych
    public static void main(String[] args) {
        Option<String> stringMaybe = Option.of("jakiś string");
        System.out.println(stringMaybe.orElse("no value"));

        Option<Integer> integerMaybe = Option.of(1);
        Option<Double> doubleMaybe = Option.of(1.0);

        Option<List<Integer>> listMaybe = Option.of(List.of(1, 2, 3));
        // Option<Collection<?>> collectionMaybe = listMaybe; // błąd kompilacji
        Option<Collection<Integer>> collection2Maybe = (Option) listMaybe;
        Option<? extends Collection<Integer>> collection3Maybe = listMaybe; // wildcard extends Collection<Integer <=> zawieram obiekty będące lub dziedziczące po Collection<Integer>
        Option<? super List<Integer>> collection4Maybe = listMaybe; // wildcard super Collection<Integer <=> zawieram obiekty będące List<Integer> lub typem bazowym List<Integer>
        //////////////////////////////////////////////////////////////

        List<Integer> numbers = List.of(1, 2, 3, 4);
        List<? extends Object> objects = numbers;
        System.out.println(numbers.getClass());
        System.out.println(objects.getClass());
        System.out.println(objects.get(0).getClass());
    }
}
