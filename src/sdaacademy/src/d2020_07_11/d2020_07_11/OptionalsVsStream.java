package sdaacademy.src.d2020_07_11.d2020_07_11;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OptionalsVsStream {


    public static void main(String[] args) {
       List<Integer> numbers = Optional.of(123)
               .stream()
               .collect(Collectors.toUnmodifiableList());

//        List<Integer> numbers = new ArrayList<>();
//        numbers.add(123);

        Optional<Integer> firstEvenNumberMaybe = Stream.of(123, 456, 789).filter(number -> number % 2 == 0).findFirst();
        int firstEvenNumber = Stream.of(123, 456, 789) // Stream<Integer>
                .filter(number -> number % 2 == 0) // Stream<Integer>
                .findFirst() // Optional<Integer>
                .orElse(-1); // Integer
        List<Integer> evenNumbers = Stream.of(123, 456, 789).filter(number -> number % 2 == 0).collect(Collectors.toList());

        // Optional.ofNullable(null).orElseGet(Collections::emptyList)
    }
}
