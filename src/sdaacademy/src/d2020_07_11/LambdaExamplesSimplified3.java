package sdaacademy.src.d2020_07_11;


import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class LambdaExamplesSimplified3 {

    public static void main(String[] args) {
        Function<Double, Double> pow2 = x -> x * x; // pow2 - power of 2
        System.out.println(pow2.apply(6.0));
        System.out.println(pow2.apply(9.0));

        BiFunction<Double, Double, Double> sum = (a, b) -> a + b;
        System.out.println(sum.apply(6.0, 7.0));

        Consumer<Double> printOnScreen = x -> System.out.println(x);
        printOnScreen.accept(53.0);

        Supplier<Double> sampleValue = () -> 1.0;
        System.out.println(sampleValue.get());

        Predicate<Double> isEven = x -> x % 2 == 0;
        System.out.println(isEven.test(7.0));
        System.out.println(isEven.test(10.0));
    }

}
