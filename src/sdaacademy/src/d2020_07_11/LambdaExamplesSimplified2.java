package sdaacademy.src.d2020_07_11;


public class LambdaExamplesSimplified2 {

    // @FunctionalInterface
    interface Calculation {

        double calculate(double a, double b);
    }

    private static double sum(double a, double b) {
        return a + b;
    }

    public static void main(String[] args) {
        // 1.
        LambdaExamplesSimplified2.sum(1, 2);

        // 2. lambda
        Calculation adder = (a, b) -> a + b;
        adder.calculate(1, 2);

        // 3. lambda + method reference
        adder = LambdaExamplesSimplified2::sum;
        adder.calculate(1, 2);
    }

}
