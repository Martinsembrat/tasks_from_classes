package sdaacademy.src.d2020_07_11;

import java.util.ArrayList;
import java.util.List;

public class KlasaAnonimowa {
    private static void correctListInitialization() {
        List<Integer> list = new ArrayList<>(); // tworzymy obiekt ArrayList
        list.add(1);  // dodajemy wartość 1
        list.add(2);
        list.add(3);
    }
    private static void incorrectListInitialization() {
        List<Integer> list = new ArrayList<>() {{ // tworzymy podklasę ArrayList
            add(1); // dodajemy wartość 1 w bloku inicjującym
            add(2);
            add(3);
        }};
    }
}
