package sdaacademy.src.d2020_07_11;

import java.util.*;

public class Task4 {
    //    Utwórz mapę, gdzie kluczem będzie Student, a wartością lista jego ocen.
//• Stwórz kilku studentów oraz ich oceny, a następnie dodaj do mapy.
//• Wyświetl zawartość mapy na konsoli, tak aby w miarę czytelnie dało się
//    odczytać studentów oraz ich oceny.
//    Np. Jan Nowak PL – 2, 3, 5, 5, 4
//    Utwórz klasę StudentService zawierająca poniższe metody.
//            • double calculateAverage(List<Integer> grades) – metoda oblicza średnią ocen
//• double calculateTotalAverage(Map<Student, List<Integer>> studentToGrades) –
//    metoda oblicza średnią wszystkich ocen
//• Student findBestStudent(Map<Student, List<Integer>> studentToGrades) – metoda
//    zwraca studenta z najlepszą średnią
    public static void main(String[] args) {
        Map<Student, List<Integer>> studentGrades = new HashMap<>();
        studentGrades.put(new Student("Jan", "Nowak", "PL"), List.of(2, 3, 5, 5, 4));
        studentGrades.put(new Student("John", "Smith", "EN"), List.of(2, 3, 2, 4, 3));
        studentGrades.put(new Student("Brad", "Pitt", "EN"), List.of(5, 5, 4, 4));
//        Map.Entry entry = grades.entrySet().iterator().next();
//        System.out.println(entry.getKey());
//        System.out.println(entry.getValue());
        Set<Map.Entry<Student, List<Integer>>> studentEntries = studentGrades.entrySet(); // z mapy pobieramy zbiór zawierający pary klucz - wartość (student - jego oceny)
        Iterator<Map.Entry<Student, List<Integer>>> studentEntriesIterator = studentEntries.iterator(); // tutaj tworzymy sobie iterator dla ww. zbioru
        while (studentEntriesIterator.hasNext()) {
            Map.Entry<Student, List<Integer>> entry = studentEntriesIterator.next(); // pobieramy parę student - jego oceny
            Student student = entry.getKey(); // klucz mapy nazywamy zmienną student
            List<Integer> grades = entry.getValue(); // wartość mapy nazywamy zmienną studentGrades
            System.out.println(String.format("%s %s %s - %s",
                    student.getFirstName(),
                    student.getLastName(),
                    student.getMainLanguage(),
                    grades.toString()));
        }
//        for(Map.Entry<Student, List<Integer>> entry: grades.entrySet()) {
//            Student student = entry.getKey();
//            List<Integer> studentGrades = entry.getValue();
//            System.out.println(String.format("%s %s %s - %s",
//                    student.getFirstName(),
//                    student.getLastName(),
//                    student.getMainLanguage(),
//                    studentGrades.toString()));
//        }
    }
    private static final class StudentService {
        public double calculateAverage(List<Integer> grades) {
            if(grades.isEmpty()) {
                return 0;
            }
            int sum = 0;
            for(int grade: grades) {
                sum += grade;
            }
            return sum / (double) grades.size();
        }
        public double calculateTotalAverage(Map<Student, List<Integer>> studentGrades) {
            List<Integer> allGrades = new ArrayList<>();
            for(List<Integer> grades: studentGrades.values()) {
                allGrades.addAll(grades);
            }
            return calculateAverage(allGrades);
        }
        public Student findBestStudent(Map<Student, List<Integer>> studentToGrades) {
            Student bestStudent = null;
            double bestAverage = -1;
            for(Map.Entry<Student, List<Integer>> studentEntry: studentToGrades.entrySet()) {
                Student student = studentEntry.getKey();
                List<Integer> studentGrades = studentEntry.getValue();
                double average = calculateAverage(studentGrades);
                if(average > bestAverage) {
                    bestStudent = student;
                    bestAverage = average;
                }
            }
            return bestStudent;
        }
    }
    private static final class Student {
        private final String firstName;
        private final String lastName;
        private final String mainLanguage;
        public Student(String firstName, String lastName, String mainLanguage) {
            Objects.requireNonNull(firstName);
            Objects.requireNonNull(lastName);
            Objects.requireNonNull(mainLanguage);
            this.firstName = firstName;
            this.lastName = lastName;
            this.mainLanguage = mainLanguage;
        }
        public String getFirstName() {
            return firstName;
        }
        public String getLastName() {
            return lastName;
        }
        public String getMainLanguage() {
            return mainLanguage;
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Student student = (Student) o;
            return firstName.equals(student.firstName) &&
                    lastName.equals(student.lastName) &&
                    mainLanguage.equals(student.mainLanguage);
        }
        @Override
        public int hashCode() {
            return Objects.hash(firstName, lastName, mainLanguage);
        }
    }
}