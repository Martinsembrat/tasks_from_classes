package sdaacademy.src.d2020_07_11;

import java.util.ArrayList;
import java.util.List;

public class LambdaExamples {

    // @FunctionalInterface
    interface Calculation {

        double calculate(double a, double b);
    }

    private double sum2(double a, double b) {
        return a + b;
    }

    private static double sum(double a, double b) {
        return a + b;
    }

    public void run() {
        var c = this.sum2(1, 2);
        var d = LambdaExamples.sum(2, 4);
    }

    public static void main(String[] args) {
        List<Double> numbers = new ArrayList<>();
        numbers.add(2.0);
        numbers.add(12.0);
        numbers.add(22.0);


        // 1.
        LambdaExamples.sum(numbers.get(0), numbers.get(1));

        // 2. klasa anonimowa
        Calculation adder = new Calculation() {
            @Override
            public double calculate(double a, double b) {
                return a + b;
            }
        };

        adder.calculate(numbers.get(0), numbers.get(1));

        // 3. lambda
        adder = (a, b) -> a + b;
        adder.calculate(numbers.get(0), numbers.get(1));

        // 4. lambda + method reference
        adder = LambdaExamples::sum;
        adder.calculate(numbers.get(0), numbers.get(1));
    }


}
