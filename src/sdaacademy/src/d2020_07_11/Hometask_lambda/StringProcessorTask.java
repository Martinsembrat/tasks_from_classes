package sdaacademy.src.d2020_07_11.Hometask_lambda;

public class StringProcessorTask {
    //1. Stwórz interfejs funkcyjny StringOperation z metodą:
    //            •
    //    String operation(String input)
    //2. Utwórz klasę StringProcessor, która będzie zawierać pole typu
    //    StringOperation.
    //3. W klasie StringProcessor, napisz metodę String process(String input),
    //    która skorzysta z StringOperation i na jego podstawie zwróci wynik.
    //            4. W metodzie main przetestuj swoje rozwiązanie.
    //            1) Stwórz StringProcessor, który zwróci napis dużymi literami „abc” -> „ABC”.
    //            2) Zmień działanie StringProcessora, używając settera tak aby zwracał teraz
    //    napis małymi literami.
    //3) Zmień działanie StringProcessora, tak aby zwracał trzy pierwsze litery
    //    podanego Stringa.

    interface StringOperation {

        String operation(String input);
    }

    static class StringProcessor {

        private StringOperation stringOperation;

        public StringProcessor(StringOperation stringOperation) {
            this.stringOperation = stringOperation;
        }

        public void setStringOperation(StringOperation stringOperation) {
            this.stringOperation = stringOperation;
        }

        public String process(String input) {
            return stringOperation.operation(input);
        }
    }

    public static void main(String[] args) {
        StringProcessor processor = new StringProcessor(s -> s.toUpperCase()); // String::toUpperCase <=> s -> s.toUpperCase()
        System.out.println("Abc -> " + processor.process("Abc"));

        processor.setStringOperation(s -> s.toLowerCase());
        System.out.println("Abc -> " + processor.process("Abc"));
    }
}
