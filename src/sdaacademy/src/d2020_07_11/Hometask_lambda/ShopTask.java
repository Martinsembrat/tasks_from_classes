package sdaacademy.src.d2020_07_11.Hometask_lambda;

import java.time.LocalDate;
import java.util.*;
import java.util.function.DoubleSupplier;
import java.util.function.Predicate;

public class ShopTask {

    private interface Product {

        double getPrice();

        boolean isAvailable(LocalDate date);
    }

    private static class GenericProduct implements Product {

        private final DoubleSupplier priceSupplier;

        private final Predicate<LocalDate> availabilityPredicate;

        public GenericProduct(DoubleSupplier priceSupplier, Predicate<LocalDate> availabilityPredicate) {
            Objects.requireNonNull(priceSupplier);
            Objects.requireNonNull(availabilityPredicate);

            this.priceSupplier = priceSupplier;
            this.availabilityPredicate = availabilityPredicate;
        }

        @Override
        public double getPrice() {
            return priceSupplier.getAsDouble();
        }

        @Override
        public boolean isAvailable(LocalDate date) {
            return availabilityPredicate.test(date);
        }
    }

    private static class Basket {

        private final List<Product> products = new ArrayList<>();

        public void add(Product product) {
            products.add(product);
        }

        public void remove(Product product) {
            products.remove(product);
        }

        public List<Product> content() {
            return Collections.unmodifiableList(products);
        }
    }

    private static class Basket2 {

        private final Map<Product, Integer> products = new HashMap<>();

        public void add(Product product) {
            add(product, 1);
        }

        public void add(Product product, int times) {
            products.compute(product, (key, value) -> (value == null ? 0 : value) + times);
        }

        public void remove(Product product) {
            remove(product, 1);
        }

        public void remove(Product product, int times) {
            products.compute(product, (key, value) -> (value == null ? 0 : value) - times);
            products.remove(product, 0);
        }

        public Map<Product, Integer> content() {
            return Collections.unmodifiableMap(products);
        }
    }

    public static void main(String[] args) {
        // basket1Test();
        basket2Test();
    }

    private static void basket1Test() {
        Basket basket = new Basket();
        Product bananas = new GenericProduct(() -> 5.0, date -> date.equals(LocalDate.now()));
        Product apples = new GenericProduct(() -> 3.0, date -> date.equals(LocalDate.now()));
        Product tomatoes = new GenericProduct(() -> 7.0, date -> date.isAfter(LocalDate.now()));

        basket.add(bananas);
        basket.add(apples);
        basket.add(tomatoes);

        basket.content().forEach(product -> {
            if (product.isAvailable(LocalDate.now())) {
                System.out.println(product.getPrice());
            }
        });
    }

    private static void basket2Test() {
        Basket2 basket = new Basket2();
        Product bananas = new GenericProduct(() -> 5.0, date -> date.equals(LocalDate.now()));
        Product apples = new GenericProduct(() -> 3.0, date -> date.equals(LocalDate.now()));
        Product tomatoes = new GenericProduct(() -> 7.0, date -> date.isAfter(LocalDate.now()));

        basket.add(bananas, 5);
        basket.add(apples, 20);
        basket.add(tomatoes, 13);

        basket.content().forEach((product, count) -> {
            if (product.isAvailable(LocalDate.now())) {
                System.out.println(count + " szt. za " + product.getPrice());
            }
        });
    }
}
