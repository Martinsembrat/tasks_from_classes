package sdaacademy.src.d2020_07_11.Hometask_lambda;


import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;

public class ListsRemoveWithLambda {

        public static void main(String... args) {
            Collection<Integer> numbers = new HashSet<>();

            for(int i = 0; i < 20; ++i) {
                int number = ThreadLocalRandom.current().nextInt(0, 11);
                numbers.add(number);
            }

            System.out.print("Przed filtrowaniem: ");
            for(int number: numbers) {
                System.out.print(number);
                System.out.print(" ");
            }
            System.out.println("");

            numbers.removeIf(number -> number % 2 == 0);
            // to samo co linia powyżej
//            Iterator<Integer> numberIterator = numbers.iterator();
//
//            while (numberIterator.hasNext()) {
//                int number = numberIterator.next();
//                if(number % 2 == 0) {
//                    numberIterator.remove();
//                }
//            }

            System.out.print("Po filtrowaniem: ");
            for(int number: numbers) {
                System.out.print(number);
                System.out.print(" ");
            }
            System.out.println("");
        }
}
