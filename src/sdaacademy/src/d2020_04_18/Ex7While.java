package week2;

import java.util.Scanner;

/*
Zadanie 7. Napisać program, który pobiera od użytkownika liczbę całkowitą dodatnią,
a na- stępnie wyświetla na ekranie kolejno wszystkie liczby niepatrzyste nie większe od podanej liczby.
Przykład, dla 15 program powinien wyświetlić 1, 3, 5, 7, 9, 11, 13, 15.
 */
public class Ex7While {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int i=1;
        while (i <= number) {
            System.out.println(i);
            i=i+2;//i+=2
        }

        //alternatywnie
        int j=0;
        while(j<=number) {
            if (j%2 != 0){
                System.out.println(j);
            }
            j++;
        }

    }
}
