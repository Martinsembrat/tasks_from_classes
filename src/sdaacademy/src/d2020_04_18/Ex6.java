package sdaacademy.src.d2020_04_18;

import java.util.Scanner;

/*
Zadanie 6. Napisać program realizujący funkcje prostego kalkulatora,
pozwalającego na wy- konywanie operacji dodawania, odejmowania, mnożenia i dzielenia na dwóch licz- bach rzeczywistych.
 rogram ma identyfikować sytuację wprowadzenia błędnego symbolu działania oraz próbę dzielenia przez zero.
Zastosować instrukcję switch do wykonania odpowiedniego działania w zależności od wprowadzonego symbolu operacji.
Scenariusz działania programu:
a) Program wyświetla informację o swoim przeznaczeniu. b) Wczytuje pierwszą liczbę.
c) Wczytuje symbol operacji arytmetycznej: +, -, *, /.
d) Wczytuje drugą liczbę.
e) Wyświetla wynik lub - w razie konieczności - informację o niemożności wy- konania działania.
f) Program kończy swoje działanie po naciśnięciu przez użytkownika klawisza n
 */
public class Ex6 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char tryAgain;
        do {
            System.out.println("Podaj znak operacji: ");
            char operation = input.next().charAt(0);
            System.out.println("Podaj liczbę nr 1");
            int number1 = input.nextInt();
            System.out.println("Podaj liczbę nr 2");
            int number2 = input.nextInt();
            int result = 0;
            switch (operation) {
                case '+':
                    result = number1 + number2;
                    System.out.println(result);
                    break;
                case '-':
                    result = number1 - number2;
                    System.out.println(result);
                    break;
                case '*':
                    result = number1 * number2;
                    System.out.println(result);
                    break;
                case '/':
                    if (number2 != 0) {
                        result = number1 / number2;
                        System.out.println(result);
                    } else {
                        System.out.println("Nie można dzielić przez 0!");
                    }
                    break;
                default:
                    System.out.println("Niewspierana operacja!");

            }
            System.out.println("Czy chcesz kontynuować: (y/n)?");
            tryAgain = input.next().charAt(0); //pobranie pojedynczego znaku z klawiatury
        }while (tryAgain == 'y');
    }
}
