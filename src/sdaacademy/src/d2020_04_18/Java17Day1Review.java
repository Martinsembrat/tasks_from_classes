package week2;

import java.util.Scanner;

public class Java17Day1Review {

    public static void main(String[] args) {
        char exampleChar; //deklaracja
        char exampleCharWithValue = 'a'; //deklaracja + przypisanie

        short shortExample;
        byte byteExample; //najmniejszy
        int intExample;
        long longExample; //największy

        longExample = 43;

        float floatExample = 43.0F;
        double doubleExample;

        boolean booleanExample = true;//false

        System.out.println(booleanExample);

        Scanner input = new Scanner(System.in);

        doubleExample = input.nextDouble();//odczyt z klawiatury

        System.out.println(doubleExample);

        //Operator
        int intExample2 = 80;
        intExample = 10;
        int result = intExample - intExample2;
        intExample = 10*20;
        intExample = 90-50;
    }
}
