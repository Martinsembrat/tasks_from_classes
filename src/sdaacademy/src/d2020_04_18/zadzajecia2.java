package sdaacademy.src.d2020_04_18;

import java.util.Scanner;

public class zadzajecia2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe wierszy: ");
        int row = scanner.nextInt();
        System.out.println("Podaj liczbe kolumn: ");
        int column = scanner.nextInt();

        for (int i=0;i<row;i++){

            for (int space=0; space < i ; space++) {
                System.out.print(" ");
            }

            for (int j=0;j<column;j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}

