package sdaacademy.src.d2020_04_18;

import java.util.Scanner;

/*
Zadanie 7. Napisać program, który pobiera od użytkownika liczbę całkowitą dodatnią,
a na- stępnie wyświetla na ekranie kolejno wszystkie liczby niepatrzyste nie większe od podanej liczby.
Przykład, dla 15 program powinien wyświetlić 1, 3, 5, 7, 9, 11, 13, 15.
 */
public class Ex7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        for (int i=0; i <= number; i++) {
            if (i % 2 != 0) {
                System.out.println(i);
            }
            //alternatywnie
            /*if (i % 2 == 0) {
                continue;
                //niechcemy wykonywać instrukcji niżej, i idziemy do kolejnej iteracji
            }
            System.out.println(i);*/
        }
    }
}
