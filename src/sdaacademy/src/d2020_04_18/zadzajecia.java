package sdaacademy.src.d2020_04_18;

import java.util.Scanner;

public class zadzajecia {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe wierszy: ");
        int row = scanner.nextInt();
        System.out.println("Podaj liczbe kolumn: ");
        int column = scanner.nextInt();
        System.out.println("Podaj znak uzupełniający: ");
        char sign = scanner.next().charAt(0);
        for (int i=0;i<row;i++){
            //pętla zagnieżdżona odpowiedzialna za wyświetlenie przesunięca w ramach wiersza
            for (int space=0; space < row - i - 1; space++) {
                System.out.print(" ");
            }
            //pętla zagnieżdżona odpowiedzialna za wyświetlenie znaku wskazanego przez użytkownika
            for (int j=0;j<column;j++) {
                System.out.print(sign);
            }
            System.out.println();
        }
    }
}