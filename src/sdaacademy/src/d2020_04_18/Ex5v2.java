package sdaacademy.src.d2020_04_18;

public class Ex5v2 {

    public static void main(String[] args) {
        //int[] tab = new int[5];
        int[] tab = { 14, 5, 3, 8, 7 };//tworzona i inicjalizowana jest tablica 5 elementowa
        for (int i=0;i<tab.length;i++) {
            System.out.println(tab[i]);
        }

        //przechodzenie po wszystkich elementach tablicy
        for (int value : tab) {
            System.out.println(value);
        }
    }
}
