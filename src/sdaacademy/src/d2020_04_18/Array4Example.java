package sdaacademy.src.d2020_04_18;

import java.util.Scanner;

public class Array4Example {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj wartość z klawiatury: ");
        int tabSize = input.nextInt();
        int[] intTab = new int[tabSize];
        boolean[] booleanTab = new boolean[tabSize];
        for (int i=0;i<intTab.length;i++) {
            System.out.println("Podaj liczbę z klawiatury: ");
            int number = input.nextInt();
            intTab[i] = number * number;
            booleanTab[i] = (number % 2 == 0);
        }

        for (int i=0;i<intTab.length;i++) {
            System.out.println(intTab[i]);
            System.out.println(booleanTab[i]);
        }
    }
}
