package sdaacademy.src.d2020_04_18;

public class Zad_Tablce1 {
    public static void main(String[] args) {
        int[][] tab = {
                {1, 2, 4},
                {2, 4, 1},
                {2, 2, 0}
        };
        for (int i=0;i<tab.length;i++) {
            boolean answer = true;
            for (int j=0;j<tab[i].length;j++) {
                if (tab[i][j] % 2 != 0) {
                    answer = false;
                    break;
                }
            }
            if (answer) {
                System.out.print("Dany wiersz zawiera same parzyste wartości!");
                break;
            } else {
                System.out.println("Brak spełnienia założeń!");
                //break; -> przerwiemy działanie po pierwszym przebiegu pętli: nie poprawne!
            }
        }
    }
}