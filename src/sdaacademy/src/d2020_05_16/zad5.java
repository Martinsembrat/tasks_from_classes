package sdaacademy.src.d2020_05_16;

import java.util.Arrays;

public class zad5 {
    public static void main(String[] args) {
        int[] tab1 = {1,4,6,7};
        int[] tab2 = {2,3,8};
        int[] tab3 = new int[tab1.length + tab2.length];
        System.out.println(tab3.length);

        for (int i = 0; i < tab3.length; i++) {
            if (i < tab1.length) {
                tab3[i] = tab1[i];
            } else tab3[i] = tab2[i-tab1.length];
        }
        Arrays.sort(tab3);
        for (int i = 0; i < tab3.length; i++) {
            System.out.print(tab3[i]+" ");
        }
    }
}
