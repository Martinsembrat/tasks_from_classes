package sdaacademy.src.d2020_05_16;

public class TextTests {
    public static boolean isPalindrome(String input) {
        if (input == null) {
            return true;
        }
        String data = input.toLowerCase();
        int length = data.length();
        for(int i = 0; i < length/2; i++) {
            if (data.charAt(i) != data.charAt(length - i - 1)) {
                return false;
            }
        }

        return true;
    }
    public static boolean isAnagram(String str1, String str2) {
        if (str1.length() != str2.length()) {
            return false;
        }
        str1 = str1.toLowerCase();
        str2 = str2.toLowerCase();
        int[] counters = new int['z' - 'a' + 1];
        for (int i = 0; i < str1.length(); i++) {
            counters[str1.charAt(i) - 'a']++;
        }
        for (int i = 0; i < str2.length(); i++) {
            counters[str2.charAt(i) - 'a']--;
        }
        for (int i = 0; i < counters.length; i++) {
            if (counters[i] != 0) {
                return false;
            }
        }
        return true;
    }
}
