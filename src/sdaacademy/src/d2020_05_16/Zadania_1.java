package sdaacademy.src.d2020_05_16;

import java.util.Arrays;

public class Zadania_1 {
    //wyszukiwanie najwyższe i najniższej wartości w tablicy
    public static void findHighestAndLowest(int[] tab) {
        int min = tab[0];
        int max = tab[0];
        for(int i = 0; i < tab.length; i++) {
            if (min > tab[i]) {
                min = tab[i];
            }
            if (max < tab[i]) {
                max = tab[i];
            }
        }

        System.out.println("Min: " + min);
        System.out.println("Max: " + max);
    }

    //odwracanie tablicy "w miejscu" (bez użycia tablicy pomocniczej)
    public static void reverseArray(int[] arr) {
        System.out.println(Arrays.toString(arr));
        for (int i = 0; i < arr.length/2; i++) {
            int tmp = arr[i];
            arr[i] = arr[arr.length - 1];
            arr[arr.length - 1] = tmp;
        }
        System.out.println(Arrays.toString(arr));
    }

    //obliczanie najczęściej występującego elementu tablicy przy użyciu tablicy liczników
    //założenie: max = 1000000
    public static int findMostFrequent_v2(int[] tab, int max) {
        int[] counters = new int[max]; //max 3 {3,0,3} counters {1, ,2 }
        for (int i = 0; i < tab.length; i++) {
            counters[tab[i]]++;
        }
        int maxFound = counters[0];
        for (int i = 0; i < counters.length; i++) {
            if (max < counters[i]) {
                maxFound = counters[i];
            }
        }
        return maxFound;
    }

    //obliczanie najczęściej występującego elementu tablicy
    public static int findMostFrequent(int[] tab) {
        //ustawiamy liczniki
        int count = 0;
        int maxCount = 0;
        //indeks liczby najczesciej pojawiajacej sie
        int id = 0;
        //dla kazdej liczby w tablicy...
        for (int i = 0; i < tab.length; i++) {
            //porownaj z pozostalymi liczbami...
            for (int j = 0; j < tab.length; j++) {
                //i zwieksz licznik jesli trzeba
                if (tab[i] == tab[j]) {
                    count++;
                }
            }
            //jesli znaleziono nowy max, zaktualizuj wartosc i zapamietaj nowy indeks
            if (maxCount < count) {
                maxCount = count;
                id = i;
            }
            //wyczysc licznik tymczasowy
            count = 0;
        }
        return tab[id];
    }

    //obliczanie silni metodą rekurencyjną
    public static int factorial(int a) {
        if (a == 1) {
            return 1;
        }
        return a * factorial(a-1);
    }

    //obliczanie ciągu Fibonacciego
    public static void fib(int max) {
        //wartości dwóch pierwszych wyrazów wynikające z definicji
        int a1 = 1;
        int a2 = 1;
        for (int i = 0; i < max; i++) {
            if (i < 2) {
                //dla dwóch pierwszych po prostu wypisujemy '1'
                System.out.print(1 + " ");
            } else {
                //dla kolejnych obliczamy sumę dwóch poprzednich wyrazów...
                int sum = a2 + a1;
                System.out.print(sum + " ");
                //i aktualizujemy wartości dwóch poprzednich wyrazów
                a2 = a1;
                a1 = sum;
            }
        }
    }

    //obliczanie wyrazu ciągu Fibonacciego metodą rekurencyjną
    public static int fibR(int a) {
        if (a == 0 || a == 1) {
            return 1;
        }

        return fibR(a-2) + fibR(a-1);
    }
}
