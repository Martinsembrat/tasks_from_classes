package sdaacademy.src.d2020_05_16;

public class AsciiFigures {
    public static void drawSquare(int size) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i < j) {
                    System.out.print("o ");
                } else {
                    System.out.print("* ");
                }
            }
            System.out.println();
        }
    }
}
