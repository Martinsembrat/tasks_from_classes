package sdaacademy.src.d2020_05_16;

import java.util.List;
import java.util.Scanner;

public class Main {

    //wypisuje liczby od 1 do 100
    public static void from1to100() {
        for (int i = 1; i <= 100; i++) {
            System.out.println(i);
        }
    }

    //wypisuje liczby z zakresu określonego przez użytkownika
    public static void printInScope() {
        Scanner scanner = new Scanner(System.in);
        int start = scanner.nextInt();
        int stop = scanner.nextInt();
        scanner.close();
        for (int i = start; i <= stop; i++) {
            System.out.println(i);
        }
    }

    //wypisuje liczby z zakresu określonego przez użytkownika
    //kolejne liczby oddalone są o krok określany przez użytkownika
    public static void printInScopeWithStep() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj początek przedziału:");
        int start = scanner.nextInt();
        System.out.println("Podaj koniec przedziału:");
        int stop = scanner.nextInt();
        System.out.println("Podaj krok:");
        int step = scanner.nextInt();
        scanner.close();
        for (int i = start; i <= stop; i = i + step) {
            System.out.println(i);
        }
    }

    //sprawdzenie czy dana liczba jest parzysta
    public static boolean isOdd(int num) {
        if (num % 2 == 0) {
            return false;
        }

        return true;
    }

    //sprawdzenie czy kolejna podawana przez użytkownika liczba
    // jest parzysta; brak eleganckiego wyjścia z pętli
    public static void checkIfOdd() {
        Scanner scanner = new Scanner(System.in);
        while(true) {
            System.out.println("Podaj liczbę:");
            int value = scanner.nextInt();
            boolean result = isOdd(value);
            if (result == true) {
                System.out.println("Is odd");
            } else {
                System.out.println("Is even");
            }
        }
    }


    public static int[] concatArrays(int[] arr1, int[] arr2) {
        if (arr1 == null) {
            return arr2;
        }
        if (arr2 == null) {
            return arr1;
        }
        int[] result = new int[arr1.length + arr2.length];
        for (int i = 0; i < arr1.length; i++) {
            result[i] = arr1[i];
        }
        for (int i = 0; i < arr2.length; i++) {
            result[i + arr1.length] = arr2[i];
        }
        return result;
    }

    public static int[] concatArraysSorted(int[] arr1, int[] arr2) {
        if (arr1 == null) {
            return arr2;
        }
        if (arr2 == null) {
            return arr1;
        }
        int[] result = new int[arr1.length + arr2.length];
        int a1Idx = 0;
        int a2Idx = 0;
        for (int i = 0; i < result.length; i++) {
            if (a1Idx < arr1.length && a2Idx < arr2.length) {
                if (arr1[a1Idx] < arr2[a2Idx]) {
                    result[i] = arr1[a1Idx];
                    a1Idx++;
                } else {
                    result[i] = arr2[a2Idx];
                    a2Idx++;
                }
            } else if (a1Idx >= arr1.length) {
                result[i] = arr2[a2Idx];
                a2Idx++;
            } else {
                result[i] = arr1[a1Idx];
                a1Idx++;
            }
        }
        return result;
    }

    public static int[] convertWithString(int number) {
        //konwersja liczby na tekst
        String numberString = String.valueOf(number);

        int [] numArr = new int[numberString.length()];

        for (int i = 0; i < numberString.length(); i++) {
            //pobranie znak z przedziału [i, i+1)
            String charStr = numberString.substring(i, i+1);
            //konwersja ciągu znaków (w tym przpadku ciągu 1-no elementowego) na liczbę
            numArr[i] = Integer.valueOf(charStr);
        }
        return numArr;
    }

    public static int[] convertNumToArray(int number) {
        int digits = 0;
        int tmp = number;
        do {
            digits++;
            tmp /= 10;
        } while (tmp > 0);

        int[] result = new int[digits];
        int exponent = digits - 1;
        int i = 0;
        while(exponent >= 0) {
            result[i] = number / (int) Math.pow(10, exponent);
            number -= result[i] * (int) Math.pow(10, exponent);
            exponent--;
            i++;
        }
        return result;
    }

    public static void invertNumber(int num, List<Integer> list) {
        if (num > 0) {
            invertNumber(num/10, list);
            list.add(num%10);
        }
    }

    public static void intToChar(int i) {
        char c = (char) i;
        System.out.println("code " + i + " is char: " + c);
    }
    public static void charToInt(char c) {
        int i = (int) c;
        System.out.println("char: " + c + " have code: " + i);
    }

    public static void main(String[] args) {
        String text = "annb";
        String text1 = "anna";
        String text2 = "nzna";
        boolean result = TextTests.isAnagram(text1, text2);
        if (result) {
            System.out.println(text1 + " is anagram of " + text2);
        } else {
            System.out.println(text1 + " is not anagram of " + text2);
        }
//        boolean result = TextTests.isPalindrome(text);
//        if (result) {
//            System.out.println(text + " is a palindrome");
//        } else {
//            System.out.println(text + " is not a palindrome");
//        }
    }
}
