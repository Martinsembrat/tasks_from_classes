package sdaacademy.src.d2020_09_19.Iterator2;

public class Team {

    private String name;
    private String teamType;

    public Team(String name, String teamType) {
        this.name = name;
        this.teamType = teamType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType;
    }

    @Override
    public String toString() {
        return "Team{" +
                "name='" + name + '\'' +
                ", teamType='" + teamType + '\'' +
                '}';
    }
}
