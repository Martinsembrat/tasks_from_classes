package sdaacademy.src.d2020_09_19.Iterator2;

import java.util.ArrayList;
import java.util.List;

public class FootbalTeamCollection {

    private List<Team> team1 = new ArrayList<>();

    public void add(Team teamMember) {
        team1.add(teamMember);
        //moveServer.post(movie);
    }


    public TeamIteratorApi getIterator() { return new TeamIterator(); }

    private class TeamIterator implements TeamIteratorApi {
        private int position;

        @Override
        public boolean hasNext() {
            return position < team1.size();
        }

        @Override
        public Team next() {
            if (hasNext()) {
                return team1.get(position++);
            }
            return null;
        }

    }
}
