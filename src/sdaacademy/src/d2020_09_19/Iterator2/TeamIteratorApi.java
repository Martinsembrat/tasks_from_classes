package sdaacademy.src.d2020_09_19.Iterator2;

public interface TeamIteratorApi {
    boolean hasNext();
    Team next();
}
