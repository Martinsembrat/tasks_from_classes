package sdaacademy.src.d2020_09_19.Iterator2;



public class TeamCreator {
    public static void main(String[] args) {
        FootbalTeamCollection teamCollection = new FootbalTeamCollection();
        teamCollection.add(new Team("Pogoń", "FootbalTeam"));
        teamCollection.add(new Team("Legia", "FootbalTeam"));
        teamCollection.add(new Team("Real", "FootbalTeam"));

        TeamIteratorApi iteratorApi = teamCollection.getIterator();
        while (iteratorApi.hasNext()) {
            System.out.println(iteratorApi.next());
        }
    }
}
