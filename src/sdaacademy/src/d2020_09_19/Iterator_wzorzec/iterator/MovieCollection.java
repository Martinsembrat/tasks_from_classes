package sdaacademy.src.d2020_09_19.Iterator_wzorzec.iterator;

import java.util.ArrayList;
import java.util.List;

public class MovieCollection {

    private List<Movie> movies = new ArrayList<>();
    //private MovieServer moveServer;

    public void add(Movie movie) {
        movies.add(movie);
        //moveServer.post(movie);
    }

    public MovieIteratorApi getIterator() {
        return new MovieIterator();
    }

    private class MovieIterator implements MovieIteratorApi {

        //domyślnie równa 0
        private int position;

        @Override
        public boolean hasNext() {
            return position < movies.size();
        }

        @Override
        public Movie next() {
            if (hasNext()) {
                return movies.get(position++);
            }
            return null;
        }
    }
}
