package sdaacademy.src.d2020_09_19.Iterator_wzorzec.iterator;

public class Cinema {

    public static void main(String[] args) {
        MovieCollection movieCollection = new MovieCollection();
        movieCollection.add(new Movie("Star Wars", "Action"));
        movieCollection.add(new Movie("Harry Potter", "Fantasy"));
        movieCollection.add(new Movie("Avengers", "Action"));

        MovieIteratorApi iteratorApi = movieCollection.getIterator();
        while (iteratorApi.hasNext()) {
            System.out.println(iteratorApi.next());
        }
    }
}
