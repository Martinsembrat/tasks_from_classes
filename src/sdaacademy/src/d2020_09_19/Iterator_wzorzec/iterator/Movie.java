package sdaacademy.src.d2020_09_19.Iterator_wzorzec.iterator;

public class Movie {

    private String title;
    private String movieType;

    public Movie(String title, String movieType) {
        this.title = title;
        this.movieType = movieType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMovieType() {
        return movieType;
    }

    public void setMovieType(String movieType) {
        this.movieType = movieType;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", movieType='" + movieType + '\'' +
                '}';
    }
}
