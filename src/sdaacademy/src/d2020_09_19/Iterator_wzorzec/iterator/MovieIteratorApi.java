package sdaacademy.src.d2020_09_19.Iterator_wzorzec.iterator;

public interface MovieIteratorApi {
    boolean hasNext();
    Movie next();
}
