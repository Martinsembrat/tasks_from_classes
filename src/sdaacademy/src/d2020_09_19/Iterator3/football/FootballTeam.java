package sdaacademy.src.d2020_09_19.Iterator3.football;

import java.util.ArrayList;
import java.util.List;

public class FootballTeam {

    private List<FootballPlayer> footballPlayers = new ArrayList<>();

    public void add(FootballPlayer footballPlayer) {
        this.footballPlayers.add(footballPlayer);
    }

    public FootballTeamIteratorApi getIterator() {
        return new FootballTeamIterator();
    }

    private class FootballTeamIterator implements FootballTeamIteratorApi {

        private int position;

        @Override
        public boolean hasNext() {
            return position < footballPlayers.size();
        }

        @Override
        public FootballPlayer next() {
            if (hasNext()) {
                return footballPlayers.get(position++);
            }
            return null;
        }
    }
}
