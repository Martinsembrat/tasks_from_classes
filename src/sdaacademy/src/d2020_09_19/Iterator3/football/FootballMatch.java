package sdaacademy.src.d2020_09_19.Iterator3.football;

public class FootballMatch {

    public static void main(String[] args) {
        FootballTeam footballTeam = new FootballTeam();
        footballTeam.add(new FootballPlayer("C. Ronaldo"));
        footballTeam.add(new FootballPlayer("L. Messi"));
        footballTeam.add(new FootballPlayer("R. Lewandowski"));
        FootballTeamIteratorApi footballTeamIteratorApi = footballTeam.getIterator();
        //brak dostępu do klasy prywatnej
        //FootballTeamIterator iterator = null;
        while (footballTeamIteratorApi.hasNext()) {
            System.out.println(footballTeamIteratorApi.next());
        }
    }
}
