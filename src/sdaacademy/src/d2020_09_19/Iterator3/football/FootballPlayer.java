package sdaacademy.src.d2020_09_19.Iterator3.football;

public class FootballPlayer {
    private String name;

    public FootballPlayer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FootballPlayer{" +
                "name='" + name + '\'' +
                '}';
    }
}
