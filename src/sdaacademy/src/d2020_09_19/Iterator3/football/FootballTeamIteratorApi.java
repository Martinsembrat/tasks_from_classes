package sdaacademy.src.d2020_09_19.Iterator3.football;

public interface FootballTeamIteratorApi {

    boolean hasNext();
    FootballPlayer next();
}
