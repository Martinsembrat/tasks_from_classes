package home_tasks;

public class Ex13 {

    public static void main(String[] args) {
        //inicjalizacja tablicy bazowej
        int[] numbers = {20, 20, 30, 40, 50, 50, 50};
        //inicjalizacja tablicy na unikalne elementy
        //rozmiar tablicy musi być taki jak rozmiar tablicy bazowej
        // bo w najgorszym wypadku tablica bazowa może nie zawierać duplikatów
        int[] uniqueNumbers = new int[numbers.length];
        for (int i=0;i<numbers.length;i++) {
            //domyślnie zakładamy, że podany jest unikalny
            boolean isUnique = true;
            //sprawdzamy czy wśród podanych unikalnych liczb
            // istnieje już podana wartość
            for (int unique : uniqueNumbers) {
                //jeśli warunek jest prawdziwy to mamy pewność,
                // że tabeli na wartości unikalnej znajduje się już podana wartośc
                if (numbers[i] == unique) {
                    //ustawiamy flagę na false
                    isUnique = false;
                    //przerywamy działanie zagnieżdżonej pętli
                    break;
                }
            }
            //jeśli flaga isUnique ma wartość false to znaczy że element z tablicy
            // number jest unikalnym elementem drugiej tablicy,
            // w przeciwnym przypadku ustawiamy pod konkretnym indeksem wartość -1
            if (isUnique) {
                uniqueNumbers[i] = numbers[i];
            } else {
                uniqueNumbers[i] = -1;
            }
        }
        for (int unique : uniqueNumbers) {
            System.out.println(unique);
        }
    }
}
