package home_tasks;

/*
Zadanie 14. Napisz program obliczający różnicę pomiędzy
największą i najmniejszą wartością z tablicy.
Sample array: [20, 20, 30, 40, 50, 50, 50] result: 50 - 20 = 30
 */
public class Ex14 {

    public static void main(String[] args) {
        int[] tab = {20, 20, 30, 40, 50, 50, 50};
        int min = tab[0];
        int max = tab[0];
        for (int i=1;i<tab.length;i++) {
            if (min > tab[i]) {
                min = tab[i];
            }
            if (max < tab[i]) {
                max = tab[i];
            }
        }

        int difference = max - min;
        System.out.print("Różnica pomiędzy min i max: ");
        System.out.println(difference);
    }
}
