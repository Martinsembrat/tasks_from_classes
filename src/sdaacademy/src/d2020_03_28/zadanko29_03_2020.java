package sdaacademy.src.d2020_03_28;

import java.util.Scanner;

public class zadanko29_03_2020 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        float liczba1;
        float liczba2;

        System.out.println("podaj liczbę 1");
        liczba1 = input.nextFloat();
        System.out.println("podaj liczbę 2");
        liczba2 = input.nextFloat();

        if (liczba1 == liczba2) {
            System.out.println("liczby są równe");
        } else if (liczba1 > liczba2) {
            System.out.println("liczba 1 jest wieksza od liczba 2 i sa od siebie rozne");
        } else if (liczba1 <= liczba2) {
            System.out.println("liczba 1 jest mniejsza lub rowna liczbie 2 i sa od siebie rozne");
        }
    }
}
