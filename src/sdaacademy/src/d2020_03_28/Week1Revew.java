package sdaacademy.src.d2020_03_28;
import java.util.Scanner;
public class Week1Revew {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int numberOfClients = 10;
        for (int i=0; i<numberOfClients;i++) {
            System.out.println("Podaj swój wiek");
            int age = input.nextInt();
            if (age < 16) {
                System.out.println("Nie możesz obejrzeć filmu!");
            } else if (age >= 16 && age < 18) {
                System.out.println("Możesz obejrzeć film tylko za zgodą rodzica!");
            } else {
                System.out.println("Możesz obejrzeć film!");
            }
            System.out.println("Kolejna osoba! ----------------");
        }
//        //alternatywna wersja zakładając że wiek jest większy od 0
//        switch (age) {
//            case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8:
//                System.out.println("Nie możesz obejrzeć filmu!");
//                break;
//            case 16: case 17:
//                System.out.println("Możesz obejrzeć film tylko za zgodą rodzica!");
//                break;
//            default:
//                System.out.println("Możesz obejrzeć film!");
//        }
    }
}