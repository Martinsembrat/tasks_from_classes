package sdaacademy.src.d2020_03_28;

import java.util.Scanner;

public class Zad6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        float liczba1;
        float liczba2;
        char operation;
        int operationNumber = 0;

        System.out.println("kalkulator 2 liczbowy");
        System.out.println("wprowadz pierwszą liczbę");
        liczba1 = input.nextFloat();
        System.out.println("wprowadz drugą liczbę");
        liczba2 = input.nextFloat();
        System.out.println("wprowadz znak działania");
        operation = input.next().charAt(0);

        if (operation == '+') {
            operationNumber = 1;
        } else if (operation == '-') {
            operationNumber = 2;
        } else if (operation == '*') {
            operationNumber = 3;
        } else if (operation == '/') {
            operationNumber = 4;
        }
        System.out.println(operationNumber);
        switch (operationNumber) {
            case 1:
                System.out.println(liczba1 + liczba2);
                break;
            case 2:
                System.out.println(liczba1 - liczba2);
                break;
            case 3:
                System.out.println(liczba1 * liczba2);
                break;
            case 4:
                System.out.println(liczba1 / liczba2);
                break;
            default:
                System.out.println("błąd");

        }
    }
}
