package sdaacademy.src.d2020_03_28;

import java.util.Scanner;

public class Zad3 {
    public static void main(String[] args) {
        float wzrost;
        float wiek;
        float waga;
        float BMI;
        float wzrostDoPotęgi;
        Scanner pobieranieZKlawiatury = new Scanner(System.in);

        System.out.println("podaj wagę");
        waga = pobieranieZKlawiatury.nextFloat();

        System.out.println("podaj wzrost");
        wzrost = pobieranieZKlawiatury.nextFloat();

        System.out.println("podaj wiek");
        wiek = pobieranieZKlawiatury.nextFloat();

        System.out.println("BMI");
        wzrostDoPotęgi = (float) Math.pow(wzrost,2);
        BMI = waga/wzrostDoPotęgi;
        System.out.println(BMI);

        if (BMI >18.5 && BMI <24.9) {
            System.out.println("waga prawidłowa");
        } else if ( BMI < 18.5) {
            System.out.println("niedowaga");
        } else {
            System.out.println("nadwaga");
        }
    }
}
