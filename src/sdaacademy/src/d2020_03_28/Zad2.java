package sdaacademy.src.d2020_03_28;

import java.util.Arrays;
import java.util.Scanner;

public class Zad2 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        float liczba1;
        float liczba2;
        float liczba3;

        System.out.println("wprowadz 3 liczby");
        liczba1 = input.nextFloat();
        liczba2 = input.nextFloat();
        liczba3 = input.nextFloat();

        float[] nums={liczba1, liczba2, liczba3};
        Arrays.sort(nums);

        System.out.println("Najmniejsza liczba=" +nums[0]);
        System.out.println("Najwieksza liczba=" +nums[nums.length-1]);
    }
}
