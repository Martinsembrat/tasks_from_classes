package sdaacademy.src;

import java.util.Scanner;

public class Pobieranie_cwiczenie {
    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);
        long value1;
        float value2;

        System.out.println("wartość stało-przecinkowa");
        value1 = input.nextLong(); //pobieranie z klawiatury

        System.out.println("podaj wartość zmienno-przecinkowa");
        value2 = input.nextFloat();

        System.out.println(value1);
        System.out.println(value2);
    }
}
