package sdaacademy.src.d2020_03_28;

import java.util.Scanner;

public class Zad4 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        float dochod;
        float podatek;

        System.out.println("podaj dochod");
        dochod = input.nextFloat();
        System.out.println("podatek:");
        if (dochod < 85528) {
            podatek = dochod*18/100-556.02f;
            System.out.println(podatek);
        } else if(dochod > 85528){
            podatek = 14839.02f + dochod*31/100f;
            System.out.println(podatek);
        }
    }
}
