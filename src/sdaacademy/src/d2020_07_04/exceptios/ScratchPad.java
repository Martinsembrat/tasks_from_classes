package sdaacademy.src.d2020_07_04.exceptios;

import java.util.Objects;

/*
public final class ScratchPad {
    public static void main(String... args) {
        //dzielenie(1000, 10)
        try {
            System.out.println("Przed dzieleniem");
            System.out.println(dzielenie(1000,0));
        } catch (ArithmeticException ex) {
            System.out.println("WYJĄTEK!!!!!");
            // ex.printStackTrace();
        } finally {
            System.out.println("Po dzieleniu");
        }
        System.out.println("Koniec!");
    }
    private static int bezpieczneDzielenie(int a, int b) {
        try {
            return a / b;
        } catch (ArithmeticException ex) {
            return 0;
        }
    }
    private static int dzielenie(Integer a, Integer b) {
        return a / b;
    }
    private static int dzielenie(int a, int b) {
        return a / b;
    }
}*/
public final class ScratchPad {
    public static void main(String... args) {
        //dzielenie(1000, 10)
        try {
            test();
        } catch (NullPointerException ex) {
            System.out.println("NPE!!!!!");
        }
        System.out.println("Koniec!");
    }
    private static void test() {
        try {
            System.out.println("Przed dzieleniem");
            System.out.println(dzielenie(1000, 0));
        } catch (DivisionByZero ex) {
            System.out.println("/ 0");
        } catch (ArithmeticException ex) {
            System.out.println("WYJĄTEK ARYTMETYCZNY!!!!!");
            // ex.printStackTrace();
        } finally {
            System.out.println("Po dzieleniu");
        }
    }
    private static int bezpieczneDzielenie(int a, int b) {
        try {
            return a / b;
        } catch (ArithmeticException ex) {
            return 0;
        }
    }
    // throws pojawia się tylko dla metod, które rzucają wyjątki dziedziczące z Exception, a nie z RuntimeException
    private static int dzielenie(Integer a, Integer b) throws DivisionByZero {
        Objects.requireNonNull(a);
        Objects.requireNonNull(b);
        return dzielenie(a.intValue(), b.intValue());
    }
    private static int dzielenie(int a, int b) throws DivisionByZero {
        if (b == 0) {
            throw new DivisionByZero(a, b);
        }
        return a / b;
    }
    private static double dzielenie(double a, double b) {
        if (b == 0) {
            return Double.NaN;
        }
        return a / b;
    }
}