package sdaacademy.src.d2020_07_04.exceptios.zad_koszyk;
/*
Stwórz klasę Basket, która imituje koszyk i przechowuje aktualną ilość elementów w koszyku. Dodaj metodę addToBasket(),
która dodaje element do koszyka (zwiększając aktualny stan o 1) oraz metodę removeFromBasket(),
która usuwa element z koszyka (zmniejszając aktualny stan o 1).Koszyk może przechowywać od 0 do 10 elementów.
W przypadku, kiedy użytkownik chce wykonać akcję usunięcia przy stanie 0 lub dodania przy stanie 10,
rzuć odpowiedni runtime exception (BasketFullExceptionlub BasketEmptyException).
*/

import java.util.ArrayList;
import java.util.List;

public class Basket {
    private static final int MAX_CAPACITY = 10;
    private List<String> products = new ArrayList<>();
    public int getCapacity() {
        return MAX_CAPACITY - products.size();
    }
    public void addToBasket(String product) {
        if(getCapacity() > 0) {
            products.add(product);
        } else {
            throw new BasketFullException(MAX_CAPACITY);
        }
    }
    public void removeFromBasket(String product) {
        if(!products.isEmpty()) {
            products.remove(product);
        } else {
            throw new BasketEmptyException();
        }
    }
    public static void main(String... args) {
        Basket basket = new Basket();
        basket.addToBasket("pomidor");
        basket.addToBasket("trampki");
        basket.addToBasket("rower");
        basket.addToBasket("pomidor");
        basket.addToBasket("trampki");
        basket.addToBasket("rower");
        basket.addToBasket("pomidor");
        basket.addToBasket("trampki");
        basket.addToBasket("rower");
        basket.addToBasket("pomidor");
        try {
            basket.addToBasket("trampki");
        } catch (BasketFullException ex) {
            System.out.println("Koszyk jest już pełny. Nie wiesz, że nie wolno dodawać więcej niż " + ex.getBasketCapacity());
        }
        basket.removeFromBasket("pomidor");
        basket.removeFromBasket("trampki");
        basket.removeFromBasket("rower");
        basket.removeFromBasket("pomidor");
        basket.removeFromBasket("trampki");
        basket.removeFromBasket("rower");
        basket.removeFromBasket("pomidor");
        basket.removeFromBasket("trampki");
        basket.removeFromBasket("rower");
        basket.removeFromBasket("pomidor");
        try {
            basket.removeFromBasket("samochód");
        } catch (BasketEmptyException ex) {
            System.out.println("Co ty robisz z tym pustym koszykiem!");
        }
    }
}
class BasketFullException extends RuntimeException {
    private final int basketCapacity;
    public BasketFullException(int basketCapacity) {
        super("basket full - capacity: " + basketCapacity);
        this.basketCapacity = basketCapacity;
    }
    public int getBasketCapacity() {
        return basketCapacity;
    }
}
class BasketEmptyException extends RuntimeException {
    public BasketEmptyException() {
        super("basket empty");
    }
}
