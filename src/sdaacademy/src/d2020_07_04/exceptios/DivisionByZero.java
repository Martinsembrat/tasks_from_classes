package sdaacademy.src.d2020_07_04.exceptios;

public class DivisionByZero extends RuntimeException {
    private double a;
    private double b;
    public DivisionByZero(double a, double b) {
        this.a = a;
        this.b = b;
    }
    public double getA() {
        return a;
    }
    public double getB() {
        return b;
    }
}