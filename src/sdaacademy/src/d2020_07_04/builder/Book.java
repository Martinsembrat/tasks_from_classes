package sdaacademy.src.d2020_07_04.builder;

public class Book {
    private String title;
    private String author;
    private String description;
    private int yearOfRelease;
    private BookType bookType;

    public Book(String title,
                String author,
                String description,
                int yearOfRelease,
                BookType bookType) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.yearOfRelease = yearOfRelease;
        this.bookType = bookType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public BookType getBookType() {
        return bookType;
    }

    public void setBookType(BookType bookType) {
        this.bookType = bookType;
    }
}

enum BookType {
    PAPER, EBOOK
}
