package sdaacademy.src.d2020_07_04.builder;

public class BuilderExample {

    public static void main(String[] args) {
        //old way
        PaperBookBuilder paperBookBuilder = new PaperBookBuilder();
        //p==paperBookBuilder
        PaperBookBuilder p = paperBookBuilder.setTitle("Harry Potter");
        //p1==paperBookBuilder
        PaperBookBuilder p1 = paperBookBuilder.setAuthor("J.K. Rowling");
        //p2==paperBookBuilder
        PaperBookBuilder p2 = paperBookBuilder.setDescription("Kamień filozoficzny");
        Book book = paperBookBuilder.build();

        System.out.println("1: " + paperBookBuilder);
        System.out.println("2: " + p);
        System.out.println("3: " + p1);
        System.out.println("3: " + p2);

        //new way
        PaperBookBuilder paperBookBuilder1 = new PaperBookBuilder();
        Book book1 = paperBookBuilder1
                .setTitle("Harry Potter")
                .setAuthor("J.K. Rowling")
                .setDescription("Komnata tajemnic")
                .build();

        //new way
        Book book2 = new PaperBookBuilder()
                .setTitle("Harry Potter")
                .setAuthor("J.K. Rowling")
                .setDescription("Komnata tajemnic")
                .build();
    }
}
