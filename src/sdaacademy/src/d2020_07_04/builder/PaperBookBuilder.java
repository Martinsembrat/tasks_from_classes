package sdaacademy.src.d2020_07_04.builder;

public class PaperBookBuilder {
    private String title;
    private String author;
    private String description;

    public PaperBookBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public PaperBookBuilder setAuthor(String author) {
        this.author = author;
        return this;
    }

    public PaperBookBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public Book build() {
        return new Book(title,
                author,
                description,
                (int) System.currentTimeMillis(),
                BookType.PAPER);
    }
}
