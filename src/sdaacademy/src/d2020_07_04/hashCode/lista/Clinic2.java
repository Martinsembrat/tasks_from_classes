package sdaacademy.src.d2020_07_04.hashCode.lista;

import java.util.LinkedList;
import java.util.Queue;

public class Clinic2 {
    private final Queue<Patient> patients = new LinkedList<>();
    public Patient registerPatient(String patientName) {
        Patient patient = new Patient(patientName, true);
        patients.add(patient);
        return patient;
    }
    public String handlePatient() {
        do {
            Patient patient = patients.poll();
            if (patient == null) {
                return null;
            }
            if (patient.stillThere) {
                return patient.name;
            }
        } while (true);
    }
    public static void main(String... args) {
        Clinic2 clinic = new Clinic2();
        Patient patientA = clinic.registerPatient("A");
        Patient patientB = clinic.registerPatient("B");
        Patient patientD = clinic.registerPatient("D");
        Patient patientC = clinic.registerPatient("C");
        System.out.println(clinic.handlePatient());
        patientD.stillThere=false;
        System.out.println(clinic.handlePatient());
        System.out.println(clinic.handlePatient());
        System.out.println(clinic.handlePatient());
        System.out.println(clinic.handlePatient());
    }
}
class Patient {
    String name;
    boolean stillThere;
    public Patient(String name, boolean stillThere) {
        this.name = name;
        this.stillThere = stillThere;
    }
}