package sdaacademy.src.d2020_07_04.hashCode.lista;
/*
1. Stwórz klasę Clinic, reprezentującą przychodnie.
2. Klasa, jako pole powinna zawierać kolejkę imion pacjentów.
3. W Clinic utwórz metodę registerPatient(String name), która doda pacjenta do kolejki.
4. Dodaj kolejną metodę String handlePatient(), która zwróci imię obsługiwanego pacjenta i
usunie go z kolejki.
5. Przetestuj przychodnie za pomocą testów jednostkowych.
6. *Rozszerz system o klasę Doctor zawierającą oprócz danych osobowych, historię wizyt.
Forma dowolna
*/
import java.util.LinkedList;
import java.util.Queue;

public class Clinic {
    private final Queue<String> patients = new LinkedList<>();
    public void registerPatient(String patientName) {
        patients.add(patientName);
    }
    public String handlePatient() {
        return patients.poll();
    }
    public static void main(String... args) {
        Clinic clinic = new Clinic();
        clinic.registerPatient("A");
        clinic.registerPatient("B");
        clinic.registerPatient("D");
        clinic.registerPatient("C");
        System.out.println(clinic.handlePatient());
        System.out.println(clinic.handlePatient());
        System.out.println(clinic.handlePatient());
        System.out.println(clinic.handlePatient());
        System.out.println(clinic.handlePatient());
    }
}