package sdaacademy.src.d2020_07_04.hashCode.lista;

/*1. W metodzie main utwórz listę liczb całkowitych.
  2. Wypełnij ją dwudziestoma wylosowanymi liczbami z zakresu <0, 10>.
  3. Wyświetl zawartość listy.
  4. Usuń z listy wszystkie liczby parzyste.
  5. Wyświetl zawartość listy.rozwiązanie powyżej*/
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
public class Lists {
    public static void main(String... args) {
        List<Integer> numbers = new ArrayList<>();
        for(int i = 0; i < 20; ++i) {
            int number = ThreadLocalRandom.current().nextInt(0, 11);
            numbers.add(number);
        }
        System.out.print("Przed filtrowaniem: ");
        for(int number: numbers) {
            System.out.print(number);
            System.out.print(" ");
        }
        System.out.println("");
        Iterator<Integer> numberIterator = numbers.iterator();
        while (numberIterator.hasNext()) {
            int number = numberIterator.next();
            if(number % 2 == 0) {
                numberIterator.remove();
            }
        }
        System.out.print("Po filtrowaniem: ");
        for(int number: numbers) {
            System.out.print(number);
            System.out.print(" ");
        }
        System.out.println("");
    }
}
