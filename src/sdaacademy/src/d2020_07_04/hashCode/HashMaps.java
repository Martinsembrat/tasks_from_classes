package sdaacademy.src.d2020_07_04.hashCode;

import java.util.Map;
import java.util.Objects;
import java.util.HashMap;

public class HashMaps {
    public static void main(String... args) {
        Student adam = new Student("Adam", "666");
        Student martin = new Student("Martin", "123");
        Student aga = new Student("Aga", "345"); // było 345
        Map<String, Student> students = new HashMap<String, Student>();
        students.put(adam.getIndex(), adam);
        students.put(martin.getIndex(), martin);
        students.put(aga.getIndex(), aga);
        Student student = students.get("667");
        if(student != null) {
            System.out.println(Objects.equals(student, adam) ? "Indeks Adama" : "To nie indeks Adama ");
        } else {
            System.out.println("Nie ma takiego studenta");
        }
        System.out.println(students.keySet());
    }
}
