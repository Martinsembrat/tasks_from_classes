package sdaacademy.src.d2020_07_04.hashCode;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Hashcode1 {
    public static void main(String... args) {
        Student adam = new Student("Adam", "666");
        Student martin = new Student("Martin", "123");
        Student aga = new Student("Aga", "123"); // było 345
        Set<Student> students = new HashSet<>();
        students.add(adam);
        students.add(martin);
        students.add(aga);
        System.out.println("Hashcode Adama: " + adam.hashCode());
        System.out.println("Hashcode Martina: " + martin.hashCode());
        System.out.println("Hashcode Agi: " + aga.hashCode());
        System.out.println("Adam to nie Aga: " + (adam == aga));
        System.out.println("Adam to nie Aga: " + (adam.equals(aga)));
        for(Student student: students) {
            System.out.println(student);
        }
    }
    private static boolean compare(Object a, Object b) {
        if (a.hashCode() == b.hashCode()) {
            return a.equals(b);
        } else {
            return false;
        }
    }
}
class Student {
    private final String name;
    private final String index;
    public Student(String name, String index) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(index);
        this.name = name;
        this.index = index;
    }
    public String getName() {
        return name;
    }
    public String getIndex() {
        return index;
    }
    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", index='" + index + '\'' +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(index, student.index) && Objects.equals(name, student.name);
        // return index.equals(student.index) && name.equals(student.name); gdyby index lub name był null to NPE
    }
    @Override
    public int hashCode() {
        return Objects.hash(index);
    }
}