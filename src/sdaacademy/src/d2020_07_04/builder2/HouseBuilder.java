package sdaacademy.src.d2020_07_04.builder2;


public class HouseBuilder {
    float size;
    int lvlquantity;
    String roofType;
    int rooms;
    int windows;
    Material material;

    public HouseBuilder setSize(float size) {
        this.size = size;
        return this;
    }

    public HouseBuilder setLvlquantity(int lvlquantity) {
        this.lvlquantity = lvlquantity;
        return this;
    }

    public HouseBuilder setRoofType(String roofType) {
        this.roofType = roofType;
        return this;
    }

    public HouseBuilder setRooms(int rooms) {
        this.rooms = rooms;
        return this;
    }

    public HouseBuilder setWindows(int windows) {
        this.windows = windows;
        return this;
    }


    public House build() {
        return new House (size,
                lvlquantity,
                roofType,
                rooms,
                windows,
                Material.BRICK);
    }
}
