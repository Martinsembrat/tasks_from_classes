package sdaacademy.src.d2020_07_04.builder2;

public class House {

    float size;
    int lvlquantity;
    String roofType;
    int rooms;
    int windows;
    Material material;

    @Override
    public String toString() {
        return "House{" +
                "size=" + size +
                ", lvlquantity=" + lvlquantity +
                ", roofType='" + roofType + '\'' +
                ", rooms=" + rooms +
                ", windows=" + windows +
                ", material=" + material +
                '}';
    }

    public House(float size, int lvlquantity, String roofType, int rooms, int windows, Material material) {
        this.size = size;
        this.lvlquantity = lvlquantity;
        this.roofType = roofType;
        this.rooms = rooms;
        this.windows = windows;
        this.material = material;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public int getLvlquantity() {
        return lvlquantity;
    }

    public void setLvlquantity(int lvlquantity) {
        this.lvlquantity = lvlquantity;
    }

    public String getRoofType() {
        return roofType;
    }

    public void setRoofType(String roofType) {
        this.roofType = roofType;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public int getWindows() {
        return windows;
    }

    public void setWindows(int windows) {
        this.windows = windows;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }
}
enum Material {
    WOOD, BRICK
}