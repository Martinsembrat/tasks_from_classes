package sdaacademy.src.d2020_07_04.builder2;



public class HouseBuilderExample {

    public static void main(String[] args){
        HouseBuilder brickHouseBuilder = new HouseBuilder();

        House house1 = brickHouseBuilder
                .setSize(23.22f)
                .setLvlquantity(6)
                .setRoofType("flat1")
                .setRooms(5)
                .setWindows(29)
                .build();
        System.out.println(house1);
    }
}
