package sdaacademy.src.d2020_07_04.builder3.template_method;

public class DeveloperComputerOffer extends ComputerOffer {
    @Override
    Processor setUpProcessor() {
        return Processor.I9;
    }

    @Override
    String setUpMotherboard() {
        return "integreated graphic card";
    }

    @Override
    int provideRam() {
        return 32;
    }

    @Override
    boolean hasSsd() {
        return true;
    }

    @Override
    int setUpDiskSize() {
        return 512;
    }

    @Override
    GraphicCard setUpGraphicCard() {
        return GraphicCard.RADEON;
    }

    @Override
    String setUpOperationSystem() {
        return "Unix";
    }
}
