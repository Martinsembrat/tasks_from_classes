package sdaacademy.src.d2020_07_04.builder3.template_method;

public class ComputerShop {

    public static void main(String[] args) {
        ComputerOffer computerOffer = new DeveloperComputerOffer();
        ComputerOffer computerOffer1 = new GamingComputerOffer();

        Computer devComputer = computerOffer.createComputer();
        Computer gameComputer = computerOffer1.createComputer();

        System.out.println(devComputer);
        System.out.println(gameComputer);
    }
}
