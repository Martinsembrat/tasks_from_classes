package sdaacademy.src.d2020_07_04.builder3.template_method;

/*
        this.processor = processor;
        this.motherboard = motherboard;
        this.ram = ram;
        this.isSSD = isSSD;
        this.diskSpace = diskSpace;
        this.graphicCard = graphicCard;
        this.operationSystem = operationSystem;
 */
public abstract class ComputerOffer {

    public final Computer createComputer() {
        Processor processor = setUpProcessor();
        String motherboard = setUpMotherboard();
        int ram = provideRam();
        boolean ssd = hasSsd();
        int diskSize = setUpDiskSize();
        GraphicCard graphicCard = setUpGraphicCard();
        String os = setUpOperationSystem();
        return new Computer(processor,
                motherboard,
                ram,
                ssd,
                diskSize,
                graphicCard,
                os);
    }

    abstract Processor setUpProcessor();
    abstract String setUpMotherboard();
    abstract int provideRam();
    abstract boolean hasSsd();
    abstract int setUpDiskSize();
    abstract GraphicCard setUpGraphicCard();
    abstract String setUpOperationSystem();


}
