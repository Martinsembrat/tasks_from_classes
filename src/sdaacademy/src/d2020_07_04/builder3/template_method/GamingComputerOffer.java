package sdaacademy.src.d2020_07_04.builder3.template_method;

public class GamingComputerOffer extends ComputerOffer {
    @Override
    Processor setUpProcessor() {
        return Processor.I9;
    }

    @Override
    String setUpMotherboard() {
        return "standard";
    }

    @Override
    int provideRam() {
        return 32;
    }

    @Override
    boolean hasSsd() {
        return true;
    }

    @Override
    int setUpDiskSize() {
        return 2000;
    }

    @Override
    GraphicCard setUpGraphicCard() {
        return GraphicCard.NVIDIA;
    }

    @Override
    String setUpOperationSystem() {
        return "Windows";
    }
}
