package sdaacademy.src.d2020_07_04.builder3.template_method;

/*
Napisz aplikację realizującą metodę szablonową dla przykładu składania komputera w sklepie komputerowym. Każdy komputer powinien mieć:
- procesor
- płytę główną
- pamięć ram
- dysk, czy SSD czy HDD
- pamięć dyskową
- karta graficzna
- system operacyjny
Metoda szablonowa powinna tworzyć komputer i zwracać go jako parametr zwracany.
public class Computer {
		processor;
		motherboard;
		ram;
		isSSD;
		diskSpace;
		graphicCard
		operationSystem;
}
Oferta powinna uwzględniać:
- komputer dla gracza
- komputer dla developera
- komputer do pracy biurowej
1. Klasa modelowa Computer
2. Klasa abstrakcyjna ComputerOffer (z metodą szablonową Computer createComputer())
Abstract String setUpProcess();
Abstract int setUpRamMemory();

Final Computer createComputer() {
	String processor = setUpProcessor();
	Int ramMemory = setUpRamMemory();
	Return Computer(…, processor, …, ramMemory, …);
}
3. GamingComputerOffer extends ComputerOffer
4. DeveloperComputerOffer extends ComputerOffer
5. StandardComputerOffer extends ComputerOffer

 */
public class Computer {
    private Processor processor;
    private String motherboard;
    private int ram;
    private boolean isSSD;
    private int diskSpace;
    private GraphicCard graphicCard;
    private String operationSystem;

    public Computer(Processor processor, String motherboard, int ram, boolean isSSD, int diskSpace, GraphicCard graphicCard, String operationSystem) {
        this.processor = processor;
        this.motherboard = motherboard;
        this.ram = ram;
        this.isSSD = isSSD;
        this.diskSpace = diskSpace;
        this.graphicCard = graphicCard;
        this.operationSystem = operationSystem;
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public String getMotherboard() {
        return motherboard;
    }

    public void setMotherboard(String motherboard) {
        this.motherboard = motherboard;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public boolean isSSD() {
        return isSSD;
    }

    public void setSSD(boolean SSD) {
        isSSD = SSD;
    }

    public int getDiskSpace() {
        return diskSpace;
    }

    public void setDiskSpace(int diskSpace) {
        this.diskSpace = diskSpace;
    }

    public GraphicCard getGraphicCard() {
        return graphicCard;
    }

    public void setGraphicCard(GraphicCard graphicCard) {
        this.graphicCard = graphicCard;
    }

    public String getOperationSystem() {
        return operationSystem;
    }

    public void setOperationSystem(String operationSystem) {
        this.operationSystem = operationSystem;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "processor=" + processor +
                ", motherboard='" + motherboard + '\'' +
                ", ram=" + ram +
                ", isSSD=" + isSSD +
                ", diskSpace=" + diskSpace +
                ", graphicCard=" + graphicCard +
                ", operationSystem='" + operationSystem + '\'' +
                '}';
    }
}

enum Processor {
    I5, I7, I9
}

enum GraphicCard {
    RADEON, NVIDIA
}