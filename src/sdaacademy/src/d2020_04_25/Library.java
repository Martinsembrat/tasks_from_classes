package sdaacademy.src.d2020_04_25;

public class Library {

    public static void main(String[] args) {
        //deklaracja obiektu - deklarujemy, że taka zmienna będzie wykorzystywany w programie
        Book harryPotter;//ten obiekt jest póki co nullem
        //inicjalizacja obiektu - stworzenie konkretnego obiektu i przypisanie go do zmiennej
        //operator new jest odpowiedzialny za to by tworzyć obiekt
        harryPotter = new Book();
        //harryPotter.cover = 'T';//niedozwolne
        harryPotter.setCover('T');
        harryPotter.numberOfPages = 100;
        harryPotter.yearOfRelease = 2000;


        harryPotter.showBook();
        System.out.println("Harry potter");
        System.out.println(harryPotter.getCover());
        System.out.println(harryPotter.numberOfPages);
        System.out.println(harryPotter.yearOfRelease);
        harryPotter.title = new char[10];
        harryPotter.title[0] = 'H';
        harryPotter.title[1] = 'A';
        //...
        System.out.println(harryPotter.title);
    }
}
