package sdaacademy.src.d2020_04_25;

public class Book {

    private char cover;//brak dostępu do pola z poziomu innych klas
    int yearOfRelease;//pole jest widoczne w ramach innych klas ale tylko w ramach danego katalogu -> pakietu
    public short numberOfPages;//ogólny dostęp do zmiennej
    char[] title;//title jest tylko zdeklarowany więc jest nullem

    //tzw metoda setter
    //cover może być F, T
    public void setCover(char coverFromUser) {
        //dodatkowe operacje, np. walidowanie pola
        if (coverFromUser == 'F' || coverFromUser == 'T'){
            //ustawiamy to co wskazał użytkownik jeśli zostały spełnione wymagania
            cover = coverFromUser;
        } else {
            //ustawiamy wartość domyślną w przypadku nie spełniania wymagać
            cover = 'U';//unknown
        }
    }
    //tzw metoda getter
    public char getCover() {
        return cover;
    }

    public void showBook() {
        System.out.println("Szczegóły książki: ");
        System.out.print("Cover: ");
        System.out.println(cover);
        System.out.println("Year of release: ");
        System.out.println(yearOfRelease);
    }
}
