package sdaacademy.src.d2020_04_25;

public class Cinema {

    public static void main(String[] args) {
        Price price = new Price();
        price.value = 124;

        //deklaracja obiektu
        Movie starWars;
        //inicjalizacja
        starWars = new Movie();
        starWars.genre = 'A'; //dla obiektu star wars :ustawiamy konkretną wartość dla pola genre w ramach klasy Movie: film akcji
        starWars.length = 100;//dla obiektu star wars: ustawiamy konkretną wartość dla pola długość w ramach klasy Movie długość: 100 min
        starWars.price = price;
        //starWars.value //brak dostęp do pola value, gdyż jest ono częścią klasy price
        starWars.price.value = 34; //odwołanie do pola value z klasy Price poprzez obiekt price z klasy Movie
        //starWars.yearOfRelease = (byte) 1999; -> pole yearOfRelease i nie mamy do niego dostępu

        Movie avengers = new Movie();
        avengers.genre = 'C'; //dla obiektu avengers: ustawiamy konkretną wartość dla pola genre w ramach klasy Movie: film na podstawie komiksu
        avengers.length = 158;//dla obiektu avengers: ustawiamy konkretną wartość dla pola długość w ramach klasy Movie długość: 120 min

        System.out.println("Obiekt star wars: ");
        System.out.println(starWars.genre);
        System.out.println(starWars.length);

        System.out.println("Obiekt avengers: ");
        System.out.println(avengers.genre);
        System.out.println(avengers.length);

    }
}
