package sdaacademy.src.d2020_04_25;

public class Calculator {

    public int add(int number1, int number2) {
        int sum = number1 + number2;
        //w celu zwrócenia konkretnej wartości z metody używamy słowa kluczowego return
        return sum;
    }


    public int addArray(int[] tab) {
        int sum = 0;
        for (int i=0;i<tab.length;i++) {
            sum+=tab[i];//sum = sum + tab[i];
        }
        return sum;
    }

    public void showCalculatorInfo() {
        System.out.println(
                "To jest kalkualtor użyty na potrzeby szkolenia SDA");
    }
}
