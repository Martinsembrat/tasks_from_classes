package sdaacademy.src.d2020_04_25;

public class MathPlayground {

    public static void main(String[] args) {
        //deklaracja i inicjalizacja obiektu calculator
        Calculator calculator = new Calculator();
        //wywołanie metody add wraz z przypisaniem
        // rezultatu do zmiennej
        int sumFromAddMethod = calculator.add(10, 20);
        int sumFromAddMethodV1 = calculator.add(40, 60);
        int[] array = {1, 3, 5, 6, 10};
        int sumArray = calculator.addArray(array);
        calculator.showCalculatorInfo();
        System.out.println("Wynik dodawania: ");
        System.out.println(sumFromAddMethod);
        System.out.println(sumFromAddMethodV1);

        System.out.println("Wynik dodawania tablicy: ");
        System.out.println(sumArray);
    }
}
