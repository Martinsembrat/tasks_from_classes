package sdaacademy.src.d2020_06_06.strings;

public class StringTask {

    public static void main(String[] args) {
        //Skonwertuj daną linię tekstu na 4 zmienne:
        //name, lastName, salary, city
        //usunąć białe znaki
        //wyświetlić rezultat konwersji
        String[] lines = new String[] {
                "Wacław,  Nowak,5500,Szczecin",
                " Jan,Kowalski,4333,Warszawa  ",
                "  Stanisław  ,Czet,5544,   Kraków  "
        };
        //alternatywna forma
        for (String line : lines) {
            //dzielenie poszczególnej lini pliku na tablicę,
            // każdy element oddzielony przecinkiem stanowi oddzielny element tablicy
            //element indeks 0 = name
            //element indeks 1 = lastName
            //element indeks 2 = salary
            //element indeks 3 = city
            String[] data = line.split(",");
            //usuwanie białych znaków z początku i końca obiektu
            String name = data[0].trim();
            //usuwanie białych znaków z początku i końca obiektu
            String lastName = data[1].trim();
            //konwersja obiektu String na float
            float salary = Float.parseFloat(data[2].trim());
            //usuwanie białych znaków z początku i końca obiektu
            String city = data[3].trim();
            String text = String.format("Converted csv line -> Name: %s, Last Name: %s, Salary: %.2f, City: %s", name, lastName, salary, city);
            System.out.println(text);
        }
        for (int i=0; i<lines.length; i++) {
            String line = lines[i];
            String[] data = line.split(",");
            String name = data[0].trim();
            String lastName = data[1].trim();
            float salary = Float.parseFloat(data[2].trim());
            String city = data[3].trim();
            String text = String.format("Converted csv line -> Name: %s, Last Name: %s, Salary: %.2f, City: %s", name, lastName, salary, city);
            System.out.println(text);
        }
    }
}
