package sdaacademy.src.d2020_06_06.strings;

public class StringExample {

    public static void main(String[] args) {
        char[] greetingsChar = {'H', 'e', 'l', 'l', 'o', '!'};
        String greetings = "Hello!";
        String greetingsV2 = new String(greetingsChar);
        int greetingsSize = greetings.length();
        System.out.println(greetingsSize);
        char letter = greetings.charAt(2);
        System.out.println(letter);
        String result = greetings.replaceAll("ll", "xx");
        greetings = greetings + "Test";//stworzonie nowego obiektu typu String: Hello!Test
        greetings = greetings + "v1";//stworzonie nowego obiektu typu String: Hello!Testv1
        greetings = greetings + "v2";//stworzonie nowego obiektu typu String: Hello!Testv1v2
        greetings = greetings + "v3";//stworzonie nowego obiektu typu String: Hello!Testv1v2v3

        System.out.println(greetings);

        String test1 = "Test1";
        String test2 = "Test1";

        //test1 == test2 //porównywanie referencji (adresów w pamięci)
        //test.equals(test) //porównywanie wartości
        if (test1.equals(test2)) {
            System.out.println("Obiekty są równe!");
        } else {
            System.out.println("Obiekty nie są równe!");
        }

        String test = "";
        for (int i=0;i<10000;i++) {
            test = test + "x";
        }
        System.out.println(test);

        StringBuilder stringBuilder = new StringBuilder();
        for (int i=0;i<10000;i++) {
            stringBuilder.append("x");
        }
        System.out.println(stringBuilder);
    }
}
