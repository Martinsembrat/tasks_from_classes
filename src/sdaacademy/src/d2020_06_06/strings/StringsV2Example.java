package sdaacademy.src.d2020_06_06.strings;

public class StringsV2Example {

    public static void main(String[] args) {
        int counter = 10;
        float activation = 20.0f;
        String name = "Wacław";

        //The counter value is equal `10` activation property is equal `20.0f` for the user `Wacław`
        String text = "The counter value is equal "
                + counter + " activation property is equal "
                + activation + " for the user "
                + name;

        //%d -> int
        //%f -> float
        //%s -> string
        String messagePattern = "The counter value is equal %d activation property is equal %f for the user %s";
        String test = String.format(messagePattern,
                counter, activation, name);
        System.out.println(text);

        String csvLine = "Wacław,Nowak,34,Java";
        String[] data = csvLine.split(",");
        String dataName = data[0];//Wacław
        String dataLastName = data[1];//Nowak
        //string to float = Float.parseFloat
        //string to double = Double.parseDouble
        //string to integer = Integer.parseInt
        int dataAge = Integer.parseInt(data[2]);//34
        String dataSpecialization = data[3];
        System.out.println(dataName);
        System.out.println(dataLastName);
        System.out.println(dataAge);
        System.out.println(dataSpecialization);
    }
}
