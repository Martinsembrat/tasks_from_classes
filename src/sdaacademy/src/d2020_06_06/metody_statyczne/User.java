package sdaacademy.src.d2020_06_06.metody_statyczne;

/*
Zaimplementuj klasę user która będzie zawierała pola takie jak:
- imię
- nazwisko
- wiek
- pesel
- obywatelstwo: może być "PL" bądź "NO_PL"
- dochody
- czy był karany
Zaproponuj które z powyższych pól może być immutable
Dodatkowo zaimplementuj metodę statyczną showUser która jako argument metody statycznej przyjmie obiekt klasy User i
wyświetli informację na jego temat
 */
public class User {
    private final long id;
    String name;
    String surname;
    int age;
    String nationality;
    int income;
    boolean criminal;

    public User(String name, String surname, int age, int id, String nationality, int income, boolean criminal) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.nationality= nationality;
        this.income = income;
        this.criminal = criminal;
        this.id = id;
    }
    public static void showuser(User user) {
        String message = String.format("User: %s %s %d %s %d %s %d",
                user.name, user.surname, user.age, user.nationality, user.income, user.criminal, user.id);
        System.out.println(message);
    }
    public static void main(String[] args){
        User user = new User("wacław","x",1,3, "pl",3,false);
        User.showuser(user);

    }
}
