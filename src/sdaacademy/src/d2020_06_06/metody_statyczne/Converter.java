package sdaacademy.src.d2020_06_06.metody_statyczne;

public class Converter {

    public String converterType;

    public Converter(String converterType) {
        this.converterType = converterType;
    }

    public void convert() {
        System.out.println("Convert using: " + converterType);
    }

    public static void convertStatic() {
        System.out.println("Static converter");
    }
}
