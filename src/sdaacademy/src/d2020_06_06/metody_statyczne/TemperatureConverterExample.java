package sdaacademy.src.d2020_06_06.metody_statyczne;

public class TemperatureConverterExample {

    public static void main(String[] args) {
        float tempC = TemperatureConverter.convertKToC(290);
        System.out.println(tempC);

        //nie można nadpisać zmiennej finalnej!
        //TemperatureConverter.KELVIN_CONVERSION_FACTOR = -100;

        float tempK = TemperatureConverter.convertCToK(tempC);
        System.out.println(tempK);
    }
}
