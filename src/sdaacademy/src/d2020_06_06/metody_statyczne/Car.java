package sdaacademy.src.d2020_06_06.metody_statyczne;

public class Car {

    //pola finalnego mogą być inicjalizowane tylko i wyłącznie w konstruktorze, bądź w momencie deklracji!
    private final String model;//immutable -> zmienna niemodyfikowalna (w javie final),
    // jej wartość możemy utawić podczas deklaracji bądź też w konstrukotrze
    private float currentSpeed;//mutable -> zmienna modyfikowalna, jej wartość może zostać zmodyfikowana w dowolnym momencie
    // poprzez metody bądź też bezpośredni dostęp do pola klasy

    public Car(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public void setCurrentSpeed(float currentSpeed) {
        this.currentSpeed = currentSpeed;
    }
}
