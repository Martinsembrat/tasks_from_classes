package sdaacademy.src.d2020_06_06.metody_statyczne;

public class StaticExample {

    public static void main(String[] args) {
        Counter counterObj = new Counter();
        System.out.println("Counter field: " + counterObj.getCounter());
        System.out.println("Static counter: " + Counter.getStaticCounter());
        counterObj.incrementCounter();
        Counter.incrementStaticCounter();
        System.out.println("Counter field: " + counterObj.getCounter());
        System.out.println("Static counter: " + Counter.getStaticCounter());

        Counter counterObj1 = new Counter();
        System.out.println("Counter1 field: " + counterObj1.getCounter());
        System.out.println("Static counter: " + Counter.getStaticCounter());
    }
}
