package sdaacademy.src.d2020_06_06.dziedziczenie;

public class Student extends Person {
    private int yearOfStudies;
    private String typeOfStudies;
    private String id;

    public Student(String name, String lastName,
                   int yearOfStudies, String typeOfStudies,
                   String id) {
        //wywoływanie konstruktora klasy nadrzędnej: Person(name, lastName)
        super(name, lastName);
        this.id = id;
        this.yearOfStudies = yearOfStudies;
        this.typeOfStudies = typeOfStudies;
    }

    @Override
    public void showPerson() {
        super.showPerson();
        System.out.println("#########################");
        //zarówno odwołanie name, this.name, super.name
        // odwołuje się do tego samego pola z klasy Person
        System.out.println("Name: " + name);
        System.out.println("Name (this): " + this.name);
        System.out.println("Name (super): " + super.name);
        System.out.println("Year of studies: " + yearOfStudies);
        System.out.println("Type of studies: " + typeOfStudies);
        System.out.println("Id: " + id);
        System.out.println("#########################");
    }

    @Override
    public String toString() {
        //wywołanie metody toString z klasy nadrzędnej -> klasy Person
        String superToString = super.toString();
        return String.format("Parent toString: *%s*, Student: %d, %s, %s",
                superToString, yearOfStudies, typeOfStudies, id);
    }

    public static void main(String[] args) {
        Student student = new Student("Wacław", "Nowak",
                2, "IT", "3232");
        System.out.println(student);
        student.showPerson();
    }
}
