package sdaacademy.src.d2020_06_06.objects;

public class Book {
    private String title;
    private String author;
    private String genre;
    private int yearOfRelease;

    //konstruktor 4 argumentowy
    public Book(String title, String author, String genre, int yearOfRelease) {
        //this mówi: odwołaj się do pola instancji obiektu o nazwie title i przypisz mu wartość arguemntu konstruktora
        if (title == null || title.isEmpty()) {
            title = "unknown";
        }
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.yearOfRelease = yearOfRelease;
    }

    //konstruktor 1 argumentowy
    public Book(String title) {
        this.title = title;
        //ustawiłem wartości domyślne z punktu widzenia developera
        //własny zestaw wartości które mają określać wartości domyślne
        this.author = "not defined";
        this.genre = "not defined";
        this.yearOfRelease = -1;
    }

    //program nie wie z które konstruktora należy skorzystać: konstruktor z title i author
//    public Book(String author) {
//        this.author = author;
//    }

    public Book(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    //konstruktor 2 argumentowy
    public Book(String title, String author) {
        this.title = title;
        this.author = author;
        //pole genre i yearOfRelease będą miały ustawioną wartość domyślną dla danej zmiennej: dla obiektu -> null, dla integer ->0
    }

    public String getTitle() {
        return title;
    }

    public void displayBook() {
        String message = String.format("Book title: %s, book author: %s, book genre: %s, book year of release: %d",
                title, author, genre, yearOfRelease);
        System.out.println(message);
    }

    public boolean equals(Book bookToCompare) {
        if (this.title.equals(bookToCompare.title)
                && this.author.equals(bookToCompare.author)
        && this.genre.equals(bookToCompare.genre)
                && this.yearOfRelease == bookToCompare.yearOfRelease) {
            return true;
        }
        return false;
    }

    public String toString() {
        return String.format("Book title: %s, book author: %s, book genre: %s, book year of release: %d",
                title, author, genre, yearOfRelease);
    }
}
