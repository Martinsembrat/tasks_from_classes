package sdaacademy.src.d2020_06_06.objects;

/*
Napisz klasę która będzie odpowiedzialna za konwersję tablicy lini csv na tablicę obiektów movie.
Wygeneruj odpowiednią tablicę String i na jej podstawie dokonaj konwersji

* napisz metodę która umożliwi wyszukanie filmu o konkretnym tytule w tablicy
* napisz metodę która zwróci wszystkie filmy z kategori akcja
* napisz metodę odpowiedzialną za prezentację wszystkich filmów w tablicy
 */
public class Cinema {

    public static void main(String[] args) {
        Movie movie = new Movie("Star Wars", "Abrams", "Action", 2015, 4, 5);
        System.out.println(movie);
        movie.setNumberOfPrice(5);
        movie.setBoxOfficeRating(3);
        movie.displayMovie();

        Movie movie1 = new Movie("Star Wars", "Abrams", "Action", 2014, 0, 100);

        //sprawdzenie czy tytuł, reżyser i rok wydania filmu są jednakowe
        if (movie.equals(movie1)) {
            System.out.println("Filmy są równe");
        }

        String[] moviesCSV = new String[] {
                "Star Wars,J.J Ambrams,Action,2015,4,5",
                "Harry Potter,unknown,Fantasy,2001,3,10",
                "Titanic,Cameron,History,1999,4,2"
        };
        //tworzenie obiektu MovieConverter
        MovieConverter movieConverter = new MovieConverter();
        //konwersja tablicy String na tablicę Movie
        Movie[] movies = movieConverter.convertToMovieArray(moviesCSV);
        System.out.println("Movie Converter: ");
        //iteracja po skonwertowanych filmach
        for (Movie m : movies) {
            System.out.println(m);
        }
    }
}
