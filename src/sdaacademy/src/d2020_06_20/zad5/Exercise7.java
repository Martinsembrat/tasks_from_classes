package sdaacademy.src.d2020_06_20.zad5;

/*        ## Zadanie 7
    Zaimplementuj interfejs `GeometricObject`, który będzie
    zawierać definicję wspólnych zachowań dla klas pochodnych:
            * `double getPerimeter()`
            * `double getArea()`
            ### Klasa Circle
    Klasa `Circle` powinna implementować interfejs
`GeometricObject`, a ponadto zawierać pole: promień. Metody
    interfejsu `GeometricObject` powinny zostać zaimplementowane
    zgodnie z definicjami metematycznymi.
            ### Interfejs Resizable
    Interfejs `Resizable` powinien deklarować metodę `resize(int
    percent)`, która ma być odpowiedzialna za przeskalowanie
    obiektów implementujących tworzony interfejs.
            ### Klasa ResizableCircle
    Klasa `ResizableCircle` powinna implementować interfejs
`Resizable`. Metoda `resize` interfejsu powinna zmniejszać
    procentowo promień koła.
    Zaprezentuj zaimplementowane powyżej rozwiązanie na
    przykładzie.
```java*/
    public class Exercise7 {
        public static void main(String[] args) {
            CircleGeometricObject circleGeometricObject = new CircleGeometricObject(14);
            System.out.println(circleGeometricObject.getArea());
            System.out.println(circleGeometricObject.getPerimeter());
            System.out.println(circleGeometricObject); //dlaczego
            System.out.println(circleGeometricObject.toString());
            Resizable resizable = new ResizableCircle(40);
            resizable.resize(30);
            System.out.println(resizable);
        }
    }
