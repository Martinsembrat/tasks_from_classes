package sdaacademy.src.d2020_06_20.zad5.polimorfizm;

public interface Shape {

    double getArea();
    double getPeritmetr();

}
