package sdaacademy.src.d2020_06_20.zad5.polimorfizm;

/*Napisz interfejs Shape, który będzie realizować poniższe metody:
        - getArea()
        - getPerimeter()
        Na podstawie interfejsu shape zaimplementuj klasy szczegółowe:
        - Rectangle z polami: szerokość, wysokość
        - Cicrlce z polami: promień
        - Triangle z polami: wysokość i długośicą boku (zakładamy, że trójkąt jest równoboczny)
        Zaimplementuj metodę statyczną która na podstawie typu Shape obliczy i pole o obwód każdego z obiektu*/

class Rectangle implements Shape {
    float w;
    float h;
    public Rectangle(float wide, float hight ){
        this.w= wide;
        this.h =hight;
    }
    @Override
    public double getArea() { return w*h; }
    @Override
    public double getPeritmetr() { return 2*w+2*h; }
}
class Circle implements Shape {
    float r;

    public Circle(float radius) {
        this.r = radius;
    }

    @Override
    public double getArea() {
        return Math.PI * r * r;
    }

    @Override
    public double getPeritmetr() {
        return 2 * Math.PI * r;
    }

    public static void main(String[] args) {
        Shape circle2 = new Circle(34);
        System.out.println(circle2.getPeritmetr());


            Shape circle = new Circle(34);
            Shape rectangle = new Rectangle(10, 20);
            calculate(circle);
            calculate(rectangle);
        }
        public static void calculate(Shape shape) {
            System.out.println(shape.getArea());
            System.out.println(shape.getPeritmetr());
        }
    }