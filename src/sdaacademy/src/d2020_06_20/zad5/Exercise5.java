package sdaacademy.src.d2020_06_20.zad5;

 /*   Zaimplementuj klasę `Line`, która będzie zawierać (na zasadzie
kompozycji) instancję dwóch obiektów `Point2D`
    Klasa `Point2D` powinna zawierać:
            * dwa pola typu float: `x`, `y`
            * konstruktor bezparametrowy ustawiający wartość pól `x` i `y` na `0`
            * konstruktor z dwoma parametrami: `float x`, `float y`
            * metody typu getter odpowiedzialne za zwracanie wartości zmiennej: `x`, `y`
            * metodę getXY zwracającą współrzędne x i y w postaci tablicy dwuelementowej
            * metody typu setter odpowiedzialne za ustawianie wartości pól `x`, `y`
            * metodę setXY ustawiającą współrzędne `x` i `y`
            * metoda `toString` powinna zwracać łańcuch tekstowy o następującym formacie: `(x, y)`;.
            Punkty te będą punktem początkowym oraz końcowym odcinka.
Ponadto klasa ta powinna implementować:
* konstruktor przyjmujący dwa punkty: początkowy i końcowy
* konstruktor przyjmujący 4 parametry: współrzędne punktu początkowego oraz końcowego
* metody typu `getter` odpowiedzialne za zwracanie punktów: początkowego i końcowego
* metody typu `setter` odpowiedzialne za ustalanie punktów: początkowego i końcowego
* metodę odpowiedzialną za obliczanie długości linii na podstawie ustawionych punktów
* metodę odpowiedzialną za zwracanie współrzędnych punktu będącego środkiem stworzonej prostej
    Zaprezentuj zaimplementowane powyżej rozwiązanie na przykładzie.*/

    public class Exercise5 {
        public static void main(String[] args) {
            Line line = new Line(10, 20, 30, 40);
            System.out.println(line.getLength());
            System.out.println(line.getMiddlePoint());
            Point2DExt Point2DExt1 = new Point2DExt(77,99);
            Point2DExt1.setY(5);
            System.out.println(Point2DExt1.getY());//zmienic sobie
        }
    }
    class Line {
        private Point2DExt p1, p2;
        public Line(Point2DExt p1, Point2DExt p2) {
            this.p1 = p1;
            this.p2 = p2;
        }
        public Line(float p1Start, float p1End, float p2Start,
                    float p2End) {
            this.p1 = new Point2DExt(p1Start, p1End);
            this.p2 = new Point2DExt(p2Start, p2End);
        }
        public Point2DExt getP1() {
            return p1;
        }
        public void setP1(Point2DExt p1) {
            this.p1 = p1;
        }
        public Point2DExt getP2() {
            return p2;
        }
        public void setP2(Point2DExt p2) {
            this.p2 = p2;
        }
        public float getLength() {
            return (float) Math.sqrt((Math.pow(p2.x - p1.x, 2) +
                    Math.pow(p2.y - p1.y, 2)));
        }
        public Point2DExt getMiddlePoint() {
            float xMiddle = (p1.x + p2.x) / 2;
            float yMiddle = (p1.y + p2.y) / 2;
            return new Point2DExt(xMiddle, yMiddle);
        }
    }
    class Point2DExt {
        protected float x, y;
        public Point2DExt() {
        }
        public Point2DExt(float x, float y) { //dlaczego nie ma void
            this.x = x;
            this.y = y;
        }
        public float getX() {
            return x;
        }   // dlaczego nie static
        public void setX(float x) { this.x = x; } // dlaczego ma void
        public float getY() {
            return y;
        }
        public void setY(float y) {
            this.y = y;
        }
        public float[] getXY() {
            return new float[]{x, y};
        }
        public void setXY(float x, float y) {  // ma void - po co to skoro mamy kontruktor
            this.x = x;
            this.y = y;
        }
        @Override
        public String toString() {
            return String.format("(%f,%f)", x, y);
        }
    }


