package sdaacademy.src.d2020_06_20.zad5;

interface GeometricObject {
    double getPerimeter();
    double getArea();
}
interface Resizable {
    void resize(int percent);
}
class CircleGeometricObject implements GeometricObject {
    protected float radius;
    public CircleGeometricObject(float radius) {
        this.radius = radius;
    }
    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }
    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }
    @Override
    public String toString() {
        return "CircleGeometricObject{" +
                "radius=" + radius +
                '}';
    }
}
class ResizableCircle extends CircleGeometricObject implements
        Resizable {
    public ResizableCircle(float radius) {
        super(radius);
    }
    @Override
    public void resize(int percent) {
        radius = radius * percent / 100;
    }
    @Override
    public String toString() {
        return "ResizableCircle{" +
                "radius=" + radius +
                '}';
    }
}
