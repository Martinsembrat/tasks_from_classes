package sdaacademy.src.d2020_06_20;

/*Zaimplementuj klasę abstrakcyjną `Movable`, który będzie zawierać definicję wspólnych zachowań dla klas `MovablePoint` i *`MovableCircle`. Będą to metody:
* `void moveUp()`
* `void moveDown()`
* `void moveLeft()`
* `void moveRigth()`
### Klasa MovablePoint
Klasa `MovablePoint` powinna rozszerzać klasę abstrakcyjną `Movable`, a ponadto powinna zawierać 4 pola typu `int`: `x`, `y`, `xSpeed`, `ySpeed`. Pola `x`, `y` powinny definiować współrzędne punktu, natomiast pola `xSpeed`, `ySpeed` powinny określać o ile powinny zmieniać się odpowiednie współrzędne.
* metody `moveUp()` oraz `moveDown()` powinny każdorazowo zwiększać/zmniejszać wartość współrzędnej `y` o wskazaną wartość: `ySpeed`
* metody `moveLeft` oraz `moveRight()` powinny każdorazowo zwiększać/zmniejszać wartość współrzędnej `x` o wskazaną wartość `xSpeed` (edited)  */

abstract public class Movable {

    public abstract int moveUp();
    public abstract int moveDown();
    public abstract int moveLeft();
    public abstract int moveRight();
}
