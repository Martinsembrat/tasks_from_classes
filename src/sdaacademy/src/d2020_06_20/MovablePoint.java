package sdaacademy.src.d2020_06_20;

public class MovablePoint extends Movable{

    protected int x;
    protected int y;
    protected int xSpeed;
    protected int ySpeed;

    MovablePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x=x;
        this.y=x;
        this.xSpeed=xSpeed;
        this.ySpeed=ySpeed;
    }


    @Override
    public int moveUp() {
        y = y+ ySpeed;
        return y;
    }

    @Override
    public int moveDown() {
       y=y - ySpeed;
        return y;
    }

    @Override
    public int moveLeft() {
        x= x - xSpeed;
        return x;
    }

    @Override
    public int moveRight() {
        x=x-xSpeed;
        return x;
    }
}
