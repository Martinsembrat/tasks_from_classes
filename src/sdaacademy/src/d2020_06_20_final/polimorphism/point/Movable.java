package sdaacademy.src.d2020_06_20_final.polimorphism.point;

public interface Movable {

    void moveUp();//== public abstract void moveUp()
    void moveDown();//== public abstract void moveDown()
    void moveLeft();//== public abstract void moveLeft();
    void moveRight();//== public abstract void moveRight();
}
