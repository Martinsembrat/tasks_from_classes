package sdaacademy.src.d2020_06_20_final.polimorphism.point;

//deklaracja mechanizmu wyświetlania
public interface Presenter {

    void present();
}
