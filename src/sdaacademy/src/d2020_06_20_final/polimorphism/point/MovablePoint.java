package sdaacademy.src.d2020_06_20_final.polimorphism.point;

public class MovablePoint implements Movable, Presenter {

    int x;
    int y;
    int xSpeed;
    int ySpeed;

    public MovablePoint() {

    }

    public MovablePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    @Override
    public void moveUp() {
        y += ySpeed;//->y = y + ySpeed;
    }

    @Override
    public void moveDown() {
        y -= ySpeed;//->y = y - ySpeed;
    }

    @Override
    public void moveLeft() {
        x -= xSpeed;//->x = x - xSpeed;
    }

    @Override
    public void moveRight() {
        x += xSpeed;//->x = x + xSpeed;
    }

    @Override
    public String toString() {
        return String.format("(%d, %d)", x, y);
    }

    @Override
    public void present() {
        System.out.println("Point: ");
        System.out.println("x: " + x);
        System.out.println("y: " + y);
        System.out.println("xSpeed:" + xSpeed);
        System.out.println("ySpeed:" + ySpeed);
    }

    public static void main(String[] args) {
        //Mamy dostęp do metod interejsu Movable i interfejsu Presenter i pól/metod klasy MovablePoint
        MovablePoint movablePoint = new MovablePoint();
        movablePoint.moveDown();
        movablePoint.moveLeft();
        movablePoint.moveRight();
        movablePoint.moveUp();
        movablePoint.present();
        System.out.println(movablePoint.x);
        System.out.println(movablePoint.y);


//        //obiekt movablePointv1 przyjmuje postać Movable to mamy dostęp tylko do cech interfejsu Movable
//        Movable movablePointv1 = new MovablePoint();
//        movablePointv1.moveDown();
//        movablePointv1.moveLeft();
//        movablePointv1.moveRight();
//        movablePointv1.moveUp();
//        movablePointv1.present();
//        System.out.println(movablePointv1.x);
//        System.out.println(movablePointv1.y);
//
//        //obiekt movablePointv2 przyjmuje postać Presenter to mamy dostęp tylko do cech interfejsu Presenter
//        Presenter movablePointv2 = new MovablePoint();
//        movablePointv2.moveDown();
//        movablePointv2.moveLeft();
//        movablePointv2.moveRight();
//        movablePointv2.moveUp();
//        movablePointv2.present();
//        System.out.println(movablePointv2.x);
//        System.out.println(movablePointv2.y);
    }
}
