package sdaacademy.src.d2020_06_20_final.polimorphism.task;

public interface Shape {

    double getArea();
    double getPerimeter();
}
