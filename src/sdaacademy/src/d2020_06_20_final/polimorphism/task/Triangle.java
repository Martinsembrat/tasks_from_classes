package sdaacademy.src.d2020_06_20_final.polimorphism.task;

public class Triangle implements Shape {

    private double a;
    private double h;

    public Triangle(double a) {
        this.a = a;
        this.h = a*Math.sqrt(3)/2;
    }

    @Override
    public double getArea() {
        return a*h/2;
    }

    @Override
    public double getPerimeter() {
        return 3*a;
    }
}
