package sdaacademy.src.d2020_06_20_final.polimorphism.task;

/*
1. Napisz interfejs ShapeCalculation, który będzie realizować poniższe metody:
- getArea()
- getPerimeter()
Na podstawie interfejsu shape zaimplementuj klasy szczegółowe:
- Rectangle z polami: szerokość, wysokość
- Cicrlce z polami: promień
- Triangle z polami: wysokość i długośicą boku (zakładamy, że trójkąt jest równoboczny)

Zaimplementuj metodę statyczną która na podstawie typu ShapeCalculation obliczy i pole o obwód każdego z obiektu
 */
/*
2. Napisz interfejs Presenter, który bedzie realizować funkcjonalność prezentacji:
- void present();
Zaimplementuj interfejs Presenter w każdej z klas: Triangle, Circle, Rectangle
Zaimplementuj metodę statyczną która na podstawie typu Presenter wyświetli szczegóły każdego z przekazanych obiektów
 */
public class Task {

    public static void main(String[] args) {
        Shape[] shapes = new  Shape[]{
                new Circle(10),
                new Rectangle(20, 30),
                new Triangle(40),
        };
        for (Shape shape : shapes) {
            shapeCalculate(shape);
        }
//        ShapeCalculation circle = new Circle(10);
//        Rectangle rectangle = new Rectangle(20, 30);
//        Triangle triangle = new Triangle(40);
//        shapeCalculate(circle);
//        shapeCalculate(rectangle);
//        shapeCalculate(triangle);
    }

    public static void shapeCalculate(Shape shape) {
        System.out.println("Pole: " + shape.getArea());
        System.out.println("Obwód: " + shape.getPerimeter());
    }
}
