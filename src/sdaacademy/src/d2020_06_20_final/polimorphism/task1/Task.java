package sdaacademy.src.d2020_06_20_final.polimorphism.task1;

/*
1. Napisz interfejs ShapeCalculation, który będzie realizować poniższe metody:
- getArea()
- getPerimeter()
Na podstawie interfejsu shape zaimplementuj klasy szczegółowe:
- Rectangle z polami: szerokość, wysokość
- Cicrlce z polami: promień
- Triangle z polami: wysokość i długośicą boku (zakładamy, że trójkąt jest równoboczny)

Zaimplementuj metodę statyczną która na podstawie typu ShapeCalculation obliczy i pole o obwód każdego z obiektu
 */
/*
2. Napisz interfejs Presenter, który bedzie realizować funkcjonalność prezentacji:
- void present();
Zaimplementuj interfejs Presenter w każdej z klas: Triangle, Circle, Rectangle
Zaimplementuj metodę statyczną która na podstawie typu Presenter wyświetli szczegóły każdego z przekazanych obiektów
 */
public class Task {

    public static void main(String[] args) {
        Triangle triangle = new Triangle("blue", 10);
        Circle circle = new Circle("red", 20);
        Rectangle rectangle = new Rectangle("green", 10, 20);

        shapeCalculate(triangle);
        shapeCalculate(circle);
        shapeCalculate(rectangle);

        shapePresenter(triangle);
        shapePresenter(circle);
        shapePresenter(rectangle);

        shapeAbstract(triangle);
        shapeAbstract(circle);
        shapeAbstract(rectangle);

        //shapeAnimator(triangle);//klasa Triangle nie implementuje interfejsu Animator
        //shapeAnimator(rectangle);//klasa Rectangle nie implementuje intrefejsu Animator
        shapeAnimator(circle);//klasa Circle implementuje interfejs Animator

        ApplicationPresenter applicationPresenter = new ApplicationPresenter();
        shapePresenter(applicationPresenter);
        //shapeCalculate(applicationPresenter);//klasa ApplicationPresenter nie implementuje interfejsu ShapeCalculation
        //shapeAbstract(applicationPresenter);//klasa ApplicationPresenter nie rozszerza klasy Shape//
    }

    public static void shapeAnimator(Animator animator) {
        animator.animate(20);
    }

    public static void shapeAbstract(Shape shape) {
        System.out.println("Pole: " + shape.getArea());
        System.out.println("Obwód: " + shape.getPerimeter());
        shape.present();
    }

    public static void shapePresenter(Presenter presenter) {
        presenter.present();
    }

    public static void shapeCalculate(ShapeCalculation shapeCalculation) {
        System.out.println("Pole: " + shapeCalculation.getArea());
        System.out.println("Obwód: " + shapeCalculation.getPerimeter());
    }
}
