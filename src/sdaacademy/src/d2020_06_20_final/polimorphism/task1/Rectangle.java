package sdaacademy.src.d2020_06_20_final.polimorphism.task1;

public class Rectangle extends Shape {

    private double a;
    private double b;

    public Rectangle(String color, double a, double b) {
        super(color);
        this.a = a;
        this.b = b;
    }

    @Override
    public double getArea() {
        return a*b;
    }

    @Override
    public double getPerimeter() {
        return 2*a+2*b;
    }

    @Override
    public void present() {
        System.out.println("Rectangle with a " + a + " b " + b + " and color " + color);
    }
}
