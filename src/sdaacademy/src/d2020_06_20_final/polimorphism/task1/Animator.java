package sdaacademy.src.d2020_06_20_final.polimorphism.task1;

public interface Animator {

    void animate(int scale);
}
