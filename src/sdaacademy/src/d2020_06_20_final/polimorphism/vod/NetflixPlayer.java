package sdaacademy.src.d2020_06_20_final.polimorphism.vod;

public class NetflixPlayer implements MediaPlayer {

    public String name = "Netflix";

    public void getInfoAboutPlayer() {
        System.out.println("To jest player na licencji Netflix.com");
    }

    @Override
    public void stop() {
        //advanced netflix player logic
        System.out.println("Netflix request: stop()");
    }

    @Override
    public void play() {
        //advanced netflix player logic
        System.out.println("Netflix request: play()");
    }

    @Override
    public void pause() {
        //advanced netflix player logic
        System.out.println("Netflix request: pause()");
    }
}
