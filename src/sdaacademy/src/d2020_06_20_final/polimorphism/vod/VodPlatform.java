package sdaacademy.src.d2020_06_20_final.polimorphism.vod;

public class VodPlatform {

    public static void main(String[] args) {
        //MediaPlayer player = new NetflixPlayer();
        DisneyPlusPlayer player = new DisneyPlusPlayer();
        DisneyPlusPlayer disneyPlusPlayer = (DisneyPlusPlayer) player;
        System.out.println("Player: " + player);
        System.out.println("Disney plus player: " + disneyPlusPlayer);
//        player.pause();
//        player.stop();
//        player.play();
        player.pause();
        player.play();
        player.stop();
        ((DisneyPlusPlayer) player).getPlayerMetaData();
        System.out.println(((DisneyPlusPlayer) player).name);
        System.out.println("Czy są równe: ");
        System.out.println(player.equals((DisneyPlusPlayer)player));
        // nie możliwe do realizcji: interfesj MediaPlayer nie ma takiej metody
        //player.getPlayerMetaData();
        //nie możliwe do bezpośredniej realizacji, interfejs MediaPlyer nie ma takiej metody
        //player.name;
        //odtwarzanie multimediów
        play(player);
        pause(player);
        stop(player);
    }

    public static void play(MediaPlayer player) {
        player.play();
    }

    public static void pause(MediaPlayer player) {
        player.pause();
    }

    public static void stop(MediaPlayer player) {
        player.stop();
    }

}
