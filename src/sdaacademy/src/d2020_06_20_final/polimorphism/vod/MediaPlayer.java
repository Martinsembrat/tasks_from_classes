package sdaacademy.src.d2020_06_20_final.polimorphism.vod;

public interface MediaPlayer {

    void stop();

    void play();

    void pause();
}
