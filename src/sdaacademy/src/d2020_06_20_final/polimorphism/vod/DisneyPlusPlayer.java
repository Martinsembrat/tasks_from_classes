package sdaacademy.src.d2020_06_20_final.polimorphism.vod;

public class DisneyPlusPlayer implements MediaPlayer{

    public String name = "DinseyPlus";

    public void getPlayerMetaData() {
        System.out.println("To jest player od Disney+!");
    }

    @Override
    public void stop() {
        //advanced netflix player logic
        System.out.println("Dinsey+ request: stop()");
    }

    @Override
    public void play() {
        //advanced netflix player logic
        System.out.println("Dinsey+ request: play()");
    }

    @Override
    public void pause() {
        //advanced netflix player logic
        System.out.println("Dinsey+ request: pause()");
    }
}
