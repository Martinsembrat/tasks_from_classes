package sdaacademy.src.d2020_06_20_final.polimorphism.task2;

public class ApplicationPresenter implements Presenter {
    @Override
    public void present() {
        System.out.println("SDA app presenter!");
    }
}
