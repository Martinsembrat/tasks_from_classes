package sdaacademy.src.d2020_06_20_final.polimorphism.task2;

public abstract class Shape implements ShapeCalculation, Presenter {

    protected String color;

    public Shape(String color) {
        this.color = color;
    }
}
