package sdaacademy.src.d2020_06_20_final.polimorphism.task2;

public class Circle extends Shape implements Animator {

    private double radius;

    public Circle(String color, double radius) {
        super(color);
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI*radius*radius;
    }

    @Override
    public double getPerimeter() {
        return 2*Math.PI*radius;
    }

    @Override
    public void present() {
        System.out.println("Cricle with radius: " + radius + " and color: " + color);
    }

    @Override
    public void animate(int scale) {
        radius = radius*scale;
    }
}
