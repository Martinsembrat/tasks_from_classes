package sdaacademy.src.d2020_06_20_final.polimorphism.task2;

public class Triangle extends Shape {

    private double a;
    private double h;

    public Triangle(String color, double a) {
        super(color);
        this.a = a;
        this.h = a*Math.sqrt(3)/2;
    }

    @Override
    public double getArea() {
        return a*h/2;
    }

    @Override
    public double getPerimeter() {
        return 3*a;
    }

    @Override
    public void present() {
        System.out.println("Triangle with a " + a + " h " + h + " + and " + color);
    }
}
