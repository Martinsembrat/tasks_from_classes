package sdaacademy.src.d2020_06_20_final.polimorphism.task2;

public interface Presenter {

    void present();
}
