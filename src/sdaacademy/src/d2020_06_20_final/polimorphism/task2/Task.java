package sdaacademy.src.d2020_06_20_final.polimorphism.task2;

/*
1. Napisz interfejs ShapeCalculation, który będzie realizować poniższe metody:
- getArea()
- getPerimeter()
Na podstawie interfejsu shape zaimplementuj klasy szczegółowe:
- Rectangle z polami: szerokość, wysokość
- Cicrlce z polami: promień
- Triangle z polami: wysokość i długośicą boku (zakładamy, że trójkąt jest równoboczny)

Zaimplementuj metodę statyczną która na podstawie typu ShapeCalculation obliczy i pole o obwód każdego z obiektu
 */
/*
2. Napisz interfejs Presenter, który bedzie realizować funkcjonalność prezentacji:
- void present();
Zaimplementuj interfejs Presenter w każdej z klas: Triangle, Circle, Rectangle
Zaimplementuj metodę statyczną która na podstawie typu Presenter wyświetli szczegóły każdego z przekazanych obiektów
 */
public class Task {

    public static void main(String[] args) {
        Triangle triangle = new Triangle("blue", 10);
        Circle circle = new Circle("red", 20);
        Rectangle rectangle = new Rectangle("green", 10, 20);

        shapeCalculate(triangle);
        shapeCalculate(circle);
        shapeCalculate(rectangle);

        shapePresenter(triangle);
        shapePresenter(circle);
        shapePresenter(rectangle);

        shapeAbstract(triangle);
        shapeAbstract(circle);
        shapeAbstract(rectangle);

        //shapeAnimator(triangle);//klasa Triangle nie implementuje interfejsu Animator
        //shapeAnimator(rectangle);//klasa Rectangle nie implementuje intrefejsu Animator
        shapeAnimator(circle);//klasa Circle implementuje interfejs Animator

        ApplicationPresenter applicationPresenter = new ApplicationPresenter();
        shapePresenter(applicationPresenter);
        //shapeCalculate(applicationPresenter);//klasa ApplicationPresenter nie implementuje interfejsu ShapeCalculation
        //shapeAbstract(applicationPresenter);//klasa ApplicationPresenter nie rozszerza klasy Shape//

        Shape circleShape = new Circle("yellow", 10);
        Shape rectangleShape = new Rectangle("red", 2, 4);
        findCircle(circleShape);
        findCircle(rectangleShape);
    }

    //metoda statyczna zwracająca obiekt typu animator
    public static Animator createAnonymousAnimator() {
        //klasa Circle
        Animator animator = new Circle("purple", 20);//klas circle implementuje interfejs animator
        Animator animatorx = new Circle("white", 23);

        //klasa anonimowa
        //tworzymu obiekt typu Animator o nazwie animator1
        //powstaje w oparciu o klasę anonimową, bez nazwy, ale implemenutjącą interfejs Animator
        Animator animator1 = new Animator() {
            @Override
            public void animate(int scale) {
                System.out.println("Anonymous animator: scale");
            }
        };
        //klasa anonimowa przestaje istnieć, mamy już tylko obiekt animator1
        animator1.animate(20);
        return animator1;
    }

    public static void findCircle(Shape shape) {
        Animator animator = new Circle("black", 10);
        if (shape instanceof Circle) {
            Circle circle = (Circle) shape;
            System.out.println("Konwersja możliwa!");
        }else {
            System.out.println("Konwersja niemożliwa!");
        }
    }

    public static void shapeAnimator(Animator animator) {
        animator.animate(20);
    }

    public static void shapeAbstract(Shape shape) {
        System.out.println("Pole: " + shape.getArea());
        System.out.println("Obwód: " + shape.getPerimeter());
        shape.present();
    }

    public static void shapePresenter(Presenter presenter) {
        presenter.present();
    }

    public static void shapeCalculate(ShapeCalculation shapeCalculation) {
        System.out.println("Pole: " + shapeCalculation.getArea());
        System.out.println("Obwód: " + shapeCalculation.getPerimeter());
    }
}
