package sdaacademy.src.d2020_06_20_final.abstract_package;

public abstract class GraphicObject {

    protected String color;
    protected boolean isFill;

    public GraphicObject(String color, boolean isFill) {
        this.color = color;
        this.isFill = isFill;
    }

    protected void fillColor() {
        if (isFill) {
            System.out.println("Fill with color: " + color);
        } else {
            System.out.println("Only border should have color: " + color);
        }
    }

    public abstract void draw();
}
