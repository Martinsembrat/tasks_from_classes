package sdaacademy.src.d2020_06_20_final.abstract_package.task;

public abstract class Movable {

    public abstract void moveUp();
    public abstract void moveDown();
    public abstract void moveLeft();
    public abstract void moveRight();
}
