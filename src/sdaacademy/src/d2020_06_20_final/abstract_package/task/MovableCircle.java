package sdaacademy.src.d2020_06_20_final.abstract_package.task;

public class MovableCircle extends Movable {

    private int radius;
    //private MovablePoint movablePoint; //kompozycja -> obiekt jest nullem
    private MovablePoint movablePoint = new MovablePoint();//kompozycja -> obiekt już istnieje

    public MovableCircle(int radius, int x, int y, int xSpeed, int ySpeed) {
        this.radius = radius;
        //tworzony jest obiekt na podstawie wskazanych parametrów
        //movablePoint = new MovablePoint(x, y, xSpeed, ySpeed);
        //aktaulizowane są wartości pól już istniejącego obiektu
        movablePoint.x = x;
        movablePoint.y = y;
        movablePoint.xSpeed = xSpeed;
        movablePoint.ySpeed = ySpeed;
    }

    public MovableCircle(int radius, MovablePoint movablePoint) {
        this.radius = radius;
        this.movablePoint = movablePoint;
    }

    @Override
    public void moveUp() {
        movablePoint.moveUp();
    }

    @Override
    public void moveDown() {
        movablePoint.moveDown();
    }

    @Override
    public void moveLeft() {
        movablePoint.moveLeft();
    }

    @Override
    public void moveRight() {
        movablePoint.moveRight();
    }

    @Override
    public String toString() {
        return String.format("Circle with radius %d and center point: %s", radius, movablePoint);
    }
}
