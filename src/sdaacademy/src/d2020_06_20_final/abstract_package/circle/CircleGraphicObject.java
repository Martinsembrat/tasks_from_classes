package sdaacademy.src.d2020_06_20_final.abstract_package.circle;

import sdaacademy.src.d2020_06_20_final.abstract_package.GraphicObject;


public class CircleGraphicObject extends GraphicObject {

    private int radius;

    public CircleGraphicObject(String color, int radius, boolean isFill) {
        super(color, isFill);
        this.radius = radius;
    }

    @Override
    public void draw() {
        System.out.println("Draw circle with radius: " + radius + " and color: " + color);
        fillColor();
    }
}
