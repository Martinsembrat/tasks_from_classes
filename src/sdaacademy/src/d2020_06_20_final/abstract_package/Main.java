package sdaacademy.src.d2020_06_20_final.abstract_package;

import sdaacademy.src.d2020_06_20_final.abstract_package.circle.CircleGraphicObject;
import sdaacademy.src.d2020_06_20_final.abstract_package.rectangle.RectangleGraphicObject;

public class Main {

    public static void main(String[] args) {
        //brak możliwości tworzenia instacji klasy abstrakcyjnej
        //GraphicObject graphicObject = new GraphicObject("red");
        //graphicObject.draw();

        CircleGraphicObject circleGraphicObject = new CircleGraphicObject("green", 10, false);
        circleGraphicObject.draw();

        RectangleGraphicObject rectangleGraphicObject = new RectangleGraphicObject("yellow", 30, 40, true);
        rectangleGraphicObject.draw();
    }
}
