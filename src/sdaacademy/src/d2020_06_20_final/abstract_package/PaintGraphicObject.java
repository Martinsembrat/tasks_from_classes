package sdaacademy.src.d2020_06_20_final.abstract_package;

public abstract class PaintGraphicObject extends GraphicObject {

    public PaintGraphicObject(String color, boolean isFill) {
        super(color, isFill);
    }

    public abstract void draw3D();
}
