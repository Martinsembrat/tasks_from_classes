package sdaacademy.src.d2020_06_20_final.abstract_package.rectangle;

import sdaacademy.src.d2020_06_20_final.abstract_package.PaintGraphicObject;

public class RectangleGraphicObject extends PaintGraphicObject {

    private int a;
    private int b;

    public RectangleGraphicObject(String color, int a, int b, boolean isFill) {
        super(color, isFill);
        this.a = a;
        this.b = b;
    }

    @Override
    public void draw() {
        System.out.println("Draw rectangle with width: " + a + " and height: " + b + " in color: " + color);
        fillColor();
    }

    @Override
    public void draw3D() {
        System.out.println("Draw rectangle 3D!");
    }
}
