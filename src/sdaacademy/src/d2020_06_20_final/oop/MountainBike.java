package sdaacademy.src.d2020_06_20_final.oop;

//MountainBike<-Bike<-Object
public class MountainBike extends Bicycle {

    private int damper;
    private int wheelSize;

//    public MountainBike(int damper, int wheelSize) {
//        //super reprezentuje klasę nadrzędną Bicycle
//        //w poniższy sposób wywołujemy konstruktor z klasy nadrzędnej Bicycle
//        //super działa w odniesieniu tylko do bezpośredniego przodka
//        super(0, 0, 0);
//        this.damper = damper;
//        this.wheelSize = wheelSize;
//    }

    public MountainBike(int damper, int wheelSize, int startCadence,
                        int startSpeed,
                        int startGear) {
        //super reprezentuje klasę nadrzędną Bicycle
        //w poniższy sposób wywołujemy konstruktor z klasy nadrzędnej Bicycle
        //super działa w odniesieniu tylko do bezpośredniego przodka
        super(startCadence, startSpeed, startGear);
        this.damper = damper;
        this.wheelSize = wheelSize;
        super.speed = startSpeed;
    }

    public static void main(String[] args) {
        MountainBike mountainBike = new MountainBike(3, 10, 0, 0, 2);
    }
}
