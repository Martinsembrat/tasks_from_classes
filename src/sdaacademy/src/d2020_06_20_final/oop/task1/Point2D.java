package sdaacademy.src.d2020_06_20_final.oop.task1;

/*
### Klasa Point2D
Zaimplementuj klasę `Point2D`. Klasa powinna zawierać:
* dwa pola typu float: `x`, `y`
* konstruktor bezparametrowy ustawiający wartość pól `x` i `y` na `0`
* konstruktor z dwoma parametrami: `float x`, `float y`
* metody typu getter odpowiedzialne za zwracanie wartości zmiennej: `x`, `y`
* metodę getXY zwracającą współrzędne x i y w postaci tablicy dwuelementowej
* metody typu setter odpowiedzialne za ustawianie wartości pól `x`, `y`
* metodę setXY ustawiającą współrzędne `x` i `y`
* metoda `toString` powinna zwracać łańcuch tekstowy o następującym formacie: `(x, y)`;
 */
public class Point2D {

    private float x, y;

    public Point2D() {
        this.x = 0;
        this.y = 0;
    }

    public Point2D(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float[] getXY() {
        return new float[] {x, y};
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setXY(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("(%f, %f)", x, y);
    }
}
