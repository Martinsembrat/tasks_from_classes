package sdaacademy.src.d2020_06_20_final.oop.task1;

/*
### Klasa Point3D
Na podstawie klasy Point2D zaimplementuj klasę Point3D. Klasa ta powinna rozszerzać klasę Point2D oraz dodawać następującą implementację:
* pole prywatne typu float: `z`
* konstruktor przyjmujący wartości dla pól: `x`, `y`, `z`
* metodę typu getter odpowiedzialną za zwracanie wartości zmiennej `z`
* metodę `getXYZ` zwracającą współrzędne `x`, `y`, `z` w postaci tablicy trzyelementowej
* metodę typu setter odpowiedzialną za ustawianie zmiennej `z`
* metodę `setXYZ` ustawiającą  wartości dla zmiennych `x`, `y`, `z`
* metoda `toString` powinna zwracać łańcuch tekstowy o następującym formacie: `(x, y, z)`;
 */
public class Point3D extends Point2D {

    private float z;

    public Point3D(float x, float y, float z) {
        super(x, y);
        this.z = z;
        //alternatywnie
//        setX(x);
//        setY(y);
    }

    public float getZ() {
        return z;
    }

    public float[] getXYZ() {
        return new float[] {getX(), getY(), z};
    }

    public void setZ(float z) {
        this.z = z;
    }

    public void setXYZ(float x, float y, float z) {
        this.setXY(x, y);
        this.z = z;
    }

    @Override
    public String toString() {
        return String.format("(%f, %f, %f)", getX(), getY(), getZ());
    }

    public static void main(String[] args) {
        Point2D point2D = new Point2D();
        point2D.setXY(10, 20);
        System.out.println("Point2D: " + point2D);

        Point3D point3D = new Point3D(45, 56, 2);
        System.out.println("Point3D: " + point3D);
        point3D.setXY(10, 20);
        System.out.println("Point3D update: " + point3D);
    }
}
