package sdaacademy.src.d2020_06_20_final.oop;

//klasa nadrzędna dla klasy Bicycle to klasa Object
public class Bicycle {

    public int cadence;
    public int gear;
    public int speed;

    public Bicycle(int startCadence, int startSpeed, int startGear) {
        this.cadence = startCadence;
        this.speed = startSpeed;
        this.gear = startGear;
    }

    public void setCadence(int cadence) {
        this.cadence = cadence;
    }

    public void setGear(int speed) {
        this.speed = speed;
    }

    public void applyBreak(int decrement) {
        speed -= decrement;
    }

    public void speedUp(int increment) {
        speed += increment;
    }

    //wskazuje, że metoda pochodzi z klasy nadrzędnej
    @Override
    public String toString() {
        //super mówi by odwołać się do funkcjonalności bezpośredniej klasy nadrzędnej, w tym przypadku metody toString z klasy Object
        String baseClassToStringResult = super.toString();
        return baseClassToStringResult + " " + speed + " " + cadence + " " + gear;
    }

    public static void main(String[] args) {
        Bicycle bicycle = new Bicycle(10, 0, 1);
        String result = bicycle.toString();
        System.out.println(result);
    }
}
