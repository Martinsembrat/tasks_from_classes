package sdaacademy.src.d2020_06_20_final.oop;

public class OOPExample {
}

class Teacher {
    private final String name;
    private final String lastName;
    private final int age;
    private final String email;
    private float salary;

    public Teacher(String name, String lastName, int age, String email, float salary) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.email = email;
        this.salary = salary;
    }


    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public int getAge() {
        return age;
    }

}
