package sdaacademy.src.d2020_06_20_final.oop;

public class Employee extends Person{

    private float salary;
    private String position;

    public Employee(String name, String lastName, float salary, String position) {
        super(name, lastName);
        this.salary = salary;
        this.position = position;
    }

    @Override
    public void showPerson() {
        super.showPerson();
        System.out.println("@@@@@@@@@@@@@@@@@@@@");
        System.out.println("Salary: " + salary);
        System.out.println("Position: " + position);
        System.out.println("@@@@@@@@@@@@@@@@@@@@");
    }

    @Override
    public String toString() {
        String superToString = super.toString();
        return String.format("Parent toString: [%s]," +
                " Employee: %f, %s", superToString, salary, position);
    }

    public static void main(String[] args) {
        System.out.println("------------------Student--------------------");
        Student student = new Student("Wacław", "Nowak",
                2, "IT", "3232");
        System.out.println(student);
        student.showPerson();
        System.out.println("------------------Student--------------------");

        System.out.println("------------------Employee--------------------");
        Employee employee = new Employee("John", "Smith",
                3000, "Manager");
        System.out.println(employee);
        employee.showPerson();
        System.out.println("------------------Employee--------------------");


        System.out.println("------------------Example--------------------");
        Person person = new Person("Jan", "Kowalski");
        //obiekt typu student powstał na podstawie danych z obiektu Person
        Student student1 = new Student(person.name, person.lastName, 1,
                "Biologia", "323");
        student1.showPerson();
    }
}
