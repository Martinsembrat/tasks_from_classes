package sdaacademy.src.d2020_06_20_final.oop;

public class Person {
    public String name;
    public String lastName;

    public Person(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public void showPerson() {
        System.out.println("*******************");
        System.out.println("Name: " + name);
        System.out.println("Last name: " + lastName);
        System.out.println("*******************");
    }

    @Override
    public String toString() {
        String superToString = super.toString();
        return String.format("Parent toString: %s, Person: %s, %s", superToString, name, lastName);
    }

    public static void main(String[] args) {
        Person person = new Person("Jan", "Kowalski");
        person.showPerson();
        System.out.println(person);
    }
}
