package sdaacademy.src.d2020_06_20_final.oop.week2.access_modifiers.base;

public class Cat extends Animal {

    public void showCat() {
        System.out.println("Type: " + type);
        System.out.println("Country: " + country);
        System.out.println("Age: " + age);
        //System.out.println("Name: " + name);
    }
}
