package sdaacademy.src.d2020_06_20_final.oop.week2.access_modifiers.base;

public class Animal {
    public String type;
    protected String country;
    int age;
    private String name;

    protected void showDetails() {
        System.out.println("Type: " + type);
        System.out.println("Country: " + country);
        System.out.println("Age: " + age);
        System.out.println("Name: " + name);
        //unikajamy cyklicznych zależności!
//        Dog dog = new Dog();
//        System.out.println(dog.country);
//        System.out.println(dog.type);
    }
}
