package sdaacademy.src.d2020_06_20_final.oop.week2.access_modifiers.base;

public class ZooAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal();//klasa z pakietu access_modifiers.base
        System.out.println("Public access: " + animal.type);
        System.out.println("Protected access: " + animal.country);
        System.out.println("Package access: " + animal.age);
        animal.showDetails();
//        System.out.println("Private access: " + animal.name);
//
//        Dog dog = new Dog();//klasa za pakietu access_modifiers.dog
//        System.out.println("Race: " + dog.getRace());
//        System.out.println("Public access: " + dog.type);
//        System.out.println("Protected access: " + dog.country);
//        System.out.println("Package access: " + dog.age);
//        System.out.println("Private access: " + dog.name);
//        System.out.println("Private access: " + dog.race);
    }
}
