package sdaacademy.src.d2020_06_20_final.oop.week2.access_modifiers;

import sdaacademy.src.d2020_06_20_final.oop.week2.access_modifiers.base.Animal;

public class Zoo {

    public static void main(String[] args) {
        Animal animal = new Animal();
        System.out.println("Public access: " + animal.type);
//        System.out.println("Protected access: " + animal.country);
//        System.out.println("Package access: " + animal.age);
//        System.out.println("Private access: " + animal.name);
//
//        Dog dog = new Dog();
//        System.out.println("Public access: " + dog.type);
//        System.out.println("Protected access: " + dog.country);
//        System.out.println("Package access: " + dog.age);
//        System.out.println("Private access: " + dog.name);
//        System.out.println("Private access: " + dog.race);
    }
}
