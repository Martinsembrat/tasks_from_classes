package sdaacademy.src.d2020_06_20_final.oop.week2.access_modifiers.dog;

import sdaacademy.src.d2020_06_20_final.oop.week2.access_modifiers.base.Animal;

public class ZooDog {

    public static void main(String[] args) {
        Animal animal = new Animal();
//        System.out.println("Public access: " + animal.type);
//        System.out.println("Protected access: " + animal.country);
//        System.out.println("Package access: " + animal.age);
//        System.out.println("Private access: " + animal.name);
//
//        Dog dog = new Dog();
//        //brak dostępu:
//        dog.showDetails();//pole country, metoda showDetails() będzie widoczna tylko w
//        // pakiecie access_modifiers.base
//        // oraz w klasach pochodnych, np. Dog
//        dog.showDog();
//        System.out.println("Public access: " + dog.type);
//        System.out.println("Protected access: " + dog.country);
//        System.out.println("Package access: " + dog.age);
//        System.out.println("Private access: " + dog.name);
//        System.out.println("Private access: " + dog.race);
    }
}
