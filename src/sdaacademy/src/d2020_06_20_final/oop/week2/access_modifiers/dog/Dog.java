package sdaacademy.src.d2020_06_20_final.oop.week2.access_modifiers.dog;

import sdaacademy.src.d2020_06_20_final.oop.week2.access_modifiers.base.Animal;

public class Dog extends Animal {

    private String race;

    protected String getRace() {
        return race;
    }

    protected void showDog() {
        showDetails();
        System.out.println("Race: " + race);
        System.out.println("Type (public): " + type);
        System.out.println("Country (protected): " + country);
//        System.out.println("Age (package): " + age);
//        System.out.println("Name (private): " + name);
    }
}
