package sdaacademy.src.d2020_06_20_final.oop.week2.student;

import sdaacademy.src.d2020_06_20_final.oop.week2.Person;

public class Student extends Person {

    private int yearOfStudies;

    public void setYearOfStudies(int yearOfStudies) {
        this.yearOfStudies = yearOfStudies;
    }

    public void showInfo(){
        System.out.println("Name: " + name);
        System.out.println("Last name: " + lastName);
        System.out.println("Rok studiów: " + yearOfStudies);
    }
}