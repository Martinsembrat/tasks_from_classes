package sdaacademy.src.d2020_06_20_final.oop.week2;

public class Person {
    protected String name;
    protected String lastName;

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}

