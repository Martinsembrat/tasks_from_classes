package sdaacademy.src.d2020_06_20_final.oop;

import sdaacademy.src.d2020_06_20_final.oop.week2.Person;
import sdaacademy.src.d2020_06_20_final.oop.week2.student.Student;

public class AccessModifiers {

    public static void main(String[] args) {
        Person person = new Person();
        //person.name = "sd"; //nie możliwe do realizacji po za pakietem
        person.setName("Jan");
        person.setLastName("Kowalski");

        Student student = new Student();
       // student.setName("Wacław");
        //student.setLastName("Nowak"); cos nie dziala
        student.setYearOfStudies(323);
        //student.name = "sds"; //nie możliwe do realizacji po za pakietem
    }
}
