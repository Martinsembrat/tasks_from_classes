package sdaacademy.src.d2020_06_20_final.interfaces_extends;

public class MovablePoint extends Point implements Movable, Presenter {

    int xSpeed;
    int ySpeed;

    public MovablePoint() {

    }

    public MovablePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    @Override
    public void moveUp() {
        y += ySpeed;//->y = y + ySpeed;
    }

    @Override
    public void moveDown() {
        y -= ySpeed;//->y = y - ySpeed;
    }

    @Override
    public void moveLeft() {
        x -= xSpeed;//->x = x - xSpeed;
    }

    @Override
    public void moveRight() {
        x += xSpeed;//->x = x + xSpeed;
    }

    @Override
    public void present() {
        System.out.println("Movable point: " + toString());
    }


    @Override
    public String toString() {
        return String.format("(%d, %d)", x, y);
    }

    @Override
    public void navigate(char direction) {
        //TODO: implementation here!
    }
}
