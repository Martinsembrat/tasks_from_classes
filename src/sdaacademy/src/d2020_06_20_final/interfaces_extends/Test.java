package sdaacademy.src.d2020_06_20_final.interfaces_extends;

public class Test {

    public static void main(String[] args) {
        MovablePoint movablePoint = new MovablePoint(0, 0, 2, 4);
        System.out.println(movablePoint);
        movablePoint.moveUp();
        movablePoint.moveUp();
        movablePoint.moveRight();
        movablePoint.moveDown();
        System.out.println(movablePoint);

        MovableCircle movableCircle = new MovableCircle(10, 10, -20, 3, 1);
        System.out.println(movableCircle);
        movableCircle.moveLeft();
        movableCircle.moveLeft();
        movableCircle.moveDown();
        System.out.println(movableCircle);

        MovableCircle movableCircle1 = new MovableCircle(20, movablePoint);
        System.out.println(movableCircle1);
    }
}
