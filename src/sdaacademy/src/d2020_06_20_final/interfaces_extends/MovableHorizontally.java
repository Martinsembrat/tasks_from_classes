package sdaacademy.src.d2020_06_20_final.interfaces_extends;

public interface MovableHorizontally {

    void moveLeft();
    void moveRight();
}
