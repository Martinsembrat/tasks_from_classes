package sdaacademy.src.d2020_06_20_final.interfaces_extends;

public interface MovableVertically {

    void moveUp();
    void moveDown();
}
