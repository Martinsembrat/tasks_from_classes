package sdaacademy.src.d2020_06_20_final.interfaces_extends;

/*interfejs Movable składa się z 5 abstrakcyjnych metod
MovableHorizontally
    void moveLeft();
    void moveRight();
MovableVertically
    void moveUp();
    void moveDown();
Movable
    void navigate(char direction);
 */
public interface Movable extends MovableHorizontally, MovableVertically {

    void navigate(char direction);

}
