package sdaacademy.src.d2020_06_20_final.interfaces_extends;

//deklaracja mechanizmu wyświetlania
public interface Presenter {

    void present();
}
