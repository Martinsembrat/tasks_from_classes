package sdaacademy.src.d2020_06_20_final.overloading;

public class MovablePoint extends Movable {

    @Override
    public void moveUp() {

    }

    @Override
    public void moveDown() {
        super.moveDown();
        System.out.println("...........");
    }

    @Override
    public void moveLeft() {

    }

    @Override
    public void moveRight() {

    }

    public static void main(String[] args) {
        MovablePoint movablePoint = new MovablePoint();
        movablePoint.moveDown();
    }
}
