package sdaacademy.src.d2020_06_20_final.overloading;

public class Movable {

    /*nie skompiluje się, mimo różnych typów zwracanych metody mają te same parametry wejściowe (typy + liczba)
    float abs(float a) {
        return a;
    }

    int abs(float a) {
        return a;
    }
    */

    public Movable() {

    }

    public Movable(int x, int y){

    }

    public Movable(int value) {

    }

    //przeładowana metoda
    void moveUp() {
        System.out.println("moveUp");
    }

    //przeładowana metoda
    void moveUp(int step) {

    }

    //przeładowana metoda
    void moveUp(int step, int speed) {

    }

    void moveDown() {
        System.out.println("moveDown");
    }
    void moveLeft() {
        System.out.println("moveLeft");
    }
    void moveRight() {
        System.out.println("moveRight");
    }
}
