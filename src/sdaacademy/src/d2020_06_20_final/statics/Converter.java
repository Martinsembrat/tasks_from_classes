package sdaacademy.src.d2020_06_20_final.statics;

public class Converter {

    public String converterType;

    public Converter(String converterType) {
        this.converterType = converterType;
    }

    public void convert() {
        System.out.println("Convert using: " + converterType);
    }

    public static void convertStatic() {
        System.out.println("Static converter");
    }
}
