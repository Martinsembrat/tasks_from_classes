package sdaacademy.src.d2020_06_20_final.statics;

public class Main {

    public static void main(String[] args) {
        Converter converter = new Converter("temperatureConverter");
        converter.convert();

        Converter.convertStatic();

        Counter.incrementStaticCounter();
        System.out.println(Counter.getStaticCounter());
    }
}
