package sdaacademy.src.d2020_06_20_final.statics;

public class TemperatureConverter {

    //zmienna stała: definiowana przez static final
    public static final float KELVIN_CONVERSION_FACTOR = 273.15f;

    public static float convertKToC(float kTemp) {
        return kTemp - KELVIN_CONVERSION_FACTOR;
    }

    public static float convertCToK(float cTemp) {
        return cTemp + KELVIN_CONVERSION_FACTOR;
    }
}
