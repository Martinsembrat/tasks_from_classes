package sdaacademy.src.d2020_06_20_final.statics;

public class Counter {

    private int counter = 0;
    private static int staticCounter = 0;

    //z poziomy danego obiektu możemy odwołać się do pól statycznych
    public void incrementCounter() {
        counter++;
        Counter.incrementStaticCounter();
    }

    public static void incrementStaticCounter() {
        staticCounter++;
    }

    public int getCounter() {
        return counter;
    }

    //z poziomu metod statycznych nie ma mamy możliwości odwołania się do this
    public static int getStaticCounter() {
        //this.counter++; -> nie wolno tego robić
        return staticCounter;
    }
}
