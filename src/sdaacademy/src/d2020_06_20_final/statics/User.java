package sdaacademy.src.d2020_06_20_final.statics;

/*
Zaimplementuj klasę user która będzie zawierała pola takie jak:
- imię
- nazwisko
- wiek
- pesel
- obywatelstwo: może być "PL" bądź "NO_PL"
- dochody
- czy był karany
Zaproponuj które z powyższych pól może być immutable
Dodatkowo zaimplementuj metodę statyczną showUser która jako argument metody statycznej przyjmie obiekt klasy User i
wyświetli informację na jego temat
 */
public class User {
    private static final String PL_CITIZENSHIP = "PL";
    private static final String NO_PL_CITIZENSHIP = "NO_PL";
    private final long id;
    private String name;
    private String lastName;
    private int age;
    private String citizenship;
    private float salary;
    private boolean hasPunished;

    public User(long id, String name, String lastName, int age, String citizenship, float salary) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.citizenship = citizenship;
        this.salary = salary;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public void setHasPunished(boolean hasPunished) {
        this.hasPunished = hasPunished;
    }

    //metoda obiektowa ma informację tylko i wyłącznie o obiekcie który wywołuje metodę,
    // dostęp do pól tego obiektu jest realizowany za pomocą this
    public void showUser() {
        String userInfo = String.format("------------\nImię: %s, Nazwisko: %s, " +
                        "Wiek: %d, Pesel: %d, Obywatelstwo: %s," +
                        " Wynagrodzenie: %f, Czy był karany: %s\n------------", this.name, this.lastName, this.age,
                this.id, this.citizenship, this.salary, this.hasPunished);
        System.out.println(userInfo);
    }

    //metoda statyczna nie ma pojęcia o danych z klasy User, dlateog przekazujemy obiekt User userArg na rzecz które
    // będziemy wywoływac operacje
    public static void showUser(User userArg) {
        String userInfo = String.format("**********\nImię: %s, Nazwisko: %s, " +
                "Wiek: %d, Pesel: %d, Obywatelstwo: %s," +
                " Wynagrodzenie: %f, Czy był karany: %s\n*************", userArg.name, userArg.lastName, userArg.age,
                userArg.id, userArg.citizenship, userArg.salary, userArg.hasPunished);
        System.out.println(userInfo);
    }

    public static void main(String[] args) {
        //Nazwa zaczynająca się od dużej litery do nazwa klasy, np. User
        //Nazwa zaczynająca się od małej litery wskazuje na to, że to obiekt, np. userObj, userObj1
        User userObj = new User(9434343, "Wacław", "Nowak", 32, PL_CITIZENSHIP, 3344);
        User userObj1 = new User(9434354, "Jan", "Kowalski", 43, NO_PL_CITIZENSHIP, 5000);
        userObj.setHasPunished(true);
        //metody statyczne wywołujemy na rzecz klasy
        User.showUser(userObj);//argument userArg będzie reprezentować obiekt userObj
        User.showUser(userObj1);//argument userArg będzie reprezentować obiekt userObj1

        //metody obiektowe wywoływane są na rzecz obiektu
        userObj.showUser();
        userObj1.showUser();
    }
}
