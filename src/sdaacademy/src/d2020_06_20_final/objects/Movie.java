package sdaacademy.src.d2020_06_20_final.objects;

/*
Stwórz klasę Movie z polami:
- tytuł filmu
- reżyer filmy
- gatunek
- rok wydania
- liczba_nagród
- ranking_box_office
Dodatkowo zaimplementuj:
- konstruktor z 6 argumentami
- konstruktor umożliwiający ustawienie wartości dla pól:  tytuł, autor, gatunek, rok wydania
- metodę displayMovie odpowiedzialną za zwrócenie szczegółówu na temat książki
- metody modyfikujące wartość dla liczba_nagród, ranking_box_office (settery, przyjmuje wartość zwraca void)
- metodę toString zwracającą string do wyświetlenia przez sout
- metodę equals do porównywania obiektów

Zaprezentuj wszystko na przykładzie
 */
public class Movie {
    private String title;
    private String director;
    private String genre;
    private int yearOfRelease;
    private int numberOfPrice;
    private int boxOfficeRating;

    public Movie(String title, String director, String genre, int yearOfRelease, int numberOfPrice, int boxOfficeRating) {
        this.title = title;
        this.director = director;
        this.genre = genre;
        this.yearOfRelease = yearOfRelease;
        this.numberOfPrice = numberOfPrice;
        this.boxOfficeRating = boxOfficeRating;
    }

    public Movie(String title, String director, String genre, int yearOfRelease) {
        this.title = title;
        this.director = director;
        this.genre = genre;
        this.yearOfRelease = yearOfRelease;
    }

    public void displayMovie() {
        String message = String.format("Movie: %s %s %s %d %d %d",
                title, director, genre, yearOfRelease, numberOfPrice, boxOfficeRating);
        System.out.println(message);
    }

    public void setNumberOfPrice(int numberOfPrice) {
        this.numberOfPrice = numberOfPrice;
    }

    public void setBoxOfficeRating(int boxOfficeRating) {
        this.boxOfficeRating = boxOfficeRating;
    }

    public String toString() {
        return  String.format("Movie from toString: %s %s %s %d %d %d",
                title, director, genre, yearOfRelease, numberOfPrice, boxOfficeRating);
    }


    public boolean equals(Movie m) {
        return this.title.equals(m.title) && this.director.equals(m.director) && this.yearOfRelease == m.yearOfRelease;
    }

}
