package sdaacademy.src.d2020_06_20_final.objects;

public class Library {

    public static void main(String[] args) {
        Book book = new Book(null,
                "J.K Rowling",
                "Fantasy",
                2003);
        System.out.println(book);
        book.displayBook();
        Book book1 = new Book("Zbrodnia i Kara");
        System.out.println(book1);
        book1.displayBook();
        Book book2 = new Book("Lotr", "xxx");
        System.out.println(book2);
        book2.displayBook();

        Book book3 = new Book(2003);//konstruktor ustawi wartość pola yearOfRelease
        book3.displayBook();
        Book book4 = new Book("2003");//konstruktor ustawi wartość pola title
        book4.displayBook();


        //porównywanie obiektów
        Book b1 = new Book("Test", "Test", "Test", 1);
        Book b2 = new Book("Test", "Test", "Test1", 11);

        //operator == porównuje obiektu poprzez referencję -> adres w pamięci
        //this == b1 (obiekt na rzecz którego wywoływana jest metoda equals)
        //bookToCompare (argument metody equals) == b2 (wartość tego argumentu)
        if (b1.equals(b2)) {
            System.out.println("Obiekty są równe");
        } else {
            System.out.println("Obiekty nie są równe");
        }
    }
}
