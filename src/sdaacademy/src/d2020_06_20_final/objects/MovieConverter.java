package sdaacademy.src.d2020_06_20_final.objects;

/*
Napisz klasę która będzie odpowiedzialna za konwersję tablicy lini csv na tablicę obiektów move.
Wygeneruj odpowiednią tablicę String i na jej podstawie dokonaj konwersji

* napisz metodę która umożliwi wyszukanie filmu o konkretnym tytule w tablicy
* napisz metodę która zwróci wszystkie filmy z kategori akcja
* napisz metodę odpowiedzialną za prezentację wszystkich filmów w tablicy

String[] moviesCSV = new String[] {
        "Star Wars,J.J Ambrams,Action,2015,4,5",
        "Harry Potter,unknown,Fantasy,2001,3,10",
        "Titanic,Cameron,History,1999,4,2"
};
 */
public class MovieConverter {


    public static Movie[] convertToMovieArray(String[] csv) {
        //tworzenie tablicy o rozmiarze tablicy csv z danymi
        Movie[] movies = new Movie[csv.length];
        //iteracja po wszystkich elementach tablicy csv
        for (int i=0;i<csv.length;i++) {
            //dzielenie poszczególnej lini pliku na tablicę,
            // każdy element oddzielony przecinkiem stanowi oddzielny element tablicy
            String[] movieData = csv[i].split(",");
            //przypisanie poszczególnych elementów tablicy do pól
            String title = movieData[0];
            String director = movieData[1];
            String genre = movieData[2];
            //Integer.parseInt - konwersja pola typu string na int
            int yearOfRelease = Integer.parseInt(movieData[3]);
            int numberOfPrice = Integer.parseInt(movieData[4]);
            int boxOfficeRating = Integer.parseInt(movieData[5]);
            //tworzenie obiektu klasy movie na podstawie przekazanych parametrów
            Movie movie = new Movie(title, director, genre, yearOfRelease, numberOfPrice, boxOfficeRating);
            //dodanie obiektu do tablicy
            movies[i] = movie;
        }
        return movies;
    }
}
