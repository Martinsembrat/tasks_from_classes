package java_19_04;

/*
Zadanie 13. Napisz program usuwający duplikaty z tablicy i
zwracający nową tablicę.
Sample array: [20, 20, 30, 40, 50, 50, 50] New array: [20, 30, 40, 50]
 */
public class Excercise13V2 {

    public static void main(String[] args) {
        //----------- Przygotowanie zmiennych
        //wartości tablicy >= 0
        int[] input = {20, 20, 30, 40, 50, 50, 50};//tablica wejścia
        int[] tmpResult = new int[input.length];//tablica tymczasowa, która ma rozmiar tablicy wejściowej,
        // może być nadmiarowa
        int counter = 0;//zmienna odpowiedzialna za zliczenie liczby unikalnych elementów

        //-----------Wyszukiwanie elementów unikalnych
        for (int i=0;i<input.length;i++) {
            int element = input[i];
            boolean isUnique = true;
            //sprawdzamy, że mamy dany element w tablicy result
            for (int j=0;j<input.length;j++) {
                if (element == tmpResult[j]) {
                    //element jest już w tablicy result- nie jest unikalny
                    isUnique = false;
                    break;
                }
            }
            if (isUnique) {
                //pierwszy element unikalny trafi pod indeks 0
                //drugi element unikalny trafi pod indeks 1
                //trzeci element unikalny trafi pof indeks 2
                //itd.
                tmpResult[counter] = element;
                counter++;
            }else {
                //tutaj jest duplikat
                tmpResult[i] = -1;
            }
        }
        //wyświetlanie elementów listy TMP
        for (int i=0;i<tmpResult.length;i++) {
            System.out.println(tmpResult[i]);
        }
        //rozmiar tablicy na unikalne elementy będzie równy counter
        int[] finalResult = new int[counter];
        //uzupełnianie tablica finalna
        for (int i=0;i<counter;i++) {
            //tmpResult[0] -> pierwszy unikat
            //tmpResult[1] -> drugi unikat
            //tmpResult[2] -> trzeci unikat
            //itd
            finalResult[i] = tmpResult[i];
        }


        System.out.println("final");
        //wyświetlanie elementów listy
        for (int i=0;i<finalResult.length;i++) {
            System.out.println(finalResult[i]);
        }
    }
}
