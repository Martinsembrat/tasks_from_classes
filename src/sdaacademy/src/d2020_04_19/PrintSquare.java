package java_19_04;

public class PrintSquare {

    public static void main(String[] args) {

        //przesunięcie: 3, l. wierszy: 3, l.kolumn: 3
        /*
          *** -> 2 spacje i 3 gwiazdki
         *** -> 1 spacja i 3 gwiazdki
        ***  -> 3 gwiazdki
        */


        /*

        *****
        *****
        *****
        *****
        *****

         */

        //pętla zewnętrzna odpowiedzialna za rysowanie wierszy
        for (int i=0;i<5;i++) {
            //pętla zagnieżdżona odpowiedzialna za rysowanie znaku * w ramach wiersza
            for (int j=0;j<5;j++){
                System.out.print("*");
            }
            System.out.println();
        }
        //sposób mechaniczny
//        System.out.println();
//        for (int i=0;i<5;i++) {
//            System.out.print("*");
//        }
//        System.out.println();
//        for (int i=0;i<5;i++) {
//            System.out.print("*");
//        }
//        System.out.println();
//        for (int i=0;i<5;i++) {
//            System.out.print("*");
//        }
//        System.out.println();
//        for (int i=0;i<5;i++) {
//            System.out.print("*");
//        }
    }
}
