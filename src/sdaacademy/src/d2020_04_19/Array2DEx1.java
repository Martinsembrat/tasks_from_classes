package sdaacademy.src.d2020_04_19;

public class Array2DEx1 {

    public static void main(String[] args) {
        int[][] table = new int[9][9];
        /*
        0 0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 0
         */
        //pętla odpowiedzialna za wyliczanie wartości z tabliczki mnożenia
        //iterowanie po każdym wierszu tablicy dwuwymiarowej
        for (int i=0;i<table.length;i++) {
            //iterowanie po każdej kolumnie konkretnej tablicy zagnieżdżonej
            for (int j=0;j<table[i].length;j++) {
                //obliczanie wartości dla konkretnej komórki tabeli, wartości i oraz j zwiększane są od 1
                // by zignorować wartości 0
                table[i][j] = (i+1)*(j+1);
            }
        }

        //wyświetlanie wszystkich elementów tabliczki mnożenia
        for (int i=0;i<table.length;i++) {
            for (int j=0;j<table[i].length;j++) {
                System.out.print(table[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}
