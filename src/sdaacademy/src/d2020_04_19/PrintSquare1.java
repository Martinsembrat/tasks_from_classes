package java_19_04;

import java.util.Scanner;

/*
   *** -> 2 spacje i 3 gwiazdki
  *** -> 1 spacja i 3 gwiazdki
 ***  -> 3 gwiazdki
 */

/*
row = 3
row - i - 1
określamy relację pomiędzy numerem iteracji a liczbą spacji, można zauważyć że wraz z kolejnymi iteracjami
zmniejsza się liczba spacji jak poniżej
dla 3 wierszy maksymalna liczba spacji jest o jeden mniejsza, czyli row - 1,
i spada każdorazowo po przejściu do kolejnego wiersza co jeden czyli row - 1 -i;
i=0: 2 spacje -> po podstawieniu do wzoru: 3 - 0 - 1 = 2;
i=1: 1 spacje -> po podstawieniu do wzoru: 3 - 1 - 1 = 1;
i=2: 0 spacja -> po podstawieniu do wzoru: 3 - 2 - 1 = 0;
 */
public class PrintSquare1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe wierszy: ");
        int row = scanner.nextInt();
        System.out.println("Podaj liczbe kolumn: ");
        int column = scanner.nextInt();
        System.out.println("Podaj znak uzupełniający: ");
        char sign = scanner.next().charAt(0);

        for (int i=0;i<row;i++){
            //pętla zagnieżdżona odpowiedzialna za wyświetlenie przesunięca w ramach wiersza
            for (int space=0; space < row - i - 1; space++) {
                System.out.print(" ");
            }
            //pętla zagnieżdżona odpowiedzialna za wyświetlenie znaku wskazanego przez użytkownika
            for (int j=0;j<column;j++) {
                System.out.print(sign);
            }
            System.out.println();
        }
    }
}
