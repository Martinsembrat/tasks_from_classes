package sdaacademy.src.d2020_04_19;

public class Array3D {

    public static void main(String[] args) {
        int[][][] test = {
                {
                        {1, -2, 3},
                        {2, 3, 4}
                },
                {
                        {-4, -5, 6, 9},
                        {1},
                        {2, 3}
                }
        };

        //test.length -> pobieranie liczby tablic dwuwymiarowych w ramach tablicy test
        for (int i=0;i<test.length;i++) {
            //test[i].length -> pobierz ilość wierszy tablicy dwuwymiarowej o indeksie i
            for (int j=0;j<test[i].length;j++) {
                //test[i][j].length -> pobierz rozmiar tablicy jednowymiarowej
                for (int k=0;k<test[i][j].length;k++) {
                    System.out.print(test[i][j][k]);
                    System.out.print(" ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }
}
