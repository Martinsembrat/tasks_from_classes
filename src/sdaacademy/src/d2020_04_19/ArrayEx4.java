package sdaacademy.src.d2020_04_19;

/*
Dla tablicy int[][]
{
 {6, 2, 1},
 {2, 4, 1},
 {2, 2, 0}
}
Sprawdź czy suma któregoś z wierszy jest wartością parzystą
 */
public class ArrayEx4 {

    public static void main(String[] args){
        int[][] tab = {
                {6, 2, 1},//9 -> nie spełnia założeń
                {2, 4, 1},//7 -> nie spełnia założeń
                {2, 2, 0}//4 -> spełnia założenia
        };

        for(int i=0;i<tab.length;i++) {
            int sum = 0;
            //zliczamy sume wszystkich elementów ramach danego wiersza
            for (int j=0;j<tab[i].length;j++) {
                sum += tab[i][j];//sum = sum + tab[i][j]
            }
            if (sum % 2 == 0) {
                System.out.println("Suma wartości danej tablicy jednowymiarowej jest parzysta!");
                break;
            } else {
                System.out.println("Sprawdzamy dalej!");
            }
        }
    }

}
