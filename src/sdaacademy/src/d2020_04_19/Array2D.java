package sdaacademy.src.d2020_04_19;

public class Array2D {

    public static void main(String[] args) {
        int[][] array2d = new int[3][4];
        System.out.println("Liczba wierszy: ");
        //pobieranie liczby wierszy
        System.out.println(array2d.length);
        System.out.println("Liczba kolumn: ");
        //pobieranie liczby kolumn w danym wierszu, w tym przypadku liczby kolumn wiersza 1
        System.out.println(array2d[0].length);

        System.out.println("Tablica a .... ");

        int[][] a = {
                {1, 2, 3},
                {4, 5, 6, 9},
                {7}
        };
        System.out.println("Liczba wierszy");
        System.out.println(a.length);
        System.out.println("Liczba kolumn wiersza 0");
        System.out.println(a[0].length);//3 elementy
        System.out.println("Liczba kolumn wiersza 1");
        System.out.println(a[1].length);//4 elementy
        System.out.println("Liczba kolumn wiersza 2");
        System.out.println(a[2].length);//1 element

        System.out.println("------------------");
        //a.length -> pobieranie liczby wierszy
        for (int i=0;i<a.length;i++) {
            //a[i].length -> pobieranie rozmiaru konkretnej tablicy zagnieżdżonej
            for (int j=0;j<a[i].length;j++) {
                System.out.print(a[i][j]);
                System.out.print(", ");
            }
            //wyświetlamy nowy wiersz
            System.out.println();
        }

        System.out.println("Drugi zapis pętla foreach ------------------");
        //a.length -> pobieranie liczby wierszy
        for (int[] nestedTab : a) {
            //a[i].length -> pobieranie rozmiaru konkretnej tablicy zagnieżdżonej
            for (int j=0;j<nestedTab.length;j++) {
                System.out.print(nestedTab[j]);
                System.out.print(", ");
            }
            //wyświetlamy nowy wiersz
            System.out.println();
        }
    }
}
