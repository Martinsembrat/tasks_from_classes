package sdaacademy.src.d2020_08_22;


import java.util.HashMap;

public class Garage {

    private int trukSpace;
    private int carspace;
    private int motoBikeSpace;
    private HashMap <String,Vehicle> space;

    public Garage() {
        this.trukSpace = trukSpace;
        this.carspace = carspace;
        this.motoBikeSpace = motoBikeSpace;
        this.space = new HashMap<>();
    }

    public void CheckId (String id){
            if (space.containsKey(id) == true) {
                System.out.println("Error");}
            else {System.out.println("No License Plate detected in garage");
    }}


//       switch () {
//        case Motobike:
//            return  (motoBikeSpace > 0) ? space.put() : System.out.println("Brak Miejsca");
//            break;
//    }

    public int getTrukSpace() {
        return trukSpace;
    }

    public void setTrukSpace(int trukSpace) {
        this.trukSpace = trukSpace;
    }

    public int getCarspace() {
        return carspace;
    }

    public void setCarspace(int carspace) {
        this.carspace = carspace;
    }

    public int getMotoBikeSpace() {
        return motoBikeSpace;
    }

    public void setMotoBikeSpace(int motoBikeSpace) {
        this.motoBikeSpace = motoBikeSpace;
    }

    public HashMap<String, Vehicle> getMiejsca() {
        return space;
    }

    public void setMiejsca(HashMap<String, Vehicle> miejsca) {
        this.space = miejsca;
    }
}
