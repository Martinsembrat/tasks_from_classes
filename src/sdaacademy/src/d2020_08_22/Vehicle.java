package sdaacademy.src.d2020_08_22;

public class Vehicle {

    private String id;
    private double size;
    private boolean parked;

    public Vehicle(String id, double size, boolean parked) {
        this.id = id;
        this.size = size;
        this.parked = parked;
    }
}

