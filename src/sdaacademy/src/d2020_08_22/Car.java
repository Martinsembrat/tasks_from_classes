package sdaacademy.src.d2020_08_22;

public class Car extends Vehicle{

    double size = 1;
    private static String TYPE = "CAR";

    public Car(String id, double size, boolean parked) {
        super(id, size, parked);
    }

    public static String getTYPE() {
        return TYPE;
    }
}
