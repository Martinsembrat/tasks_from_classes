package sdaacademy.src.d2020_08_22;

public class Truck  extends Vehicle{
    double size = 1.5;
    private static String TYPE = "TRUCK";


    public Truck(String id, double size, boolean parked) {
        super(id, size, parked);
    }

    public static String getTYPE() {
        return TYPE;
    }
}
