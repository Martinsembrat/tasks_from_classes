package sdaacademy.src.d2020_08_22;

public class Motobike  extends Vehicle{
    double size = 0.5;
    private static String TYPE = "MOTOBIKE";

    public Motobike(String id, double size, boolean parked) {
        super(id, size, parked);
    }

    public static String getTYPE() {
        return TYPE;
    }
}
