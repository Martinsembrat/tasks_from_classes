package sdaacademy.src.d2020_06_27.arraylist_collection.example3;

import java.util.ArrayList;

/*
### Klasa Student

Zaimplementuj klasę `Student`, która będzie zawierać pola: nr. ineksu, imię, nazwisko, rok studiów, specjalizacja (zdeklarowana jako enum z polskim i angielskim tłumaczeniem specjalizacji - zawiera dwa pola), lista ocen (ArrayList<Float> scores).  Uwzględnij wszystkie niezbędne metody oraz parametry konstruktora. Zaimplementuj metodę `equals`.
 */
public class Student {
    private int id;
    private String name;
    private String lastName;
    private int yearOfStudies;
    private StudyType studyType;
    private ArrayList<Float> scores = new ArrayList<>();

    public Student(int id,
                   String name,
                   String lastName,
                   int yearOfStudies,
                   StudyType studyType) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.yearOfStudies = yearOfStudies;
        this.studyType = studyType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getYearOfStudies() {
        return yearOfStudies;
    }

    public void setYearOfStudies(int yearOfStudies) {
        this.yearOfStudies = yearOfStudies;
    }

    public StudyType getStudyType() {
        return studyType;
    }

    public void setStudyType(StudyType studyType) {
        this.studyType = studyType;
    }

    public void addScore(Float score) {
        if (score != null && score > 0) {
            scores.add(score);
        }
    }

    public ArrayList<Float> getScores() {
        return scores;
    }

    public float getAverageScore() {
        float scoresSum = 0;
        for (float score : scores) {
            scoresSum+=score;
        }
        if (!scores.isEmpty()) {
            return scoresSum/scores.size();
        }
        return scoresSum;
    }

    //metoda wygenerowana przez intellij
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Student student = (Student) o;
//        return id == student.id &&
//                yearOfStudies == student.yearOfStudies &&
//                Objects.equals(name, student.name) &&
//                Objects.equals(lastName, student.lastName) &&
//                studyType == student.studyType;
//    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Student)) {
            //kończymy działanie metody
            return false;
        }
        Student objToCompare = (Student) object;
        //return this.id == ((Student)object).id
        return this.id == objToCompare.id
                && this.name.equals(objToCompare.name)
                && this.lastName.equals(objToCompare.lastName)
                && this.studyType == objToCompare.studyType;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", yearOfStudies=" + yearOfStudies +
                ", studyType=" + studyType +
                ", scores=" + scores +
                '}';
    }
}
