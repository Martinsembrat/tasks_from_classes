package sdaacademy.src.d2020_06_27.arraylist_collection.example3;

import java.util.ArrayList;

/*
### Klasa StudentService

Zaimplementuj klasę `StudentService `, która będzie zawierać w sobie listę studentów, oraz będzie realizować poniższe metody:

* dodawanie studentów do listy
* usuwanie studentów z listy (po nr indeksu)
* zwracanie listy wszystkich studentów
* zwracanie studentów studiujących daną specjalizację
* zwracanie studentów będących na `n` roku studiów
* zwracanie średniej każdego studenta
* zwracanie najniższej średniej
* zwracanie najwyższej średniej

 */
public class StudentService {

    private ArrayList<Student> students = new ArrayList<>();

    public void addStudent(Student student) {
        //TODO: dodatkowa logika
        students.add(student);
    }

    public void removeStudentById(int id) {
        for (Student student : students) {
            if (student.getId() == id) {
                students.remove(student);
                return;
            }
        }
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public ArrayList<Student> findStudentsByStudyType(StudyType studyType) {
        ArrayList<Student> results = new ArrayList<>();
        for (Student student : students) {
            if (student.getStudyType() == studyType) {
                results.add(student);
            }
        }
        return results;
    }

    public ArrayList<Student> findStudentsByYearOfStudies(int yearOfStudies) {
        ArrayList<Student> results = new ArrayList<>();
        for (Student student : students) {
            if (student.getYearOfStudies() == yearOfStudies) {
                results.add(student);
            }
        }
        return results;
    }

    public ArrayList<Float> getStudentsAverageScores() {
        ArrayList<Float> results = new ArrayList<>();
        for (Student student : students) {
            results.add(student.getAverageScore());
        }
        return results;
    }

    public float findStudentWithMinAverageScore() {
        ArrayList<Float> averageScores = getStudentsAverageScores();
        float minValue = Float.MAX_VALUE;
        for (float averageScore : averageScores) {
            if (averageScore < minValue){
                minValue = averageScore;
            }
        }
        return minValue;
    }

    public float findStudentWithMaxAverageScore() {
        float maxValue = Float.MIN_VALUE;
        for (Student student : students) {
            float averageValue = student.getAverageScore();
            if (averageValue > maxValue) {
                maxValue = averageValue;
            }
        }
        return maxValue;
    }

    public StudentAverageScore findStudentWithMaxAverageScoreWithId() {
        StudentAverageScore studentAverageScore = new StudentAverageScore(Integer.MIN_VALUE, Float.MIN_VALUE);
        for (Student student : students) {
            float averageValue = student.getAverageScore();
            if (averageValue > studentAverageScore.getAverageScore()) {
                studentAverageScore.setAverageScore(averageValue);
                studentAverageScore.setStudentId(student.getId());
            }
        }
        return studentAverageScore;
    }
}
