package sdaacademy.src.d2020_06_27.arraylist_collection.example3;

public class StudentAverageScore {
    private int studentId;
    private float averageScore;

    public StudentAverageScore(int studentId, float averageScore) {
        this.studentId = studentId;
        this.averageScore = averageScore;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public float getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(float averageScore) {
        this.averageScore = averageScore;
    }

    @Override
    public String toString() {
        return "StudentAverageScore{" +
                "studentId=" + studentId +
                ", averageScore=" + averageScore +
                '}';
    }
}
