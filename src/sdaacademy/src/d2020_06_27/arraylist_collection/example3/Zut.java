package sdaacademy.src.d2020_06_27.arraylist_collection.example3;

public class Zut {

    public static void main(String[] args) {
        StudentService studentService = new StudentService();
        Student student = new Student(123, "Jan", "Nowak", 3,
                StudyType.COMPUTER_SCIENCE);
        student.addScore(4f);
        student.addScore(4.5f);
        student.addScore(3f);

        Student student1 = new Student(345, "Wacław", "Nowak", 2,
                StudyType.NANO_TECHNOLOGY);
        student1.addScore(2f);
        student1.addScore(2.5f);
        student1.addScore(3f);

        Student student2 = new Student(567, "Stanisław", "Kowalski", 3,
                StudyType.CIVIL_ENGINEERING);
        student2.addScore(5f);
        student2.addScore(5f);

        studentService.addStudent(student);
        studentService.addStudent(student1);
        studentService.addStudent(student2);

        System.out.println("Wszyscy studenci: ");
        System.out.println(studentService.getStudents());
        System.out.println("Studenci 3 roku: ");
        System.out.println(studentService.findStudentsByYearOfStudies(3));
        System.out.println("Studenci inżynierii środowiska");
        System.out.println(studentService.findStudentsByStudyType(StudyType.CIVIL_ENGINEERING));
        System.out.println("Średnie studentów");
        System.out.println(studentService.getStudentsAverageScores());
        System.out.println("Wartość min: ");
        System.out.println(studentService.findStudentWithMinAverageScore());
        System.out.println("Wartość max: ");
        System.out.println(studentService.findStudentWithMaxAverageScore());
        System.out.println("Wartość max v2: ");
        System.out.println(studentService.findStudentWithMaxAverageScoreWithId());
    }
}
