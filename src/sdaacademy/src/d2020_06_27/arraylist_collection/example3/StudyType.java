package sdaacademy.src.d2020_06_27.arraylist_collection.example3;

public enum StudyType {
    COMPUTER_SCIENCE("informatyka", "computer science"),
    CIVIL_ENGINEERING("inżynieria środowiska", "civil engineering"),
    MACHINE_LEARNING("uczenie maszynowe", "machine learning"),
    NANO_TECHNOLOGY("nano technologia", "nano technology");

    private String pl;
    private String en;

    StudyType(String pl, String en) {
        this.pl = pl;
        this.en = en;
    }

    public String getPl() {
        return pl;
    }

    public String getEn() {
        return en;
    }
}
