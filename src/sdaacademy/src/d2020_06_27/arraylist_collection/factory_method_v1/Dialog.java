package sdaacademy.src.d2020_06_27.arraylist_collection.factory_method_v1;

public abstract class Dialog {
    private OperationSystemType operationSystemType;
    protected String color;
    protected int width;
    protected int height;

    public Dialog(OperationSystemType type, String color, int width, int height) {
        this.operationSystemType = type;
        this.color = color;
        this.width = width;
        this.height = height;
    }

    public abstract void show();

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public OperationSystemType getOperationSystemType() {
        return operationSystemType;
    }

    public void setOperationSystemType(OperationSystemType operationSystemType) {
        this.operationSystemType = operationSystemType;
    }
}
