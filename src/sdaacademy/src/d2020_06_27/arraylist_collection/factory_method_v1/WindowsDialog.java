package sdaacademy.src.d2020_06_27.arraylist_collection.factory_method_v1;

public class WindowsDialog extends Dialog{
    public WindowsDialog() {
        super(OperationSystemType.WINDOWS, "blue", 100, 100);
    }

    @Override
    public void show() {
        System.out.println(getOperationSystemType());
        System.out.println("Size: " + width + ", " + height);
        System.out.println("Color: " + color);
    }
}
