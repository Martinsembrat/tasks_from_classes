package sdaacademy.src.d2020_06_27.arraylist_collection.factory_method_v1;

public class SystemDialogFactory implements DialogFactory {

    @Override
    public Dialog createDialog(OperationSystemType type) {
        switch (type) {
            case WINDOWS:
                //return -> zwracamy obiekt kończymy metodę
                return new WindowsDialog();
            case OSX:
                return new OsxDialog();
            case LINUX:
                return new LinuxDialog();
            case ANDROID:
                return new AndroidDialog();
        }
        return null;
    }
}
