package sdaacademy.src.d2020_06_27.arraylist_collection.factory_method_v1;

public class OperationSystem {

    public static void main(String[] args) {
        //wykorzystanie polimorfizmu, do postacji DialogFragment
        //przypisujemy konkretną implementację
        //mówimi interfejsowi DialogFragment, że ma zachowywać się jak funkcjonalność SystemDialogFactory
        //bądź też jak MobileSystemDialogFactory
        //Typ DialogFactory może przyjąć dowolną postać, wystarczy że dana klasa będzie
        //implementować interfejs DialogFactory
        DialogFactory dialogFactory = new SystemDialogFactory();
        //DialogFactory dialogFactory = new MobileSystemDialogFactory();

        Dialog dialog1 = dialogFactory.createDialog(OperationSystemType.LINUX);
        //dialog1 -> LinuxDialog
        dialog1.show();

        Dialog dialog2 = dialogFactory.createDialog(OperationSystemType.ANDROID);
        //dialog2 -> AndroidDialog
        dialog2.show();

        Dialog dialog3 = dialogFactory.createDialog(OperationSystemType.OSX);
        //dialog3 -> OsxDialog
        dialog3.show();

    }
}
