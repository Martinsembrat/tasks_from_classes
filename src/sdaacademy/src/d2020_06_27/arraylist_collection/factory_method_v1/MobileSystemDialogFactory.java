package sdaacademy.src.d2020_06_27.arraylist_collection.factory_method_v1;

public class MobileSystemDialogFactory implements DialogFactory{
    @Override
    public Dialog createDialog(OperationSystemType type) {
        return new AndroidDialog();
    }
}
