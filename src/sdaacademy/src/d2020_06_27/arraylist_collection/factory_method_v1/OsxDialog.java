package sdaacademy.src.d2020_06_27.arraylist_collection.factory_method_v1;

public class OsxDialog extends Dialog{
    public OsxDialog() {
        super(OperationSystemType.OSX, "red", 400, 800);
    }

    @Override
    public void show() {
        System.out.println("######################");
        System.out.println("########" + getOperationSystemType() + "#############");
        System.out.println("########" + color + "#############");
        System.out.println("########" + width + "#############");
        System.out.println("########" + height + "#############");
        System.out.println("######################");
    }
}
