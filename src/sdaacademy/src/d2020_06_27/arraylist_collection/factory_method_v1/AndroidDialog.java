package sdaacademy.src.d2020_06_27.arraylist_collection.factory_method_v1;

public class AndroidDialog extends Dialog{
    public AndroidDialog() {
        super(OperationSystemType.ANDROID, "yellow", 50, 50);
    }

    @Override
    public void show() {
        System.out.println("*********");
        System.out.println(getOperationSystemType());
        System.out.println("Size: " + width + ", " + height);
        System.out.println("Color: " + color);
        System.out.println("*********");
    }
}
