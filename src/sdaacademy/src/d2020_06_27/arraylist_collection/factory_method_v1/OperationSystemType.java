package sdaacademy.src.d2020_06_27.arraylist_collection.factory_method_v1;

public enum  OperationSystemType {
    WINDOWS,
    LINUX,
    OSX,
    ANDROID
}
