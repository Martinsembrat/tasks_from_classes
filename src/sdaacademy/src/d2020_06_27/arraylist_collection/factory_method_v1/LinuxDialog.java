package sdaacademy.src.d2020_06_27.arraylist_collection.factory_method_v1;

public class LinuxDialog extends Dialog{
    public LinuxDialog() {
        super(OperationSystemType.LINUX, "green", 200, 400);
    }

    @Override
    public void show() {
        System.out.println(OperationSystemType.LINUX + " , " + width + " , " + height + ", " + color);
    }
}
