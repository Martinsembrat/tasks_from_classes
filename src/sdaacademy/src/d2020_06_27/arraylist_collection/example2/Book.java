package sdaacademy.src.d2020_06_27.arraylist_collection.example2;

public class Book {

    private String title;
    private float price;
    private int yearOfRelease;
    private BookGenre bookGenre;

    public Book(String title, float price, int yearOfRelease, BookGenre bookGenre) {
        this.title = title;
        this.price = price;
        this.yearOfRelease = yearOfRelease;
        this.bookGenre = bookGenre;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public BookGenre getBookGenre() {
        return bookGenre;
    }

    public void setBookGenre(BookGenre bookGenre) {
        this.bookGenre = bookGenre;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", price=" + price +
                ", yearOfRelease=" + yearOfRelease +
                ", bookGenre=" + bookGenre +
                '}';
    }
}
