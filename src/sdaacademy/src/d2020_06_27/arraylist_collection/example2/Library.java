package sdaacademy.src.d2020_06_27.arraylist_collection.example2;

import java.util.ArrayList;
import java.util.List;

/*
 * dodawanie książek do listy
 * zwracanie listy wszystkich książek
 * zwracanie książek typu `Fantasy`
 * sprawdzanie czy konkretna książka (po tytule) znajduje się na liście
 */
public class Library {

    private ArrayList<Book> books = new ArrayList<>();

    public void addBook(Book book) {
        books.add(book);
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public ArrayList<Book> findBooksByGenre(BookGenre bookGenre) {
        ArrayList<Book> results = new ArrayList<>();
        for (Book book : books) {
            if (book.getBookGenre() == bookGenre) {
                results.add(book);
            }
        }
        return results;
    }

    public boolean hasBookWithTitle(String bookTitle) {
        for (Book book : books) {
            if (book.getTitle().contains(bookTitle)){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Library library = new Library();

        Book book = new Book("Harry Potter", 20, 2000, BookGenre.FANTASY);
        Book book1 = new Book("Lord of the Rings", 34, 1995, BookGenre.FANTASY);
        Book book2 = new Book("Spider Man", 40, 1980, BookGenre.COMICS);

        library.addBook(book);
        library.addBook(book1);
        library.addBook(book2);

        System.out.println(library.getBooks());

        System.out.println(library.findBooksByGenre(BookGenre.FANTASY));

        System.out.println(library.hasBookWithTitle("Harry"));
    }
}
