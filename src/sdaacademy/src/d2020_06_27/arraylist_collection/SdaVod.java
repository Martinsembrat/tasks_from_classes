package sdaacademy.src.d2020_06_27.arraylist_collection;

import java.util.ArrayList;
import java.util.List;

/*
dodawanie filmów do platformy
usuwanie filmów po nazwie
wyszukiwanie filmu po rating
zwracanie wszystkich filmów
sprawdzanie czy film o danym tytule znajduje się na platformie
 */
public class SdaVod {

    //listę movies można alternatywnie zainicjalizować w konstruktorze
    private ArrayList<Movie> movies = new ArrayList<>();

    public SdaVod() {
        System.out.println("Tworzenie platformy vod!");
        //inicjalizacja na poziomie deklaracji bądź w konstrukotrze
        movies = new ArrayList<>();
    }

    //dodawanie filmów do platformy
    public void addMovie(Movie movie) {
        movies.add(movie);
    }

    //usuwanie filmów po nazwie
    public void removeMovieByTitle(String title) {
         for (Movie movie : movies) {
             if (movie.getTitle().equals(title)) {
                 movies.remove(movie);
                 return;
             }
         }
    }

    //wyszukiwanie filmu po rating
    public ArrayList<Movie> findMoviesByRating(int rating) {
        ArrayList<Movie> findResults = new ArrayList<>();
        for (Movie movie : movies) {
            if (movie.getRating() == rating) {
                findResults.add(movie);
            }
        }
        return findResults;
    }

    //zwracanie wszystkich filmów
    public ArrayList<Movie> getMovies() {
        return movies;
    }

    //sprawdzanie czy film o danym tytule znajduje się na platformie
    public boolean hasMovieWithTitle(String title) {
        for (Movie movie : movies) {
            //sprawdzamy czy tytuł jest dokładnie taki sam
            if (movie.getTitle().equals(title)) {
                return true;
            }
//            //sprawdzamy czy fraza zawiera się w tytule
//            if (movie.getTitle().contains(title)){
//                return true;
//            }
        }
        return false;
    }

    public static void main(String[] args) {
        SdaVod sdaVod = new SdaVod();

        Movie movie = new Movie("Star Wars", 12, "Episode 9");
        Movie movie1 = new Movie("Harry Potter", 8, "Komnata Tajemnic");
        Movie movie2 = new Movie("Lord of the Rings", 16, "Return of the King");
        sdaVod.addMovie(movie);
        sdaVod.addMovie(movie1);
        sdaVod.addMovie(movie2);
        System.out.println("Dostępne filmy: ");
        System.out.println(sdaVod.getMovies());
        System.out.println("Czy jest film Star Wars");
        System.out.println(sdaVod.hasMovieWithTitle("Star Wars"));
        System.out.println("Filmy o ratingu 16");
        System.out.println(sdaVod.findMoviesByRating(16));
        System.out.println("Usuwanie filmy harry potter: ");
        sdaVod.removeMovieByTitle("Harry Potter");
        System.out.println("Dostępne filmy na koniec: ");
        System.out.println(sdaVod.getMovies());
    }

}
