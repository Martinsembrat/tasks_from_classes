package sdaacademy.src.d2020_06_27.arraylist_collection.review;

import java.util.ArrayList;

public class ArrayListExample {

    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>(10);
        integers.add(10);
        integers.add(20);
        integers.add(null);
        integers.add(30);
        integers.add(0);

        System.out.println(integers.size());

        //do remove przekazaliśmy typ prymitywny int, więc usuwamy po ineksie
        integers.remove(0);
        System.out.println(integers);

        //do remove przekazuemy obiekt typu obiektowego, więc usuwamy element
        integers.remove(Integer.valueOf(0));
        //new Integer jest nieakutalne i zostanie usunięte w nowszych wersjach javy
        //integers.remove(new Integer(0));

        System.out.println(integers);

        ArrayList<String> list = new ArrayList<>(11);
        //add
        list.add("Jan");
        list.add("Wacław");
        list.add("Martin");
        //show items
        for (String item : list) {
            System.out.println(item);
        }
        //remove
        list.remove(0);
        list.remove("Wacław");
        //show items
        System.out.println(list);
    }
}
