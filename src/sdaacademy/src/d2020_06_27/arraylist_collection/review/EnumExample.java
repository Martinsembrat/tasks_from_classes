package sdaacademy.src.d2020_06_27.arraylist_collection.review;


public class EnumExample {

    public static void main(String[] args) {
        MovieType movieType = MovieType.FANTASY;
        System.out.println(movieType);
        System.out.println(movieType.getShortCut());
        for (MovieType movieType1 : MovieType.values()) {
            System.out.println(movieType1);
            System.out.println(movieType1.ordinal());
        }
    }

}

enum MovieType {
    FANTASY('F'),
    ACTION('A'),
    ANIMATION('N'),
    DRAMA('D');

    private char shortCut;

    MovieType(char shortCut) {
        this.shortCut = shortCut;
    }

    public char getShortCut() {
        return shortCut;
    }
}