package sdaacademy.src.d2020_06_27.arraylist_collection.review;

public class GenericExample {

    public static void main(String[] args) {
        Integer[] tab = new Integer[] { 10, 20, 30 };
        Container<Integer> container = new Container<>(12);
        Container<String> container1 = new Container<>("test");
        Container<Double> container2 = new Container<>(10.3);
        Container<Integer>[] containers = new Container[10];
    }
}

class Container<T> {
    T item;

    public Container(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }
}
