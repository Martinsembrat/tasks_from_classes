package sdaacademy.src.d2020_06_27.design_patters;

public interface ChocolateFactory {
    Chocolate produceChocolate(ChocolateType type);
}