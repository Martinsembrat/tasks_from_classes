package sdaacademy.src.d2020_06_27.design_patters;

public enum ChocolateType {
    MILK("milk"), NUTS("nuts"), DARK("dark");
    private String choclateType;

    ChocolateType(String choclateType) {
        this.choclateType = choclateType;
    }
}