package sdaacademy.src.d2020_06_27.design_patters;

public class MilkaFactory implements ChocolateFactory {
    @Override
    public Chocolate produceChocolate(ChocolateType type) {
        Chocolate chocolate = null;
        switch (type) {
            case DARK:
                chocolate = new DarkChocolate();
                break;
            case MILK:
                chocolate = new MilkChocolate();
                break;
            case NUTS:
                chocolate = new NutsChocolate();
                break;
        }
        return chocolate;
    }
}