package sdaacademy.src.d2020_06_27.design_patters;

public class DarkChocolate extends Chocolate {
    public DarkChocolate() {
        super(ChocolateType.DARK);
    }

    @Override
    public String getDescription() {
        return "Czekolada gorzka";
    }

    @Override
    public void showChocolate() {
        showCacaoInfo(90);
        showSugarInfo(10);
    }
}