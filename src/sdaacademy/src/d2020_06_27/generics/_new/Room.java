package sdaacademy.src.d2020_06_27.generics._new;

public class Room {

    public static void main(String[] args) {
        String text = "TextToStore";
        Box<String> stringBox = new Box<>(text);
        String textFromStringBox = stringBox.getObjectToStore();
        System.out.println(textFromStringBox);

        Car car = new Car("VW", "UP");
        //stringBox oczekuje obiektu typu String -> błąd kompilacji
        //stringBox.setObjectToStore(car);

        Box<Car> carBox = new Box<>(car);
        Car carFromCarBox = carBox.getObjectToStore();
        System.out.println(carFromCarBox);
    }
}
