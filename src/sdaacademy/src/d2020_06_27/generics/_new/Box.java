package sdaacademy.src.d2020_06_27.generics._new;

//literka T od Type
//T liczba i nazwy dowolne
//T pochodną klasy Object
public class Box<T> {
    //chcemy przechowywać Stringi, obiekty Car, Integer, itp.
    private T objectToStore;

    public Box(T objectToStore) {
        this.objectToStore = objectToStore;
    }

    public T getObjectToStore() {
        return objectToStore;
    }

    //stary sposób, z ryzkiem błędu na poziomie wywoływania
//    public Car getObjectToStore() {
//        return (Car) objectToStore;
//    }

    public void setObjectToStore(T o) {
        this.objectToStore = o;
    }
}
