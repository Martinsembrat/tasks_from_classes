package sdaacademy.src.d2020_06_27.generics.example2;

/*Zadanie typy generyczne:
        Zaprojektuj klasę Pair, która w oparciu o typy generyczne będzie umożliwiała przechowanie dowolnej pary obiektów.
        Klasa Pair oprócz standardowych setterów i getterów powinna zawierać metodę generyczną put,
        która będzie przyjmować zarówno klucz jak i wartość.*/

public class Pair<K, V> {
    private K key;
    private V value;
    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }
    public void setKey(K key) {
        this.key = key;
    }
    public K getKey() {
        return key;
    }
    public void setValue(V value) {
        this.value = value;
    }
    public V getValue() {
        return value;
    }
    public void put(K key, V value) {
        this.key = key;
        this.value = value;
    }
    public static void main(String[] args) {
        Pair<String, String> stringStringPair =
                new Pair<>("Sala 128", "Grupa JavaScn17");
        //Integer(typ obiektowy) - int(typ prymitywny)
        //Double(typ obiektowy) - double(typ prymitywny)
        //Long(typ obiektowy) - long(typ prymitywny)
        //Boolean(typ obiektowy) - boolean(typ prymitywny)
        //Character(typ obiektowy) - char(typ prymitywny)
        Integer value = 1;
        Long value2 = 2L;
        Long result = value + value2;
        //niepoprawne -> int nie jest typem obiektowym!
        /*Pair<int, String> integerStringPair =
                new Pair<>(128, "Grupa JavaScn17");*/
        Pair<Integer, String> integerStringPair =
                new Pair<>(128, "Grupa JavaScn17");
        Pair<Double, String> doubleStringPair =
                new Pair<>(34.78, "Test");
    }
}