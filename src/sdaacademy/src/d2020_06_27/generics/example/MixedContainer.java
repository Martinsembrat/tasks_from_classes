package sdaacademy.src.d2020_06_27.generics.example;

import sdaacademy.src.d2020_06_27.generics._new.Car;

public class MixedContainer<T, R> {
    private String text;
    private T field1;
    private R field2;

    public MixedContainer(String text, T t, R r) {
        this.text = text;
        this.field1 = t;
        this.field2 = r;
    }

    public T getField1() {
        return field1;
    }

    public R getField2() {
        return field2;
    }

    public static void main(String[] args) {
        MixedContainer<String, Car> container =
                new MixedContainer<>("text",
                        "t content",
                        new Car("VW", "UP"));

        String tValue = container.getField1();
    }
}
