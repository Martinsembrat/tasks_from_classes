package sdaacademy.src.d2020_06_27.enums;

public enum MonthV2 {

    //tworzone obiekty są static final
    JANUARY("styczeń", "january"),//ordinal == 0
    FEBRUARY("luty", "february"),//ordinal == 1
    MARCH("marzec", "march"),//ordinal == 2
    APRIL("kwiecień", "april"),//ordinal == 3
    MAJ("maj", "may"),//ordinal == 4
    JUNE("czerwiec", "june")////ordinal == 5
    //APRIL//trzeba wywołać konstruktor dwuargumentowy
    //stworzymy obiekty typu MonthV2
    ;//; oznacza koniec deklaracji obiektów typu MonthV2

    String namePl;//pole obiektowe klasy typu String
    private String nameEn;//pole obiektowe klasy typu String

    MonthV2(String namePl, String nameEn) {
        this.namePl = namePl;
        this.nameEn = nameEn;
    }

//    MonthV2() {
//
//    }

    public String getNameEn() {
        return nameEn;
    }
}
