package sdaacademy.src.d2020_06_27.enums.zad1;

public enum Weekday {
    MONDAY, //== MONDAY()
    TUESDAY,//== TUESDAY()
    WEDNESDAY,//== WEDNESDAY()
    THURSDAY,//== THURSDAY()
    FRIDAY,//== FRIDAY()
    SATURDAY,//== SATURDAY()
    SUNDAY;//== SUNDAY()

    Weekday() {
        //przykład pokazujący czym jest this
        System.out.println(this);
    }

    public boolean isHoliday() {
        return this == SATURDAY || this == SUNDAY;
    }

    public boolean isWeekday() {
        return !isHoliday();
    }

    public void whichIsGreater(Weekday weekday) {
        if (this.ordinal() < weekday.ordinal()) {
            System.out.printf("%s is lower than %s\n", this, weekday);
        } else if (this.ordinal() == weekday.ordinal()) {
            System.out.printf("%s is the same day like %s\n", this, weekday);
        } else {
            System.out.printf("%s is greater than %s\n", this, weekday);
        }
    }
}
