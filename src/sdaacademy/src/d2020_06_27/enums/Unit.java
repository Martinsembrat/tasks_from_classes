package sdaacademy.src.d2020_06_27.enums;

public enum Unit {

    //deklaracja i tworzenie obiektów typu Unit
    DISTANCE("km"),
    SPEED("m/s^2")
    ;

    String name;
    Unit(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName() {
        this.name = name;
    }
}
