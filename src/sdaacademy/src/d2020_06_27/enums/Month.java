package sdaacademy.src.d2020_06_27.enums;

public enum Month {
    //TEST(),//TEST() == TEST
    JANUARY("January"),//obiekt typu Month
    FEBRUARY("February"),//obiekt typu Month
    MARCH("March"),//obiekt typu Month
    APRIL("April"),//obiekt typu Month
    MAY("May"),//obiekt typu Month
    JUNE("June");//obiekt typu Month
    //JULY(1);//name będzie równe null, a value 1

    String name;
    //int value;

    //konstruktor w enum może być tylko pakietowy
    Month(String name) {
        this.name = name;
    }

//    Month(int value) {
//        this.value = value;
//    }

    public String getMonthName() {
        return name;
    }
}
