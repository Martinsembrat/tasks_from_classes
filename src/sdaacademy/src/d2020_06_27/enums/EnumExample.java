package sdaacademy.src.d2020_06_27.enums;

public class EnumExample {

    public static final String JANUARY = "styczeń";

    public static void main(String[] args) {
//        System.out.println(MonthV2.FEBRUARY.namePl);
//        System.out.println(MonthV2.FEBRUARY.getNameEn());

        MonthV2 monthV2 = MonthV2.MARCH;

        MonthV2[] monthV2s = MonthV2.values();
        for (MonthV2 month : monthV2s) {
            System.out.println(month);//wyświetla nazwę obiektu
            System.out.println(month.namePl);
            System.out.println(month.getNameEn());
            System.out.println(month.ordinal());
        }

        switch (monthV2) {
            case JANUARY:
            case FEBRUARY:
                System.out.println("Zima!");
                break;
            case MARCH:
                System.out.println("Pół zima, pół wiosna!");
                break;
            case APRIL:
            case MAJ:
                System.out.println("Wiosna");
            default:
                System.out.println("Nie wiadomo!");
        }
    }
}
