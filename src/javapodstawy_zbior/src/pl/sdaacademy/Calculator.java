package javapodstawy_zbior.src.pl.sdaacademy;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number;
        boolean correctnumber;
        System.out.println("wprowadz liczbę całkowitą dodatnią");
        number = input.nextInt();
        int count = 0;
        while (count < 0) {
            if (number > 0) {
                correctnumber = true;
                System.out.println("poprawne wprowadzenie");
                count++;
            } else {
                correctnumber = false;
                System.out.println("błędne wprowadzenie, wprowadz ponownie");
            }
        }
        for (int i = 1; i < number; i++) {
            if (i % 2 != 0) {
                System.out.println(i);
            }
        }
    }
}