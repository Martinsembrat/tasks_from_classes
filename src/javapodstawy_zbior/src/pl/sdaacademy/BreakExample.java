package javapodstawy_zbior.src.pl.sdaacademy;

import java.util.Scanner;

public class BreakExample {

    public static void main(String[] args) {
        int number = 0;
        int counter = 0;
        int instructionCounter = 0;
        Scanner scanner = new Scanner(System.in);
        while (number != 30) {
            instructionCounter++;

            System.out.println("Podaj liczbę (jeśli chcesz wyjść wpisz -1)");
            number = scanner.nextInt();
            if (number == 100 ) {
                continue;//przejdź do kolejnej iteracji, ignoruj operacje poniżej!
            }
            //alternatywnie
//            if (number != 100) {
//                System.out.println("Zwiększamy licznik!");
//                counter++;
//            }
            if (number == -1) {
                System.out.println("Kończymy");
                break;//przerwij działanie pętli while
            }
        }
        System.out.println("Liczba prób ");
        System.out.println(counter);

        System.out.println("Liczba iteracji ");
        System.out.println(instructionCounter);
    }
}
