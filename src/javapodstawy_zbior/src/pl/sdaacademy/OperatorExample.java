package javapodstawy_zbior.src.pl.sdaacademy;

public class OperatorExample {

    public static void main(String[] args) {
        //int a;
        //int b;
        //int a, b;
        int a = 10;
        int b = 20;

        a = a + 1;//11
//        System.out.println(++a);//12
//        System.out.println(a++);//12
//        System.out.println(a);

        //b = b++ + 3;//ingorowanie b++
        a = b++ + 3;//23
        System.out.println(a);
        System.out.println(b);//21


//        int a1 = 12;
//        int b1 = 20;
//
//        ++a1;//13
//        ++b1;//21
//
//        a1 = a1 + b1; //34
//        b1 = ++b1 + a1;//22 + 34 = 56
//        System.out.println(a1);
//        System.out.println(b1);
//
//        int c = 20;
//
//        //przykłady inkrementacji o 1
//        //++c;
//        //c++;
//        //c=c+1;
//
//        //c+=1; //=> c=c+1
//        //c-=1; //=> c=c-1;
//        //c-=4; //=> c=c-4;
//        //c*=4; //=> c=c*4
//        //c%=4; //=> c=c%4
//        //c/=4; //=>c= c/4
//        System.out.println(c);
//
//        System.out.println(++c);//c=c+1; wyświetlanie
//        System.out.println(c++);//wyświetlanie;c=c+1;
    }
}
