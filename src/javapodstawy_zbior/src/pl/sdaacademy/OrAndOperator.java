package javapodstawy_zbior.src.pl.sdaacademy;

public class OrAndOperator {


    public static void main(String[] args) {
        int age = 18;//wartość pobrana od użytkownika
        char gender = 'M';//wartość pobrana od użytkownika
        boolean suite = true;//wartość pobrana od użytkownika

        boolean accept = (age >= 21) || suite || gender == 'M';
        System.out.println(accept);

        boolean acceptV2 = (age >= 21) && suite && gender == 'M';
        System.out.println(acceptV2);

        boolean acceptV3 = gender == 'K' || (age >= 21 && suite);
        System.out.println(acceptV3);
    }
}
