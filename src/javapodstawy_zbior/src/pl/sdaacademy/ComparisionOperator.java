package javapodstawy_zbior.src.pl.sdaacademy;

public class ComparisionOperator {

    public static void main(String[] args) {
        int a = 20;
        int b = 30;

        boolean equal = (a == b);//sprawdzanie czy a jest równe b
        System.out.println("Czy są równe");
        System.out.println(equal);

        boolean notEqual = (a != b);//sprawdzenie czy a jest różne od b
        System.out.println("Czy są różne: ");
        System.out.println(notEqual);

        boolean greaterThan = (a > b);//sprawdzenie czy a jest większe od b
        System.out.println("Czy a jest większe od b");
        System.out.println(greaterThan);

        boolean lessThan = (a < b);
        System.out.println("Czy a jest mniejsze od b");
        System.out.println(lessThan);

        //a>=b; a=20, b=20;


        boolean greaterOrEqualThan = (a >= b);
        System.out.println("Czy a jest większe bądź równe b");
        System.out.println(greaterOrEqualThan);


        int c = 10;
        System.out.println(c>=c-2);
        //a==(c<b);//wartość int == wartość boolean

        System.out.println((c>=c)==(c<=c));

        //System.out.println((5>60)>(50<40)); //false == false;
    }
}
