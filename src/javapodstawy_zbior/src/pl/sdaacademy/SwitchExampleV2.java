package javapodstawy_zbior.src.pl.sdaacademy;

public class SwitchExampleV2 {

    public static void main(String[] args) {
        int place = 12;

        switch (place) {
            case 1:
            case 2:
            case 3:
                System.out.println("Dostajemy medal");
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                System.out.println("Dostajemy dyplom");
            default:
                System.out.println("Gratulacje");
        }
    }
}
