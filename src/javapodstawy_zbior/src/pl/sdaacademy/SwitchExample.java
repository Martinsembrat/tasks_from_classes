package javapodstawy_zbior.src.pl.sdaacademy;

public class SwitchExample {

    public static void main(String[] args) {
        int room = 23;
        switch (room) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 12:
                System.out.println("Dowody osobiste");
                break;
            case 17:
                System.out.println("Podatek od nieruchomości");
                break;//zakończ instrukcję switch
            case 23:
                System.out.println("Ewidencja ludności");
                break;
            default:
                System.out.println("Nieznany!");
        }
    }
}
