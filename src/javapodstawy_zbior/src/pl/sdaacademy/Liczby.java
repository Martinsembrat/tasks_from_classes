package javapodstawy_zbior.src.pl.sdaacademy;

public class Liczby {

    public static void main(String[] args) {
        //liczby stałoprzecinkowe
        byte temperature = 23;
        temperature = 25;
        int numberOfPeople = 3000000;
        //nie można
        //long moneyDebt = 900_000_0000_L;
        //long moneyDebt = 900_000_0000L_;
        long moneyDebt = 900_000_0000L;

        temperature = 19;

        System.out.println("Temperatura");
        System.out.println(temperature);
        System.out.println("Liczba osób");
        System.out.println(numberOfPeople);
        System.out.println("Dług");
        System.out.println(moneyDebt);

        //liczby zmiennoprzecinkowe
        //nie można
        //float price = 12._99f;//F
        float price = 12.99f;//F
        double gpsCoordinate = 1.984343434343434343434343434;
        System.out.println("Cena: ");
        System.out.println(price);
        System.out.println("GPS ");
        System.out.println(gpsCoordinate);

        //znak
        char newLine = '\n';
        char letter = 'P';
        System.out.print(newLine);
        System.out.println(letter);

        //boolean
        boolean lightTurnOn = false;
        System.out.println("światło");
        System.out.println(lightTurnOn);
    }
}
