package javapodstawy_zbior.src.pl.sdaacademy;

import java.util.Scanner;

public class Request9 {
    public static void main(String[] args) {
        /*
        Zadanie 9. Napisać program, który wczytuje od użytkownika liczbę całkowitą dodatnią
        , a następnie wyświetla na ekranie wszystkie potęgi liczby 2 nie większe, niż podana liczba.
        Przykładowo, dla liczby 71 program powinien wyświetlić: 1 2 4 8 16 32 64
         */
        Scanner input = new Scanner(System.in);
        int number;
        int b = 2;
        int i;
        System.out.println("Proszę wpisac liczbę całkowitą dodatnią: ");
        number = input.nextInt();
        System.out.println("Wszystkie potęgi liczby 2 występujace w " + number + " :");
        for (i = 1; i <= number; i++) {
            int sum = (int) Math.pow(b, i);
            System.out.println(" " + sum);
        }
    }
}