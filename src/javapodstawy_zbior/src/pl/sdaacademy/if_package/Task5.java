package javapodstawy_zbior.src.pl.sdaacademy.if_package;

import java.util.Scanner;
public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        float dochód;
        float podatek;
        float próg = 85.528f;
        System.out.println("Podaj dochód");
        dochód = input.nextFloat();
        if (dochód <= próg) {
            podatek = ((dochód * 18) / 100) - 556.02f;
            System.out.println("Suma podatku");
            System.out.println(podatek);
        } else if (dochód > próg) {
            float nadwyżka = (dochód - 14_839.02f);
            podatek = 14_839.02F + ((nadwyżka) * 32) / 100;
            System.out.println("Suma podatku");
            System.out.println(podatek);
        }
    }
}