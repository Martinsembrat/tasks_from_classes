package javapodstawy_zbior.src.pl.sdaacademy.if_package;

import java.util.Scanner;

public class BMICalculator {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        float weight, height;
        System.out.println("Podaj wagę: ");
        weight = input.nextFloat();
        System.out.println("Podaj wzrost: ");
        height = input.nextFloat();
        float bmi = weight/(height*height);

        //Wersja 1
        if (bmi >= 18.5 && bmi <= 24.9) {
            System.out.println("Waga prawidłowa");
        } else if (bmi < 18.5) {
            System.out.println("Niedowaga");
        } else {
            System.out.println("Nadwaga");
        }

        //Wersja 2
        if (bmi < 18.5) {
            System.out.println("Niedowaga");
        } else if (bmi > 24.9) {
            System.out.println("Nadwaga");
        } else {
            System.out.println("Waga prawidłowa");
        }
    }
}
