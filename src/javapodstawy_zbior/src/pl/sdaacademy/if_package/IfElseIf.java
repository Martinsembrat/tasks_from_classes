package javapodstawy_zbior.src.pl.sdaacademy.if_package;

public class IfElseIf {

    public static void main(String[] args) {
        int age = 19;
        char gender = 'M';

        if (gender == 'K' || age >= 21) {//1
            System.out.println("Wchodzimy do lokalu");
        } else if (age == 19) { //<0;n>
            System.out.println("Jeśli ubierzesz garnitur to wejdziesz");
        } else if (age == 20) {
            System.out.println("Przyjdź za rok :)");
        } else { //<0,1>
            System.out.println("Nie można wejść do lokalu");
        }

        //if ()
    }
}
