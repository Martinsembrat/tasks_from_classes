package javapodstawy_zbior.src.pl.sdaacademy.if_package;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float income, tax;
        float taxCountPoint = 85528F;
        byte taxPercent1 = 18;
        byte taxPercent2 = 32;
        float taxValue1 = 556.02F;
        float taxValue2 = 14839.02F;
        System.out.println("Enter your income value");
        income = scanner.nextFloat();
        if (income < taxCountPoint) {
            tax = income * taxPercent1 / 100 - taxValue1;
        } else {
            tax = taxValue2 + (income - taxCountPoint) * taxPercent2 / 100;
        }
        System.out.println("The tax is " + tax);
    }
}