package javapodstawy_zbior.src.pl.sdaacademy.if_package;

public class IfThen {

    public static void main(String[] args) {
        int age = 18;
        char gender = 'M';

        System.out.println("Sprawdzamy warunek 1");
        if (gender == 'K' || age >= 21) {
            //wyświetli się jeśli warunek jest spełniony
            System.out.println("Wchodzimy do klubu!");
        } else {
            System.out.println("Nie możemy wejść do klubu");
        }

        //System.out.println("Sprawdzamy warunek 2");
        //if (gender != 'K' && age < 21) {
            //System.out.println("Nie możemy wejść do klubu");
        //}
        //wyświetli się zawsze
        //System.out.println("Nie możemy wejść do klubu");

        /*
        komentarz
         */
    }
}
