package javapodstawy_zbior.src.pl.sdaacademy.zadania;

import java.util.Scanner;

public class Zadanie1 {
    public static void main(String[] args) {
        double temperatureInCelsius;
        double temperatureInFahrenheit;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter temperature in Celsius");
        temperatureInCelsius = scanner.nextDouble();
        temperatureInFahrenheit = (1.8 * temperatureInCelsius + 32.0);
        System.out.println("Temperature in Celsius = " + temperatureInCelsius);
        System.out.println("Temperature in Fahrenheit = " + temperatureInFahrenheit);
    }
}