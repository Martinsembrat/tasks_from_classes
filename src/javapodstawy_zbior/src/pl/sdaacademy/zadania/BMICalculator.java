package javapodstawy_zbior.src.pl.sdaacademy.zadania;

import java.util.Scanner;

public class BMICalculator {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        float weight;
        int height;
        float bmi;

        //pobieranie wartości od użytkownika
        System.out.println("Podaj wagę: ");
        weight = input.nextFloat();
        System.out.println("Podaj wzrost");
        height = input.nextInt();

        bmi = (weight/(height*height)) * 10000;

        System.out.println("Wynik BMI: ");
        System.out.println(bmi);

    }
}
