package javapodstawy_zbior.src.pl.sdaacademy;

import java.util.Locale;
import java.util.Scanner;

import java.util.Scanner;
public class ScannerExample {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        long value1;
        float value2;
        System.out.println("Wartość stało-przecinkowa:");
        value1 = input.nextLong(); //pobieranie z klawiatury stałoprzecinkowej
        System.out.println("Wartość zmienno-przecinkowa:");
        value2 = input.nextFloat(); //zmiennoprzecinkowej
        System.out.println(value1);
        //System.out.println(value2);
        System.out.print(String.format(Locale.US, "%.2f", value2));
    }
}
