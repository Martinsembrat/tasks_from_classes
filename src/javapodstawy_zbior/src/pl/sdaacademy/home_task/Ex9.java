package javapodstawy_zbior.src.pl.sdaacademy.home_task;

import java.util.Scanner;

public class Ex9 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        int number = scanner.nextInt();
        int marginValue = (int) Math.sqrt(number);//obliczenie pierwiastka z wskazanej wartości we celu określenia ile iteracji jest potrzebnych
        for (int i = 0; i < marginValue - 1; i++) {//iterowanie od wartości 0 do wartości marginValue - 1
            int powI = (int) Math.pow(2, i); //podnoszenie do potęgi każdorazowo wartości i
            System.out.println(powI);
        }
    }
}
