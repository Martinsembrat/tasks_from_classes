package javapodstawy_zbior.src.pl.sdaacademy.home_task;

import java.util.Scanner;

public class Ex2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a, b, c;
        System.out.println("Podaj liczbę a: ");
        a = scanner.nextInt();
        System.out.println("Podaj liczbę b: ");
        b = scanner.nextInt();
        System.out.println("Podaj liczbę c: ");
        c = scanner.nextInt();

        int max = a;//zakładamy, że pierwsza podana jest największa
        if (b > max) { //jeśli druga liczba jest większa od aktualnego maximum dokonujemy aktualizacji
            max = b;
        }
        if (c > max) {//jeśli trzecia liczba jest większa od aktualnego maximum dokonujemy aktualizacji
            max = c;
        }
        System.out.println("Liczba jest największa!");
        System.out.println(max);
    }
}
