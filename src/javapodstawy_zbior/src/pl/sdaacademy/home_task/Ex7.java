package javapodstawy_zbior.src.pl.sdaacademy.home_task;

import java.util.Scanner;

public class Ex7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        int number = scanner.nextInt();
        for (int i = 0; i < number; i++) {
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }
    }
}
