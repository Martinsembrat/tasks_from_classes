package javapodstawy_zbior.src.pl.sdaacademy;

import java.util.Scanner;

public class MyScanner {
    public static void main(String[] args) {
        //new HyScanner(System.ini -> przekazanie żródła danych (System.in)
        //wprowadzanych z klawiatury do obiektu MyScanner
        int temperature;
        int temperature2;
        Scanner pobieranieZKlawiaruty = new Scanner(System.in);
        System.out.println("Podaj dane z klawiatury: ");
        //czekaj na dane z klawiatury
        temperature = pobieranieZKlawiaruty.nextInt();
        System.out.println("Podaj temperature drugą");
        temperature2 = pobieranieZKlawiaruty.nextInt();
        System.out.println("Podane temperatury");

        if (temperature > temperature2) {
            System.out.println(temperature);
        } else {
            System.out.println(temperature2);
        }
        System.out.println(temperature);
        System.out.println(temperature2);
    }
}