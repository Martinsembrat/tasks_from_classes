package javapodstawy_zbior.src.pl.sdaacademy;

import java.util.Scanner;
public class Zadania {
    public static void main(String[] args) {
        System.out.println("Jak masz na imię?");
        Scanner czytnik = new Scanner(System.in);
        String imię = czytnik.nextLine();
        System.out.println("Witaj, " + imię + "!");
        System.out.println("Kiedy masz urodziny? ;)");
        float data = 9.03f;
        float input = 0;
        while (input != data) {
            input = czytnik.nextFloat();
            if(input == data) {
                System.out.println("W takim razie wszystkiego najlepszego!");
            }else{
                System.out.println("A to spadaj.");
            }
        }
        System.out.println("Miło było poznać.");
    }
}