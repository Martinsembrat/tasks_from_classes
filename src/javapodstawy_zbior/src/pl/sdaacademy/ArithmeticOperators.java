package javapodstawy_zbior.src.pl.sdaacademy;

public class ArithmeticOperators {

    public static void main(String[] args) {
        int value1 = 10;
        int value2 = 20;

        int sumResult = value1 + value2; //sumowanie: value1+value2
        System.out.println("Suma: ");
        System.out.println(sumResult);

        int multiplyResult = value1 * value2; //mnożenie: value1*value2
        System.out.println("Mnożenie: ");
        System.out.println(multiplyResult);

        int subResult = value1 - value2; //odejmowanie: value1-value2
        System.out.println("Odejmowanie: ");
        System.out.println(subResult);

        int divideResult = value2 / value1; //dzielenie: value1/value2
        System.out.println("Dzielenie: ");
        System.out.println(divideResult);

        int modResult = value2 % value1; //modulo: value%value1; tzw. reszta z dzielenia
        System.out.println("Modulo: ");
        System.out.println(modResult);

        int complexExpression = ((value2 + value1) - (value2-value1))/3;
        System.out.println("Złożone wyrażenie: ");
        System.out.println(complexExpression);
    }
}
