package javapodstawy_zbior.src.pl.sdaacademy;

import java.util.Scanner;
public class KonwersjaTemp {
    public static void main(String[] args) {
        float celsiusTemp;
        Scanner pobieranieDanych = new Scanner(System.in);
        System.out.println("Podaj temperaturę w C: ");
        celsiusTemp = pobieranieDanych.nextFloat();
        float fahrenheitTemp = (1.8F * celsiusTemp) + 32;
        System.out.println("Konwertowanie: " + celsiusTemp + " stopni Celsjusza to:");
        System.out.print(fahrenheitTemp);
        System.out.println(" stopni Fahrenheita");
    }
}