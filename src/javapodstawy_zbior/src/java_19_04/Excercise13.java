package javapodstawy_zbior.src.java_19_04;

/*
Zadanie 13. Napisz program usuwający duplikaty z tablicy i
zwracający nową tablicę.
Sample array: [20, 20, 30, 40, 50, 50, 50] New array: [20, 30, 40, 50]
 */
public class Excercise13 {

    public static void main(String[] args) {
        //wartości tablicy >= 0
        int[] input = {20, 20, 30, 40, 50, 50, 50};
        int[] result = new int[input.length]; //tablica z samymi 0 -> {0,0,0,0,0,0,0}

        for (int i=0;i<input.length;i++) {
            int element = input[i];
            boolean isUnique = true;
            //sprawdzamy, że mamy dany element w tablicy result
            for (int j=0;j<result.length;j++) {
                if (element == result[j]) {
                    //element jest już w tablicy result- nie jest unikalny
                    isUnique = false;
                    break;
                }
            }
            if (isUnique) {
                result[i] = element;
            }else {
                //tutaj jest duplikat
                result[i] = -1;
            }
        }
        //wyświetlanie elementów listy
        for (int i=0;i<result.length;i++) {
            System.out.println(result[i]);
        }
    }
}
