package javapodstawy_zbior.src.java_19_04;

public class ArrayEx5Dom {
    public static void main(String[] args) {
        int[][] tab = {
                {1, -2, 3},
                {3, 2, 1},
                {2, 1, -3}
        };
        System.out.println("Czy w tablicy są wszystkie liczby większe od zera? ");
        for (int i = 0; i < tab.length; i++) {
            boolean answer = true;
            for (int j = 0; j < tab[i].length; j++) {
                if (tab[i][j] <= 0) {
                    answer = false;
                    break;
                }
            }
            if (answer) {
                System.out.println("Wszystkie większe od ZERA");
            } else {
                System.out.println("Mniejsze, bądź równe 0");
            }
        }
    }
}