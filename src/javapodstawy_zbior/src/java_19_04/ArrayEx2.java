package javapodstawy_zbior.src.java_19_04;

public class ArrayEx2 {

    public static void main(String[] args) {
        int[][] tab = {
                {'O', 'O', 'X'},
                {'O', 'X', 'O'},
                {'X', 'X', 'X'}
        };


        for (int i = 0; i < tab.length; i++) {
            //szukamy wiersz który zawiera same znaki X
            boolean answer = true;
            for (int j = 0; j < tab[i].length; j++) {
                //jeśli któryś z elementów tablicy jednowymiarowej i nie jest `X`
                // to zmieniamy status zmiennej answer na false i przerywamy działeni pętli,
                // nie ma sensu sprawdzać dalej elementów
                if (tab[i][j] != 'X') {
                    answer = false;
                    break;
                }
            }
            //jeśli w wyniku weryfikacji konkretnego wiersza zmienna answer jest równa true to znaczy
            // że analizowany wiersz zawiera same znaki `X` -> wyświetlamy komunikat
            // i przerywamy działanie pętli zewnętrznej
            if (answer) {
                System.out.println("W tablicy jest wiersz zawierający same znaki X");
                break;
            }
        }
    }
}
