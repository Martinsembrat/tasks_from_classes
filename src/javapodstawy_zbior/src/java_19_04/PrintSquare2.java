package javapodstawy_zbior.src.java_19_04;


/*
liczba wierszy: 4
liczba kolumn: 3
przesunięcie: 4

    ***
    ***
    ***
    ***

 */
public class PrintSquare2 {

    public static void main(String[] args) {
        int row = 4;
        int column = 3;
        int offset = 4;
        char sign = '&';

        //pętla jest odpowiedzialna za wyświetlanie poszczególnych wierszy
        for (int i=0;i<row;i++) {
            for (int space=0;space<offset;space++) {
                System.out.print(" ");
            }
            for (int j=0;j<column;j++) {
                System.out.print(sign);
            }
            System.out.println();
        }
    }
}


/*
liczba wierszy: 4
liczba kolumn: 3

***
 ***
  ***
   ***
 */