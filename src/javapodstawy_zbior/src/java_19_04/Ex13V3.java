package javapodstawy_zbior.src.java_19_04;

public class Ex13V3 {

    public static void main(String[] args) {
        /*
        ------------------------------------------------
         */
        //deklaracja i inicjalizacja tablicy na dane wejściowe
        int[] input = {20, 20, 30, 40, 50, 50, 50};//tablica wejścia
        //deklaracja i inicjalizacja tablicy tymczasowej na liczby unikalne,
        //która będzie mieć ten sam rozmiar co tablica wejściowa co może powodować
        //zbyt duży rozmiar względem unikatów - będzie nadmiarowa (będzie mniej unikalnych elementów niż rozmiar tablicy)
        int[] tmpResult = new int[input.length];
        //deklaracja i inicjalizacja zmiennej odpowiedzialnej za zliczenie liczby unikalnych elementów
        int counter = 0;

        /*
        ------------------------------------------------
        */
        //Wyszukiwanie unikalnych elementów
        for (int i=0;i<input.length;i++) {
            int element = input[i];
            //zmienna odpowiedzialna za określanie unikalności wartości element
            //zakładamy wstępnie, że element jest unikaln
            boolean isUnique = true;
            //każdorazowo sprawdzamy, czy element który zakładamy
            // jest unikalny pojawił się już wcześniej w tablicy input
            //w porównaniu do wersji Ex13V2 nie porównujemy
            // potencjalnych unikatów z wartościami tabeli pośredniej, tylko z rzeczywistymi wartościami w ramach tabeli wejściowej
            //porównywanie następuje w ramacj zasady element weryfikowany z każdym poprzednim,np.
            /*
            {20, 20, 30, 40, 50, 50, 50}
            element 50 pod indeksem 5 porównywany jest z: 20, 20, 30, 40, 50 i element o indeksie 4 ma tą
            samą wartość więc mamy doczynienia z duplikatem
             */
            for (int j=0;j<i;j++) {
                //jeśli się znajduje to znaczy że mamy doczyniania z duplikatem
                if (element == input[j]) {
                    //ustawiamy wartość isUnique w celu zaznaczenia, że element nie jest unikalny w zbiorze
                    isUnique = false;
                    break;
                }
            }
            //jeśli weryfikowanye element `element` jest unikalny to dodajemy go do tablicy tymczasowej
            if (isUnique) {
                //zmienna counter określa, który unikat znaleźliśmy
                //pierwszy element unikalny trafi pod indeks 0
                //drugi element unikalny trafi pod indeks 1
                //trzeci element unikalny trafi pof indeks 2
                //itd.
                //zmienna counter powoduje, że elementy unikalne będą się znajdować w tablicy obok siebie
                tmpResult[counter] = element;
                counter++;
            }
        }

        /*
        ------------------------------------------------
        */
        //Tworzenie finalnej tablicy
        //rozmiar tablicy wyjściowej na unikalne elementy będzie równy counter
        int[] finalResult = new int[counter];
        //uzupełnienie tablicy finalnej
        for (int i=0;i<counter;i++) {
            //tmpResult[0] -> pierwszy unikat
            //tmpResult[1] -> drugi unikat
            //tmpResult[2] -> trzeci unikat
            //itd
            finalResult[i] = tmpResult[i];
        }


        /*
        ------------------------------------------------
        */
        //wyświetlanie elementów listy
        for (int i=0;i<finalResult.length;i++) {
            System.out.println(finalResult[i]);
        }
    }
}
