package javapodstawy_zbior.src.java_19_04;

public class DeleteDuplicates {
    public static void main(String[] args) {
        //deklaracja i inicjalizacja tablicy na dane wejściowe
        int[] tab = {1, 24, 23, 543, 6, 6, 6, 6, 24, 24, 1, 234, 6, 3, 3};
        //deklaracja tablicy na o
        boolean isUnique[] = new boolean[tab.length];
        int counter = 0;
        for (int i = 0; i < tab.length; i++) {
            isUnique[i] = true;
            for (int j = 0; j < i; j++) {
                if (tab[i] == tab[j]) {
                    isUnique[i] = false;
                    break;
                }
            }
            if (isUnique[i]) counter++;
            System.out.println(tab[i] + " - " + isUnique[i] + " - " + counter);
        }
        int result[] = new int[counter];
        int resultCounter = 0;
        for (int i = 0; i < tab.length; i++) {
            if (isUnique[i]) {
                result[resultCounter] = tab[i];
                resultCounter++;
            }
        }
        System.out.println("--------");
        System.out.println("Wynik");
        System.out.println("--------");
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + ", ");
        }
    }
}