package javapodstawy_zbior.src.java_19_04;

public class ArrayEx1 {

    public static void main(String[] args) {

        //tablica ma dwa identyczne rozmiary
        int[][] tab = {
                {'X', 'O', 'O'},
                {'O', 'X', 'O'},
                {'O', 'O', 'X'}
        };
        /*
        0,0
        1,1
        2,2

         */
        //zmienna określająca czy tablica zawiera same znaki X po przekątnej
        boolean answer = true;
        for (int i=0;i<tab.length;i++) {
            //tab[0][0], tab[1][1], tab[2][2]
            if (tab[i][i] != 'X') {
                answer = false;
                break;//przerwij działanie pętli
            }
        }


        if (answer) {
            System.out.print("Znak X znajduje się w całości po przekątnej");
        } else {
            System.out.print("Znak X nie znajduje się w całości po przekątnej");
        }

    }
}
