package javapodstawy_zbior.src.java_19_04;

public class Array2DEx2 {

    /*
            int[][] a = {
                {1, 2, 3, 87, 43},
                {4, 5, 6, 9},
                {7, 435, 45}
        };
     */

    public static void main(String[] args) {
        int[][] a = {
                {1, 2, 3, 87, 43},
                {4, 5, 6, 9},
                {7, 435, 45}
        };
        int sum = 0;
        int counter = 0;
        for (int i=0;i<a.length;i++) {
            //opcja ze zliczaniem wszystkich elementów danej tablicy jednowymiarowej
            counter+=a[i].length;//=>counter = counter + a[i].length
            //pobierz rozmiar tablicy jednowymiarowej znajdującej się pod indeksem i
            for (int j=0;j<a[i].length;j++) {
                sum = sum + a[i][j];
                //opcja ze zliczaniem każdego elementu osobna
                //counter++;
            }
        }
        System.out.print("Suma: ");
        System.out.println(sum);
        //obliczanie średniej arytmetycznej
        float average = (float)sum/(float)counter;
        System.out.print("średnia: ");
        System.out.println(average);
    }
}
