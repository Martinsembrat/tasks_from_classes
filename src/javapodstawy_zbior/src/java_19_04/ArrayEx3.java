package javapodstawy_zbior.src.java_19_04;

/*
Dla tablicy int[]
{
 {6, 2, 4},
 {2, 4, 1},
 {2, 2, 0}
}
Sprawdź czy któryś z wierszy zawiera same parzyste wartości
 */
public class ArrayEx3 {

    public static void main(String[] args) {
        int[][] tab = {
                {1, 2, 4},
                {2, 4, 1},
                {2, 2, 0}
        };

        for (int i=0;i<tab.length;i++) {
            boolean answer = true;
            for (int j=0;j<tab[i].length;j++) {
                if (tab[i][j] % 2 != 0) {
                    answer = false;
                    break;
                }
            }
            if (answer) {
                System.out.print("Dany wiersz zawiera same parzyste wartości!");
                break;
            } else {
                System.out.println("Brak spełnienia założeń!");
                //break; -> przerwiemy działanie po pierwszym przebiegu pętli: nie poprawne!
            }
        }

    }

}
