package javapodstawy_zbior.src.home_tasks;

public class Array2d {

    public static void main(String[] args) {
        int[][] a = {
                {1, 2, 3},
                {4, 5, 6, 9},
                {7},
        };
        System.out.println(a.length);
        System.out.println(a[0].length);
        int[][] tab = new int[5][6];
        System.out.println(tab.length);
        System.out.println(tab[0].length);
        for (int i=0;i<5;i++) {
            for (int j=0;j<6;j++) {
                System.out.print(tab[i][j]);
            }
            System.out.println();
        }
    }
}
