package javapodstawy_zbior.src.home_tasks;

import java.util.Scanner;


public class TemperatureConverter {

    public static void main(String[] args) {
        Scanner pobieranieZKlawiatury = new Scanner(System.in);
        char decyzjaUzytkownika;

        //Chcesz wykonywać coś dopóki nie podasz litery y, więc sugeruje Ci to, że musisz skorzystać z pętli, najlepiej skorzystać z pętli do-while bo chcesz wykonać operację conajmniej raz
        //W instrukcji do  prosisz o podanie temperatury do konwersji i po wykonaniu obliczeń prosisz użytkownika o informacje czy chce liczyć dalej, przypisujesz ten znak do zmiennej zadeklarowanej w punkcie nr 2.
        do {
            System.out.println("Czy chcesz przekonwertować C na F [y/n]");
            //pobierasz wybór użytkownika z klawiatury
            decyzjaUzytkownika = pobieranieZKlawiatury.next().charAt(0);
            if (decyzjaUzytkownika != 'y') {
                //użytkownik nie chce konwertować temperatury,
                // więc przerywamy pętlę do-while
                break;
            }
            //jeśli decyzja użytkonika to `y` to prosi o podanie temperatury do konwersji
            System.out.println("Podaj temperaturę w stopniach Celsjusza - wynik faranhaity");
            //W instrukcji do  prosisz o podanie temperatury do konwersji i po wykonaniu obliczeń prosisz użytkownika o informacje czy chce liczyć dalej, przypisujesz ten znak do zmiennej zadeklarowanej w lini nr 19.
            //w ciele while  deklarujesz warunek stopu, a chcesz by operacja konwersji odbywała się dopóki podajesz literkę y
            double temperatureC = pobieranieZKlawiatury.nextInt();
            double temperaturaF = temperatureC * 1.8 + 32;
            System.out.println(temperaturaF);
            System.out.println("Czy chcesz kontynuować działanie programu [y/n]");
            decyzjaUzytkownika = pobieranieZKlawiatury.next().charAt(0);
            //dopóki użytkownik wyraża zgode kontynujesz działanie programu
        } while (decyzjaUzytkownika == 'y');
        System.out.println("Dziękujemy za skorzystanie z programu");
    }
}
