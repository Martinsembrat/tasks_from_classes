package javapodstawy_zbior.src.home_tasks;

import java.util.Scanner;

/*

Zadanie 1. Napisać program służący wyszukiwania
liczb parzystych w podanym przez użytkownika przedziale.
Użytkownik na wejściu podać ma początek i koniec przedziału
 */
public class Ex1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj początek przedziału: ");
        int begin = scanner.nextInt();
        System.out.println("Podaj koniec przedziału: ");
        int end = scanner.nextInt();
        for (int i = begin; i <= end; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }
}
