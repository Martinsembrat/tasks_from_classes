package javapodstawy_zbior.src.home_tasks;

import java.util.Scanner;

/*
Zadanie 7. Napisz program pobierający od użytkownika liczbę
 n i na jej podstawie wyświetlający kwadrat każdej liczby całkowitej <= n.
 */
public class Ex7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        int number = scanner.nextInt();
        for (int i = 1; i <= number; i++) {
            int pow = i * i;
            System.out.println(pow);
        }
    }
}
