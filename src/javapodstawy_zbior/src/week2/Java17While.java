package javapodstawy_zbior.src.week2;

import java.util.Scanner;

public class Java17While {

    public static void main(String[] args) {
        //aplikacja pobierająca wartości z klawiatury do momentu przekazania
        // wartości parzystej większej niż 20
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę");
        int number = scanner.nextInt();
        while (number <= 20 || number % 2 == 1) {
            System.out.println("Podaj jeszcze raz: ");
            number = scanner.nextInt();
        }
    }
}
