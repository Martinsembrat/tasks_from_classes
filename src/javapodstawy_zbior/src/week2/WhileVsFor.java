package javapodstawy_zbior.src.week2;

import java.util.Scanner;

public class WhileVsFor {
    public static void main(String[] args) {
//        System.out.println("Wersja for");
//        for(int i=0;i<10;i++) {
//            System.out.println(i);
//        }
//        System.out.println("Wersja while");
//        int i = 0;
//        while (i<10) {
//            System.out.println(i);
//            i++;
//        }

        Scanner input = new Scanner(System.in);
        int numberOfClients = 10;
        int i; // twórz zmienną któ®a będzie wykorzystywana po za pętlą również
        for (i = 0; i < numberOfClients; i++) {
            System.out.println("Podaj swój wiek");
            int age = input.nextInt();
            if (age < 16) {
                System.out.println("Nie możesz obejrzeć filmu!");
            } else if (age >= 16 && age < 18) {
                System.out.println("Możesz obejrzeć film tylko za zgodą rodzica!");
            } else {
                System.out.println("Możesz obejrzeć film!");
            }
            System.out.println("Kolejna osoba! -----------");
        }
        // nie można tego zrobićdrugi raz - int i = 2323;
        System.out.println(i);
    }
}
