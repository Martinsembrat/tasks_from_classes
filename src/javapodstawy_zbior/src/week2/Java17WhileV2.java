package javapodstawy_zbior.src.week2;

import java.util.Scanner;

public class Java17WhileV2 {

    public static void main(String[] args) {
        //aplikacja pobierająca wartości z klawiatury do momentu przekazania
        // wartości parzystej większej niż 20
        Scanner scanner = new Scanner(System.in);
        int number;
        int counter = 0;
        //wykonanie operacji do następnie warunek while
        do {
            System.out.println("Podaj liczbę");
            number = scanner.nextInt();
            counter++;
        } while (number != 28);
        System.out.println("Próba: " + counter);

//        System.out.println("Podaj liczbę");
//        int number = scanner.nextInt();
//        int counter = 1;
          //warunek, a następnie operacja
//        while (number != 28) {
//            System.out.println("Podaj jeszcze raz: ");
//            number = scanner.nextInt();
//            counter++;
//        }
//        System.out.println("Próba: " + counter);
    }
}
