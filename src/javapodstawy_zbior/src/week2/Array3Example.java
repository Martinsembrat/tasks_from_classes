package week2;

import java.util.Scanner;

public class Array3Example {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] table = new int[10];
        for (int i=0;i<table.length;i++) {
            System.out.print("Podaj wartość dla elementu: ");
            System.out.println(i);
            table[i] = input.nextInt();
        }

        for (int i=0;i<table.length;i++) {
            System.out.print("Element: ");
            System.out.println(table[i]);
        }
    }

}