package javapodstawy_zbior.src.week2;

public class Java17WhileInfinity {

    public static void main(String[] args) {
//        while (true) {
//            System.out.println("Przebieg pętli");
//        }

        for(int i=0;;i++){
            System.out.println("Przebieg pętli for: ");
            System.out.println(i);
        }
    }
}
