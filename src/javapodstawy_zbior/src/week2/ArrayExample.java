package week2;

public class ArrayExample {

    public static void main(String[] args) {
        int[] array;//deklaracja
        int a=10;
        int[] array1 = new int[a];//tworzona jest tablica 20 elementowa, której wartości równe są 0
        System.out.println("Rozmiar tablicy: ");
        System.out.println(array1.length);
        System.out.println("Pobieranie elementu 10");
        System.out.println(array1[9]);//pobieranie wartości elementu 10
        array1[11] = 567;//przypisanie elementowi nr 12 wartości 567
        System.out.println("Pobieranie elementu 12");
        System.out.println(array1[11]);
        System.out.println("Pobieranie elementu 21");
        System.out.println(array1[20]);
    }
}
