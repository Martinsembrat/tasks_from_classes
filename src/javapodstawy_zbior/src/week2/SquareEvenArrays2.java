package javapodstawy_zbior.src.week2;

import java.util.Scanner;

public class SquareEvenArrays2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int lengthArray;
        System.out.println("Podaj wielkość tablicy: ");
        lengthArray = input.nextInt();
        int[] insertedValues, squares;
        boolean[] ifIsEven;
        insertedValues = new int[lengthArray];
        squares = new int[lengthArray];
        ifIsEven = new boolean[lengthArray];
        for (int i = 0; i < lengthArray; i++) {
            System.out.println("Podaj wartość nr " + (i + 1) + ": ");
            insertedValues[i] = input.nextInt();
            squares[i] = insertedValues[i] * insertedValues[i];
            if ((insertedValues[i] % 2) == 0)
                ifIsEven[i] = true;
            else
                ifIsEven[i] = false;
        }
        for (int i = 0; i < lengthArray; i++) {
            System.out.println((i + 1) + " --- " + insertedValues[i] + " --- " + squares[i] + " --- " + ifIsEven[i]);
        }
    }
}